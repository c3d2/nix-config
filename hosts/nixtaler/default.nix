{
  c3d2.deployment.server = "server10";
  system.stateVersion = "24.05";
  microvm.mem = 512;
  networking.hostName = "nixtaler";
  sops.defaultSopsFile = ./secrets.yaml;

  services.taler = {
    enable = true;
    settings.taler.CURRENCY = "SPURLOS";
    merchant = {
      enable = true;
      settings.merchant-exchange-spurlose = {
        EXCHANGE_BASE_URL = "https://exchange.taler.datenspuren.de/";
        # See `master_public_key` of https://exchange.taler.datenspuren.de/keys
        MASTER_KEY = "YRT0XTSCWT2ACCZJ4ACJ2030SGKH0V2916FE16X07ZHEWZFPS9Z0";
        CURRENCY = "SPURLOS";
      };
    };
    # exchange = {
    #   enable = true;
    # };
  };

  # services.libeufin = {
  #   enable = true;
  #   bank = {
  #     enable = true;
  #     settings = {};
  #   };
  #   nexus = {
  #     enable = true;
  #     settings = {};
  #   };
  # };

  services.nginx.virtualHosts."nixtaler.serv.zentralwerk.org" = {
    enableACME = true;
    forceSSL = true;
    locations."/".proxyPass = "http://localhost:8083/";
  };
}
