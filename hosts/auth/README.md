# Auth

We are using [Portunus](https://github.com/majewsky/portunus), an opinionated and simple to use LDAP distribution, to manage our OpenLDAP server and [DexIdp](https://dexidp.io/) to offer OIDC.
The [upstream documentation](https://github.com/majewsky/portunus?tab=readme-ov-file) complements this document.
New services should use OAuth/OIDC if possible to lay the groundwork for SSO.
If the application only support LDAP, that is also fine to use.

## Introduction to LDAP

Please see the [Portunus readme](https://github.com/majewsky/portunus?tab=readme-ov-file#connecting-services-to-portunus) for a very good introduction to LDAP and how to connect your service to LDAP.

## How to use it

See the [Grafana](../grafana) configuration to see an example on how to use OAuth.
Some parts are also in the [shared config](../../config/default.nix), to make them available to Portunus and the targeted service (in this case Grafana).
To create a new application, edit the Dex configuration under `services.portunus.dex` in the Portunus MicroVM.
The application credentials are saved securely in sops.

For an exmaple LDAP configuration in Nix using the nixos-modules abstraction see the [Gitea](https://github.com/NuschtOS/nixos-modules/blob/main/modules/gitea.nix), [Hydra](https://github.com/NuschtOS/nixos-modules/blob/main/modules/hydra.nix) or [Mail](../hosts/mail).
For the [LDAP connection settings see the nixos-modules](https://github.com/NuschtOS/nixos-modules/blob/main/modules/portunus.nix#L153-L168) and the upstream documentation about the [LDAP directory structure](https://github.com/majewsky/portunus?tab=readme-ov-file#ldap-directory-structure) and [Connecting services to Portunus](https://github.com/majewsky/portunus?tab=readme-ov-file#connecting-services-to-portunus).

For example:
- Host: `auth.serv.zentralwerk.org` (changed to internal IP via `/etc/hosts`)
- Port: `636`
- Bind DN: `uid=search,ou=users,dc=c3d2,dc=de`
- User Search Base: `ou=users,dc=c3d2,dc=de`
- User Filter: `(&(&(objectclass=person)(isMemberOf=cn=gitea-users,ou=groups,dc=c3d2,dc=de))(|(uid=%s)(mail=%s)))`
- Admin Filter: `(&(objectclass=person)(isMemberOf=cn=gitea-admins,ou=groups,dc=c3d2,dc=de))`
- Username Attribute: `uid`
- First Name Attribute: `givenName`
- Surname Attribute: `sn`
- Email Attribute: `mail`
- Public SSH Key Attribute: `sshPublicKey`

New service accounts are seeded via [`services.portunus.seedSettings.users`](./default.nix).
