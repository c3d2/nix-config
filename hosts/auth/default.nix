{ config, lib, libC, pkgs, ... }:

{
  c3d2.deployment.server = "server10";

  system.stateVersion = "22.05";

  networking = {
    firewall.extraInputRules = ''
      ip saddr { ${lib.concatStringsSep ", " libC.subnets.v4} } tcp dport ${toString config.security.ldap.port} accept comment "ldaps from internal"
      ip6 saddr { ${lib.concatStringsSep ", " libC.subnets.v6} } tcp dport ${toString config.security.ldap.port} accept comment "ldaps from internal"
    '';
    hostName = "auth";
    nftables.enable = true;
  };

  security.acme.certs."auth.serv.zentralwerk.org" = {
    dnsProvider = "rfc2136";
    credentialsFile = config.sops.secrets."acme/credentials-file".path;
    reloadServices = [ "portunus" ];
  };

  services = {
    backup = {
      enable = true;
      paths = [ "/var/lib/portunus/" ];
    };

    nginx = {
      enable = true;
      virtualHosts."auth.c3d2.de" = {
        enableACME = true;
        forceSSL = true;
        listen = libC.defaultListen;
        locations = {
          "/".proxyPass = "http://127.0.0.1:${toString config.services.portunus.port}";
          "/dex".proxyPass = "http://127.0.0.1:${toString config.services.portunus.dex.port}";
        };
      };
    };

    portunus = {
      enable = true;
      dex.oidcClients = [{
        callbackURL = "https://rspamd.c3d2.de/oauth2/callback";
        id = "oauth2_proxy_mail";
      }];
      ldap = {
      # TODO: drop after 31.12.2024
        package = pkgs.openldap.override { libxcrypt = pkgs.libxcrypt-legacy; };
        searchUserName = "search";
        suffix = "dc=c3d2,dc=de";
        tls = true;
      };
      # TODO: drop after 31.12.2024
      package = pkgs.portunus.override { libxcrypt = pkgs.libxcrypt-legacy; };
      port = 5555;
      removeAddGroup = true;
      seedGroups = true;
      seedSettings = {
        groups = [
          {
            long_name = "Portunus Administrators";
            name = "admins";
            members = [ "admin" ];
            permissions.portunus.is_admin = true;
          }
          {
            long_name = "Search";
            name = "search";
            members = [ "search" ];
            permissions.ldap.can_read = true;
          }
        ];
        users = [
          {
            family_name = "Administrator";
            given_name = "Initial";
            login_name = "admin";
            password.from_command = [ "/usr/bin/env" "cat" "/run/secrets/portunus/users/admin-password" ];
          }
          {
            email = "search@c3d2.de";
            family_name = "-";
            given_name = "Search";
            login_name = "search";
            password.from_command = [ "/usr/bin/env" "cat" "/run/secrets/portunus/users/search-password" ];
          }
          {
            email = "hackbert@c3d2.de";
            family_name = "Service Account";
            given_name = "Hackbert";
            login_name = "hackbert";
            password.from_command = [ "/usr/bin/env" "cat" "/run/secrets/portunus/users/hackbert-password" ];
          }
        ];
      };
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "acme/credentials-file" = { };
      "dex/environment".owner = "dex";
      "portunus/users/admin-password".owner = "portunus";
      "portunus/users/hackbert-password".owner = "portunus";
      "portunus/users/search-password".owner = "portunus";
    };
  };

  systemd.services.dex.serviceConfig = {
    DynamicUser = lib.mkForce false;
    EnvironmentFile = config.sops.secrets."dex/environment".path;
    StateDirectory = "dex";
    User = "dex";
  };

  users = {
    groups.dex = { };
    users.dex = {
      group = "dex";
      isSystemUser = true;
    };
  };
}
