{ zentralwerk, config, lib, libC, pkgs, ... }:

{
  imports = [
    ./soapysdr.nix
    ./adsb.nix
  ];

  c3d2 = {
    # unless you automate walking up to the roof, do never enable
    # automatic updates for this machine!
    autoUpdate = lib.mkForce false;

    pi-sensors = if true then [] else [ {
      type = "dht22";
      pin = 17;
      location = "Schrank";
    } {
      type = "dht22";
      pin = 23;
      location = "Aussen";
    } ];

    statistics.enable = true;
  };

  boot = {
    loader = {
      # generates entry for u-boot
      generic-extlinux-compatible.enable = true;
      grub.enable = false;
    };

    kernelParams = lib.mkForce [
      "console=tty0"
      # allow GPIO access
      "iomem=relaxed" "strict-devmem=0"
    ];
    # No ZFS on latest kernel:
    supportedFilesystems = lib.mkForce [ "vfat" "ext4" ];

    tmp.useTmpfs = true;
    extraModulePackages = [ ];
    initrd = {
      availableKernelModules = [ "usbhid" ];
      kernelModules = [ ];
    };
    kernelModules = [ ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };

    "/boot/firmware" = {
      device = "/dev/disk/by-label/FIRMWARE";
      fsType = "vfat";
    };
  };

  swapDevices = [ ];

  hardware.deviceTree.enable = false;

  powerManagement.cpuFreqGovernor = lib.mkDefault "performance";

  nixpkgs.config.packageOverrides = pkgs: {
    makeModulesClosure = x:
      # prevent kernel install fail due to missing modules
      pkgs.makeModulesClosure (x // { allowMissing = true; });
  };

  nix.settings = {
    cores = 4;
    max-jobs = 2;
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ./secrets.yaml;
  };

  networking = {
    hostName = "radiobert"; # Define your hostname.
    useDHCP = false;
    interfaces.eth0.ipv4.addresses = [{
      address = libC.hostRegistry."${config.networking.hostName}".ip4;
      prefixLength = zentralwerk.lib.config.site.net.serv.subnet4Len;
    }];
    defaultGateway = "172.20.73.1";
  };

  environment.systemPackages = with pkgs; [
    libraspberrypi
    raspberrypi-eeprom
  ];

  services = {
    # Do not log to flash:
    journald.extraConfig = ''
      Storage=volatile
    '';
    openssh.enable = true;

    # Allow access to USB
    udev.extraRules = ''
      SUBSYSTEM=="usb", MODE:="0666"
    '';
  };

  systemd.extraConfig = ''
    # Keep cores 2-3 exclusive for SDR processing
    CPUAffinity=0-1
  '';
  systemd.services = {
    soapysdr-server.serviceConfig.CPUAffinity = "2-2";
    dump1090.serviceConfig.CPUAffinity = "3-3";
  };

  system.stateVersion = "21.05"; # Did you read the comment?
}
