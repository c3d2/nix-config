{ libC, pkgs, ... }:

{
  c3d2.deployment = {
    server = "server10";
    nixStoreBackend = "blk";
  };

  microvm.mem = 3 * 1024;

  networking.hostName = "pretalx";

  services = {
    backup.enable = true;

    nginx = {
      enable = true;
      commonHttpConfig = /* nginx */ ''
        proxy_headers_hash_bucket_size 64;
      '';
      virtualHosts = {
        "pretalx.c3d2.de" = {
          forceSSL = true;
          enableACME = true;
          listen = libC.defaultListen;
          locations."/".return = "307 https://talks.datenspuren.de$request_uri";
        };
        "talks.datenspuren.de" = {
          default = true;
          enableACME = true;
          forceSSL = true;
          listen = libC.defaultListen;
        };
      };
    };

    postgresql = {
      enable = true;
      package = pkgs.postgresql_17;
    };

    pretalx = {
      enable = true;
      gunicorn.extraArgs = [
        "--name=pretalx"
        "--workers=4"
        "--max-requests=1200"
        "--max-requests-jitter=50"
        "--log-level=info"
      ];
      nginx.domain = "talks.datenspuren.de";
      settings.mail = {
        from = "noreply@c3d2.de";
        host = "mail.flpk.zentralwerk.org";
        port = "25";
      };
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
  };

  system.stateVersion = "23.11";
}
