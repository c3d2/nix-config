{ config, lib, ... }:

{
  c3d2.deployment.server = "server10";

  networking.hostName = "mucbot";

  services.tigger = {
    enable = true;
    user = "tigger";
    group = "tigger";
    jid = "astrobot@jabber.c3d2.de";
    passwordFile = config.sops.secrets."mucbot/password".path;
    mucs = [ "c3d2@chat.c3d2.de/Astrobot" "international@chat.c3d2.de/Astrobot" ];
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "mucbot/password".owner = "tigger";
    };
  };

  # Required by tigger `evalNix()`
  nix.enable = lib.mkForce true;

  system.stateVersion = "18.09";

  users = {
    groups.tigger = { };

    users.tigger = {
      createHome = true;
      isNormalUser = true;
      group = "tigger";
    };
  };
}
