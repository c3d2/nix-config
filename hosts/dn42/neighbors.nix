{
  dc16 = {
    address4 = "172.22.16.1";
    address6 = "fe80::250:bfff:fe41:5e57";
    asn = 64616;
    description = "Astro";
    wireguard = {
      listenPort = 2325;
      publicKey = "vCtPRpRc2SbTVRxKB6v7qL4DwaGvBAwh4z3uVFeh9BU=";
    };
  };
  dc24  = {
    address4 = "172.22.24.1";
    address6 = "fe80::cafe:babe";
    asn = 64624;
    description = "spaceboyz.net";
    wireguard = {
      publicKey = "5GyoCIRvTeUp6nSusZ8CMNVK/kjUTk1qkOSxKDW8Ng8=";
      listenPort = 2399;
      endpoint = "spaceboyz.net:2399";
    };
  };
  dc98 = {
    address4 = "172.22.100.254";
    address6 = "fe80::c3d2";
    asn = 64698;
    description = "inbert.c3d2.de";
    openvpn = ''
      remote 217.197.84.54  # dn42.c3d2.de
      port 2327
      comp-lzo
    '';
  };
  zw = {
    interface = "eth0";
    address4 = "172.22.99.250";
    address6 = "fe80::814:48ff:fe01:2201";
    asn = "4242421127";
    description = "bgp.c3d2.zentralwerk.org";
  };
  dc113 = {
    address4 = "172.22.113.244";
    asn = "64713";
    description = "martin89";
    openvpn = ''
      remote tunnel1.martin89.de 40533 udp4
      comp-lzo
    '';
  };
  dc4242421374 = {
    asn = "4242421374";
    description = "eri";
    address4 = "172.23.64.129";
    address6 = "fe80::0302";
    wireguard = {
      listenPort = 2338;
      publicKey = "rve99NGZIJLB4mPj2M7mjQmTHWMkLo5sXvFuur6TKiU=";
    };
  };
  dc4242421789 = {
    asn = "4242421789";
    description = "gregoire.boillet@protonmail.com";
    address4 = "172.21.93.97";
    address6 = "fe80::17:89";
    wireguard = {
      listenPort = 64699;
      endpoint = "217.12.209.150:64699";
      publicKey = "ifxB+zSOdT5vJ/HfdXjvphy962rfezrA9CWYe4ISTgE=";
    };
  };
  dc4242421602 = {
    asn = "4242421602";
    description = "toon";
    address4 = "172.23.162.33";
    address6 = "fe80::1602";
    wireguard = {
      listenPort = 2339;
      publicKey = "D9swl/xZOhTDT57IMf9QKq+FR7plZHCtbMo+SE7uJg0=";
    };
  };

  dc4242420604 = {
    asn = "4242420604";
    description = "http://blog.cas7.moe/peering/";
    address4 = "172.23.89.3";
    address6 = "fe80::604:3";
    multiprotocol = "ipv6";
    wireguard = {
      listenPort = 2337;
      endpoint = "de1.dn42.cas7.moe:34699";
      publicKey = "1dJpFLegKHKButkXqbv1KLLMTmS6KtFkWBz6GRo2uxE=";
    };
  };

  dc4242420197 = {
    asn = "4242420197";
    description = "https://md.n0emis.eu/s/dn42";
    address4 = "172.20.190.96";
    address6 = "fe80::42:42:1";
    multiprotocol = "ipv6";
    wireguard = {
      listenPort = 24699;
      endpoint = "himalia.dn42.n0emis.eu:24699";
      publicKey = "ObF+xGC6DdddJer0IUw6nzC0RqzeKWwEiQU0ieowzhg=";
    };
  };

  dc4242423804 = {
    asn = "4242423804";
    description = "eXO.cat";
    address4 = "172.20.206.33";
    address6 = "fe80::4242:3804";
    wireguard = {
      listenPort = 2340;
      publicKey = "uAsya5O3yoCQFSpZiDhUwsjWQIaw64//eegHf8D1+ko=";
    };
  };

  dc64738 = {
    asn = "64738";
    description = "welterde";
    # address4 = "";
    address6 = "fe80::fcbb";
    wireguard = {
      listenPort = 2341;
      endpoint = "vpngw.edge0.denue3.welterde.net:41003";
      publicKey = "In8V093CGV1sLK0XZBh86fJvGW1SCVkYbxR5/MJXinM=";
    };
  };

  dc76140 = {
    asn = "76140";
    description = "feuerrot";
    address4 = "172.23.148.1";
    address6 = "fe80::2342";
    wireguard = {
      listenPort = 2342;
      endpoint = "home.dn42.feuerrot.org:51839";
      publicKey = "aoq7ctyAf3P9aohQYLhbFa1LbPMFx93tWYpFRr1CfB4=";
    };
  };
}
