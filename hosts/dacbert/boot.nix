{ config, pkgs, ... }:

{
  boot.loader.external = {
    enable = true;

    installHook = pkgs.writeScript "install-rpi-boot" ''
      #!${pkgs.runtimeShell}
      set -e

      TOPLEVEL=$1

      cp -r ${pkgs.raspberrypifw}/share/raspberrypi/boot/{bootcode.bin,fixup*.dat,start*.elf,overlays} /boot/
      cp ${pkgs.raspberrypi-armstubs}/armstub8-gic.bin $out/armstub8-gic.bin
      cp ${config.system.build.kernel}/Image /boot/kernel8.img
      cp ${config.system.build.initialRamdisk}/initrd /boot/initrd.img
      cp ${config.hardware.deviceTree.package}/broadcom/bcm2711-rpi-4-b.dtb /boot/

      cat << EOF > /boot/config.txt
      kernel=kernel8.img
      initramfs initrd.img followkernel
      arm_64bit=1
      enable_gic=1
      armstub=armstub8-gic.bin
      # Otherwise the resolution will be weird in most cases, compared to
      # what the pi3 firmware does by default.
      disable_overscan=1
      # Supported in newer board revisions
      arm_boost=1
      gpu_mem=256
      dtparam=audio=on
      #dtoverlay=dwc2
      # Pi touch display is attached
      dtoverlay=vc4-kms-v3d
      dtoverlay=vc4-kms-dsi-ili9881-5inch
      # disable uart, enable bluetooth
      enable_uart=0
      #hdmi_group=1
      #hdmi_drive=2
      # Fix booting with no HDMI plugged in:
      hdmi_force_hotplug=1
      display_auto_detect=1
      EOF

      echo "init=$TOPLEVEL/init $(cat $TOPLEVEL/kernel-params)" > /boot/cmdline.txt
    '';
  };

  boot.kernelParams = [
    "video=DSI-1:720x1280M@60"
    "fbcon=rotate:1"
  ];
}
