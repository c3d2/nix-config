{ config, lib, libC, pkgs, zentralwerk, ... }:

{
  c3d2.deployment = {
    # /tmp is to small for drone to clone the repo even with depth
    mounts = lib.mkOptionDefault [ "tmp" ];
    nixStoreBackend = "blk";
    server = "server10";
  };

  microvm = {
    # Running on server10 which has 40 threads on 20 cores
    vcpu = 6;
    # drone-ssh-runner clones the git repo into tmpfs which requires some RAM
    mem = 2560;
    balloonMem = 1 * 1024;
  };

  # drone-ssh-runner clones into /tmp which needs to be bigger than the default rootfs tmpfs
  boot.tmp = {
    useTmpfs = true;
    tmpfsSize = "90%";
  };

  networking = {
    hostName = "c3d2-web";
    firewall.allowedTCPPorts = [
      23 # telme10
      1965 # gemini
    ];
  };

  security.acme.certs = {
    # agate cannot load modern crypto like "ec256" keys
    # TODO: consider dropping this with 25.05 if it doesn't change...
    "www.c3d2.de".keyType = "rsa4096";
  };

  services = {
    # Gemini server
    agate = {
      enable = true;
      addresses = [
        # sysctl net.ipv6.bindv6only = 0
        "[::]:1965"
      ];
      certificatesDir = "/var/lib/agate/certificates";
      contentDir = "/var/www/gemini";
      language = "de";
    };

    nginx = {
      enable = true;
      commonHttpConfig = /* nginx */ ''
        server_names_hash_bucket_size 128;
      '';
      upstreams.c3d2-web = {
        servers."c3d2.de:443" = { };
        extraConfig = /* nginx */ ''
          keepalive 2;
        '';
      };
      virtualHosts = {
        "www.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
          listen = libC.defaultListen;
          locations."/".return = "307 https://c3d2.de$request_uri";
          serverAliases = [
            "c3dd.de" "www.c3dd.de"
            "cccdd.de" "www.cccdd.de"
            "dresden.ccc.de" "www.dresden.ccc.de"
            "netzbiotop.org" "www.netzbiotop.org"
          ];
        };

        "c3d2deccoolwjnqsuy6e2f5f7d75nxnib5o6o75pekzc23zjteojkoad.onion" = {
          forceSSL = false;
          locations."/".proxyPass = "https://c3d2-web";
          extraConfig = /* nginx */ ''
            more_set_headers Strict-Transport-Security "";
          '';
        };

        "c3d2.de" = {
          default = true;
          enableACME = true;
          forceSSL = true;
          listen = libC.defaultListen;
          locations = {
            "/".root = "/var/www/c3d2";

            # Mastodon
            "~ ^/\\.well-known/webfinger".return = "301 https://c3d2.social/.well-known/webfinger?resource=acct%3ac3d2%40c3d2.social";

            # Matrix
            "= /.well-known/matrix/client" = {
              alias = pkgs.writeText "well-known-matrix-client.json" (builtins.toJSON {
                "m.homeserver".base_url = "https://matrix.c3d2.de";
              });
              extraConfig = /* nginx */ ''
                more_set_headers "Content-Type application/json";
                more_set_headers "Access-Control-Allow-Origin: *";
              '';
            };
            "= /.well-known/matrix/server" = {
              alias = pkgs.writeText "well-known-matrix-server.json" (builtins.toJSON {
                "m.server" = "matrix.c3d2.de:443";
              });
              extraConfig = /* nginx */ ''
                more_set_headers "Content-Type application/json";
                more_set_headers "Access-Control-Allow-Origin: *";
              '';
            };

            "/index.php".extraConfig = /* nginx */ ''
              if ($arg_id) {
                return 307 /index.php/id=$arg_id;
              }
            '';

            "~ ^/index\\.php/id=".extraConfig = /* nginx */ ''
              more_set_headers "Content-Type text/html";
            '';

            "~ ^/schule$".return = "307 /schule/";
            "/schule/" = {
              alias = "/var/www/cms-slides/";
              extraConfig = ''
                index index.html;
              '';
            };

            # SpaceAPI
            "/status.png".proxyPass = "http://[${libC.hostRegistry.spaceapi.ip6}]:3000/status.png";
            "/spaceapi.json".proxyPass = "http://[${libC.hostRegistry.spaceapi.ip6}]:3000/spaceapi.json";
          };
          extraConfig = /* nginx */ ''
            index portal.html index.html;

            more_set_headers 'Onion-Location: http://c3d2deccoolwjnqsuy6e2f5f7d75nxnib5o6o75pekzc23zjteojkoad.onion$request_uri';
          '';
        };

        "autotopia.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
          listen = libC.defaultListen;
          locations."/".root = "/var/www/c3d2/autotopia";
          extraConfig = ''
            index index.html;
            rewrite ^/$ /2020/ redirect;
          '';
        };

        "chaosmachtschule.de" = {
          enableACME = true;
          forceSSL = true;
          listen = libC.defaultListen;
          locations."/".return = "307 https://c3d2.de/schule.html";
        };

        "datenspuren.de" = {
          enableACME = true;
          forceSSL = true;
          listen = libC.defaultListen;
          locations = {
            "/".root = "/var/www/c3d2/datenspuren";
            # Mastodon
            "~ ^/.well-known/webfinger".return = "301 https://c3d2.social/.well-known/webfinger?resource=acct%3adatenspuren%40c3d2.social";
          };
          serverAliases = [
            "ds.c3d2.de" "datenspuren.c3d2.de" "www.datenspuren.de"
          ];
          extraConfig = /* nginx */ ''
            index index.html;
            rewrite ^/$ /2025/ redirect;
          '';
        };

        "openpgpkey.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
          listen = libC.defaultListen;
          locations = {
            "/".return = "307 /.well-known/openpgpkey/";
            # WKD: Web Key Directory for PGP Keys
            "~ ^/.well-known/openpgpkey/".extraConfig = ''
              autoindex off;
              default_type  "application/octet-stream";
              add_header    Access-Control-Allow-Origin "* always";
            '';
          };
        };

        "taler.datenspuren.de" = {
          enableACME = true;
          forceSSL = true;
          listen = libC.defaultListen;
          locations."/".return = "307 https://datenspuren.de/2024/taler/index.html";
        };

        "zentralwerk.org" = rec {
          forceSSL = true;
          enableACME = true;
          listen = libC.defaultListen;
          locations = {
            "/".root = "${zentralwerk.packages.${pkgs.stdenv.system}.network-homepage}/share/doc/network-homepage/www";
            "=/captive.json" = locations."/" // {
              extraConfig = ''
                types { application/captive+json json; }
              '';
            };
          };
          serverAliases = [ "www.zentralwerk.org" ];
        };
      };
    };

    tor = {
      enable = true;
      client.enable = true;
      relay.onionServices."c3d2.de".map = [ 80 ];
      settings = {
        ClientUseIPv6 = true;
        HardwareAccel = true;
        Sandbox = true;
      };
    };
  };

  system.stateVersion = "22.05";

  systemd = {
    packages = with pkgs; [ telme10 ];
    services = {
      # lets agate access the tls certs
      agate = {
        requires = [ "agate-keys.service" ];
        after = [ "agate-keys.service" ];
        serviceConfig = {
          Group = "keys";
        };
      };
      agate-keys = {
        path = with pkgs; [ openssl ];
        script =
          let
            stateDir = "/var/lib/agate/certificates";
          in
          /* bash */ ''
            mkdir -p ${stateDir}
            openssl x509 \
              -in /var/lib/acme/www.c3d2.de/cert.pem \
              -out ${stateDir}/cert.der \
              -outform DER
            openssl rsa \
              -in /var/lib/acme/www.c3d2.de/key.pem \
              -out ${stateDir}/key.der \
              -outform DER
            chown root:keys ${stateDir}/*
            chmod 0640 ${stateDir}/*
          '';
        serviceConfig = {
          Type = "oneshot";
        };
      };
      telme10 = {
        serviceConfig.AmbientCapabilities = "CAP_NET_BIND_SERVICE";
      };
    };

    sockets.telme10.wantedBy = [ "sockets.target" ];
  };

  users = {
    groups = {
      c3d2-web = { };
      telme10 = { };
    };
    users = {
      c3d2-web = {
        group = "c3d2-web";
        home = "/var/lib/c3d2-web";
        isSystemUser = true;
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHIkIN1gi5cX2wV2WuNph/QzVK7vvYkvqnR/P69s36mZ drone@c3d2"
        ];
        packages = with pkgs; [
          (stdenv.mkDerivation {
            pname = "atomic-rsync";
            inherit (rsync) version src meta;

            dontBuild = true;
            dontConfigure = true;

            buildInputs = [ python3 ];

            installPhase = ''
              substituteInPlace support/atomic-rsync \
                --replace-fail /usr/bin/rsync rsync

              install -Dm755 support/atomic-rsync -t $out/bin
            '';
          })
          (libxslt.override { cryptoSupport = true; })
          libxml2
          rsync
          gnumake
        ];
        # otherwise the the drone ssh runner cannot log in
        useDefaultShell = true;
      };
      telme10 = {
        isSystemUser = true;
        group = "telme10";
      };
    };
  };

  systemd.tmpfiles.settings.c3d2-web = with config.users.users.c3d2-web; {
    "/var/lib/tor/onion/c3d2.de".d = {
      group = "tor";
      mode = "0700";
      user = "tor";
    };
    "/var/lib/tor/onion/c3d2.de/hostname"."C+" = {
      argument = config.sops.secrets."c3d2-web/tor/hostname".path;
      group = "tor";
      mode = "0400";
      user = "tor";
    };
    "/var/lib/tor/onion/c3d2.de/hs_ed25519_public_key"."C+" = {
      argument = config.sops.secrets."c3d2-web/tor/public-key".path;
      group = "tor";
      mode = "0400";
      user = "tor";
    };
    "/var/lib/tor/onion/c3d2.de/hs_ed25519_secret_key"."C+" = {
      argument = config.sops.secrets."c3d2-web/tor/secret-key".path;
      group = "tor";
      mode = "0400";
      user = "tor";
    };
    "/var/www/c3d2".d = {
      inherit group;
      mode = "0755";
      user = "c3d2-web";
    };
    ${config.services.agate.contentDir}.d = {
      inherit group;
      mode = "0755";
      user = "c3d2-web";
    };
    ${home}.d = {
      inherit group;
      mode = "0700";
      user = "c3d2-web";
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "c3d2-web/gitea-token".owner = "c3d2-web";
      "c3d2-web/tor/hostname" = { };
      "c3d2-web/tor/public-key" = {
        format = "binary";
        sopsFile = ./hs_ed25519_public_key;
      };
      "c3d2-web/tor/secret-key" = {
        format = "binary";
        sopsFile = ./hs_ed25519_secret_key;
      };
    };
  };

  systemd.services.tor = {
    after = [ "systemd-tmpfiles-setup.service" ];
    requires = [ "systemd-tmpfiles-setup.service" ];
  };
}
