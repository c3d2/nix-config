{ config, lib, libC, pkgs, ... }:

let
  cfg = config.services.mediawiki;
in
{
  assertions = [
    {
      assertion = lib.versions.majorMinor pkgs.mediawiki.version == "1.42";
      # https://www.mediawiki.org/wiki/Version_lifecycle
      message = "Please keep mediawiki on LTS versions which is required by the LDAP extension";
    }
  ];
  c3d2 = {
    deployment.server = "server10";
    sendmail = {
      enable = true;
      name = "Wiki";
    };
  };

  microvm.mem = 1536;

  networking = {
    firewall.allowedTCPPorts = [ 80 443 ];
    hostName = "mediawiki";
  };

  services = {
    backup.paths = [ "/var/lib/mediawiki/uploads/" ];

    mediawiki = {
      enable = true;
      database.type = "postgres";

      # see https://extdist.wmflabs.org/dist/extensions/ for list of extensions
      # save them on https://web.archive.org/save and copy the final URL below
      extensions = {
        LDAPAuthorization = pkgs.fetchzip {
          url = "https://web.archive.org/web/20241114225916/https://extdist.wmflabs.org/dist/extensions/LDAPAuthorization-REL1_41-97fa73a.tar.gz";
          hash = "sha256-WwceT1gQkvrSMGNsEmDGpqgtLHDhOUwDZ+uL5497FWE=";
        };
        LDAPUserInfo = pkgs.fetchzip {
          url = "https://web.archive.org/web/20241123213859/https://extdist.wmflabs.org/dist/extensions/LDAPUserInfo-REL1_41-3dff331.tar.gz";
          hash = "sha256-SKAjKQ2e9yCsRcFpgpWwVamQFSeZdOdj034xTGm6rAc=";
        };
        Lockdown = pkgs.fetchzip {
          url = "https://web.archive.org/web/20241110163215/https://extdist.wmflabs.org/dist/extensions/Lockdown-REL1_41-3b9656f.tar.gz";
          hash = "sha256-4PYJ/GK435wbp6XfWy2oc72T/JtZTGhPMF3zmvkD2Vg=";
        };
        # TODO: replace with https://www.mediawiki.org/wiki/Extension:DynamicPageList3
        intersection = pkgs.fetchzip {
          url = "https://web.archive.org/web/20241110172510/https://extdist.wmflabs.org/dist/extensions/intersection-REL1_41-f411b92.tar.gz";
          hash = "sha256-Frh63rQ5RvIjwsaWk3lP5pe0xQf3/4GBLFEotSaz7e8=";
        };
        VisualEditor = pkgs.fetchzip {
          url = "https://web.archive.org/web/20250103165403if_/https://extdist.wmflabs.org/dist/extensions/VisualEditor-REL1_42-c3f3a06.tar.gz";
          hash = "sha256-EpXU3XQK9/BiHF9vnwCyigJYZYGrL2pXCnMZipZjPyc=";
        };
      };
      name = "C3D2";
      nginx.hostName = "wiki.c3d2.de";
      package = pkgs.php81.buildComposerProject2 rec {
        pname = "mediawiki-pre-full";
        inherit (pkgs.mediawiki) version patches postPatch;

        src = pkgs.applyPatches {
          inherit (pkgs.mediawiki) src;
          postPatch = ''
            cp ${./composer.local.json} composer.local.json
            cp ${./composer.lock} composer.lock
            cp ${./fix-patch-applying.diff} fix-patch-applying.diff

            PATH=$PATH:${lib.makeBinPath (with pkgs; [ jq moreutils php81Packages.composer ])}

            composer config --no-plugins allow-plugins.cweagans/composer-patches true
            composer config --no-plugins preferred-install source

            jq \
              '.extra.patches."mediawiki/semantic-media-wiki"."fix patch applying"="fix-patch-applying.diff" |
              .extra.patches."mediawiki/semantic-media-wiki"."MediaWiki 1.43 compatibility part 1"="https://github.com/SemanticMediaWiki/SemanticMediaWiki/pull/5715.patch" |
              .extra.patches."mediawiki/semantic-media-wiki"."MediaWiki 1.43 compatibility part 2"="https://github.com/SemanticMediaWiki/SemanticMediaWiki/pull/5733.patch"' \
              composer.json | sponge composer.json
          '';
        };

        composerVendor = pkgs.php81.mkComposerVendor {
          inherit patches pname src vendorHash version;
          composerNoPlugins = false;
          postInstall = ''
            # for some reason beyond me, the composer.local.json files are not being vendored
            cp -r extensions $out/

            find $out/ -name .git -type d -prune -exec rm -rf {} \;;
          '';
        };
        vendorHash = "sha256-Mbsm2D33Mt1/8voYJ/UnmCMInxqnU30eHdQZEfjgfw4=";

        postInstall = /* bash */ ''
          mv $out/share/{php/mediawiki-pre-full,mediawiki}/
          rm -r $out/share/php

          # https://github.com/NixOS/nixpkgs/blob/master/pkgs/servers/web-apps/mediawiki/default.nix#L21-L23
          echo "<?php
            return require(getenv('MEDIAWIKI_CONFIG'));
          ?>" > $out/share/mediawiki/LocalSettings.php
        '';
      };
      # skins = {
      #   Vector = "${config.services.mediawiki.package}/share/mediawiki/skins/Vector";
      #   Hector = "${config.services.mediawiki.package}/share/mediawiki/skins/Hector";
      # };
      # initial admin user password
      passwordFile = config.sops.secrets."mediawiki/adminPassword".path;
      uploadsDir = "/var/lib/mediawiki/uploads";
      webserver = "nginx";
      extraConfig = /* php */ ''
        $wgAllowExternalImages = true;
        $wgAllowUserCss = true;
        $wgCacheEpoch = "20250204194314";
        $wgParserCacheExpireTime = "${toString (60 * 60 * 4)}";
        $wgDBmwschema = "mediawiki";
        $wgDefaultSkin = "vector";
        $wgEmergencyContact = "root@c3d2.de";
        $wgEnableAPI = true;
        $wgEnableMWSuggest = true;
        $wgFavicon = "https://c3d2.de/favicon.ico";
        $wgFileExtensions = array_merge(
            $wgFileExtensions, [ 'svg' ]
        );
        $wgLanguageCode = "de";
        $wgLogos = [
          '1x' => "https://www.c3d2.de/images/ck.svg",
          '1.5x' => "https://www.c3d2.de/images/ck.svg",
          '2x' => "https://www.c3d2.de/images/ck.svg",
          'icon' => "https://www.c3d2.de/images/ck.svg",
        ];
        // ONLY FOR DEBUGGING!!!!
        // $wgShowExceptionDetails = true;
        $wgUseAjax = true;
        $wgPasswordSender   = "noreply@c3d2.de";

        $wgGroupPermissions['*']['edit'] = false;
        $wgGroupPermissions['user']['edit'] = true;
        $wgGroupPermissions['sysop']['userrights'] = true;

        $wgNamespacesToBeSearchedDefault[NS_TALK] = true;
        $wgNamespacesWithSubpages[NS_MAIN] = true;

        define("NS_INTERN", 100);
        define("NS_INTERN_TALK", 101);
        $wgExtraNamespaces[NS_INTERN] = "Intern";
        $wgExtraNamespaces[NS_INTERN_TALK] = "Intern_Diskussion";
        $wgNamespacesToBeSearchedDefault[NS_INTERN] = true;
        $wgNamespacesToBeSearchedDefault[NS_INTERN_TALK] = true;
        $wgNamespacesWithSubpages[NS_INTERN] = true;
        $wgNamespacesWithSubpages[NS_INTERN_TALK] = true;
        $wgGroupPermissions['intern']['move'] = true;
        $wgGroupPermissions['intern']['move-subpages']    = true;
        $wgGroupPermissions['intern']['move-rootuserpages'] = true; // can move root userpages
        $wgGroupPermissions['intern']['read'] = true;
        $wgGroupPermissions['intern']['edit'] = true;
        $wgGroupPermissions['intern']['createpage'] = true;
        $wgGroupPermissions['intern']['createtalk'] = true;
        $wgGroupPermissions['intern']['writeapi'] = true;
        $wgGroupPermissions['intern']['upload'] = true;
        $wgGroupPermissions['intern']['reupload'] = true;
        $wgGroupPermissions['intern']['reupload-shared'] = true;
        $wgGroupPermissions['intern']['minoredit'] = true;
        $wgGroupPermissions['intern']['purge'] = true; // can use ?action=purge without clicking "ok"
        $wgGroupPermissions['intern']['sendemail'] = true;
        $wgNamespacePermissionLockdown[NS_INTERN]['*'] = array('intern');
        $wgNamespacePermissionLockdown[NS_INTERN_TALK]['*'] = array('intern');

        define("NS_I4R", 102);
        define("NS_I4R_TALK", 103);
        $wgExtraNamespaces[NS_I4R] = "IT4Refugees";
        $wgExtraNamespaces[NS_I4R_TALK] = "IT4Refugees_Diskussion";
        $wgNamespacesToBeSearchedDefault[NS_I4R] = true;
        $wgNamespacesToBeSearchedDefault[NS_I4R_TALK] = true;
        $wgNamespacesWithSubpages[NS_I4R] = true;
        $wgNamespacesWithSubpages[NS_I4R_TALK] = true;
        $wgGroupPermissions['i4r']['move']             = true;
        $wgGroupPermissions['i4r']['move-subpages']    = true;
        $wgGroupPermissions['i4r']['move-rootuserpages'] = true; // can move root userpages
        $wgGroupPermissions['i4r']['read']             = true;
        $wgGroupPermissions['i4r']['edit']             = true;
        $wgGroupPermissions['i4r']['createpage']       = true;
        $wgGroupPermissions['i4r']['createtalk']       = true;
        $wgGroupPermissions['i4r']['writeapi']         = true;
        $wgGroupPermissions['i4r']['upload']           = true;
        $wgGroupPermissions['i4r']['reupload']         = true;
        $wgGroupPermissions['i4r']['reupload-shared']  = true;
        $wgGroupPermissions['i4r']['minoredit']        = true;
        $wgGroupPermissions['i4r']['purge']            = true; // can use ?action=purge without clicking "ok"
        $wgGroupPermissions['i4r']['sendemail']        = true;
        $wgNamespacePermissionLockdown[NS_I4R]['*'] = array('i4r');
        $wgNamespacePermissionLockdown[NS_I4R_TALK]['*'] = array('i4r');

        $wgGroupPermissions['sysop']['deletelogentry'] = true;
        $wgGroupPermissions['sysop']['deleterevision'] = true;

        wfLoadExtension('ConfirmEdit/QuestyCaptcha');
        $wgCaptchaClass = 'QuestyCaptcha';
        $wgCaptchaQuestions[] = array('question' => 'How is C3D2 logo in ascii?', 'answer' => '<<</>>');

        wfLoadExtension('Interwiki');
        $wgGroupPermissions['sysop']['interwiki'] = true;

        wfLoadExtension('Cite');
        wfLoadExtension('CiteThisPage');
        wfLoadExtension('ConfirmEdit');
        wfLoadExtension('ParserFunctions');
        wfLoadExtension('SyntaxHighlight_GeSHi');
        wfLoadExtension('WikiEditor');

        // TODO: what about $wgUpgradeKey ?

        // TODO: does this even work?
        // https://www.mediawiki.org/wiki/Extension:Scribunto#Requirements mentions quite some extra steps which we didn't do
        wfLoadExtension('Scribunto');
        $wgScribuntoDefaultEngine = 'luastandalone';

        # LDAP
        wfLoadExtension('LDAPAuthentication2');
        wfLoadExtension('LDAPGroups');
        wfLoadExtension('LDAPProvider');
        wfLoadExtension('PluggableAuth');
        $LDAPProviderDomainConfigs = "${config.sops.secrets."mediawiki/ldapprovider".path}";
        $wgPluggableAuth_Config['Log In with LDAP'] = [
          'plugin' => 'LDAPAuthentication2',
          'data' => [
            'domain' => 'LDAP',
          ],
        ];
        $wgPluggableAuth_EnableLocalLogin = true;

        # SemanticMediaWiki
        wfLoadExtension('SemanticMediaWiki');
        # TODO: expose https://github.com/NixOS/nixpkgs/blob/nixos-unstable/nixos/modules/services/web-apps/mediawiki.nix#L19
        $smwgConfigFileDir = "/var/lib/mediawiki";
        # default was 100 which is already occupied, using 200 to 215
        # https://www.semantic-mediawiki.org/wiki/Help:$smwgNamespaceIndex
        $smwgNamespaceIndex = 200;
        enableSemantics('${config.services.mediawiki.nginx.hostName}');
        $smwgURITypeSchemeList = array_merge(
          $smwgURITypeSchemeList, [
            'xmpp', 'mumble', 'ssh'
          ]
        );

        # VisualEditor
        $wgDefaultUserOptions['visualeditor-enable'] = 1;
        $wgDefaultUserOptions['visualeditor-newwikitext'] = 1;
        $wgGroupPermissions['user']['writeapi'] = true;
        $wgVisualEditorAvailableNamespaces = [
         'Intern' => true
       ];
      '';
    };

    nginx = {
      enable = true;
      commonHttpConfig = /* nginx */ ''
        # for some reason nginx adds a port for the 301 redirect from / to /wiki/
        port_in_redirect off;
      '';
      virtualHosts."${config.services.mediawiki.nginx.hostName}" = {
        enableACME = true;
        forceSSL = true;
        listen = libC.defaultListen;
        locations = {
          "/".extraConfig = lib.mkForce /* nginx */ ''
            return 307 /wiki$request_uri;
          '';
          # https://www.semantic-mediawiki.org/wiki/Help:Reduce_server_load_with_a_robots.txt_file
          "=/robots.txt".alias = pkgs.writeText "robots.txt" ''
            User-agent: *
            Disallow: /w/
            Disallow: /wiki/MediaWiki:
            Disallow: /wiki/MediaWiki%3A
            Disallow: /wiki/Special:
            Disallow: /wiki/Special%3A
          '';
        };
      };
    };

    phpfpm = {
      phpPackage = pkgs.php.buildEnv {
        extensions = { all, enabled }: enabled ++ (with all; [ apcu ]);
      };
      phpOptions = /* ini */ ''
        post_max_size = "100M"
        # see https://github.com/nixos/nixpkgs/blob/master/nixos/modules/services/web-apps/nextcloud.md
        realpath_cache_size = "0"
        upload_max_filesize = "100M"
      '';
    };

    postgresql = {
      enable = true;
      ensureDatabases = [ cfg.database.name ];
      ensureUsers = [{
        name = cfg.database.user;
        ensureDBOwnership = true;
      }];
      package = pkgs.postgresql_17;
    };
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "mediawiki/adminPassword".owner = config.systemd.services.mediawiki-init.serviceConfig.User;
      "mediawiki/ldapprovider" = {
        mode = "440";
        owner = config.systemd.services.mediawiki-init.serviceConfig.User;
      };
      "mediawiki/secretKey" = {
        mode = "440";
        owner = config.systemd.services.mediawiki-init.serviceConfig.User;
        path = "/var/lib/mediawiki/secret.key";
      };
      "mediawiki/upgradeKey".owner = config.systemd.services.mediawiki-init.serviceConfig.User;
    };
  };

  system.stateVersion = "22.05";

  systemd.services.mediawiki-init = {
    after = [ "postgresql.service" ];
    requires = [ "postgresql.service" ];
  };
}
