# MediaWiki

## How to update

If MediaWiki is updated, the `composer.lock` needs to be updated, too.
This can be done by running the following commands

```bash
nix build .#nixosConfigurations.mediawiki.config.services.mediawiki.package.src
cp --no-preserve=all result/composer.json .
composer update
```

and then updating the vendorHash by trying to deploy once.
