{ config, lib, ... }:

{
  services.prometheus.exporters.blackbox = {
    dnsProbe.c3d2 = {
      names = [ "c3d2.de" "c3d2.social" "c3d2.space" "c3dd.de" "cccdd.de" "datenspuren.de" "dresden.ccc.de" "hq.c3d2.de" "netzbiotop.org" "pentamedia.org" ];
      targets = (if config.services.resolved.enable then [ "127.0.0.53" ] else lib.head config.networking.nameservers)
        ++ /* targets */ [ "ns.c3d2.de." "ns.spaceboyz.net." "ns1.supersandro.de." ];
      type = "SOA";
    };

    httpProbe = rec {
      c3d2_http = {
        statusCode = [ 301 ];
        urls = /* http */ [ "http://auth.c3d2.de" "http://blogs.c3d2.de" "http://relay.fedi.buzz" "http://autotopia.c3d2.de" "http://c3d2.de" "http://datenspuren.de" "http://openpgpkey.c3d2.de" "http://taler.datenspuren.de" "http://www.c3d2.de" "http://www.datenspuren.de" "http://zentralwerk.org" "http://fedi.buzz" "http://drone.c3d2.de" "http://engel.datenspuren.de" "http://ftp.c3d2.de" "http://gitea.c3d2.de" "http://grafana.c3d2.de" "http://codimd.c3d2.de" "http://hackmd.c3d2.de" "http://hedgedoc.c3d2.de" "http://c3d2.social" "http://element.c3d2.de" "http://matrix.c3d2.de" "http://media.c3d2.de" "http://wiki.c3d2.de" "http://code.nixos.c3d2.de" "http://nixos.c3d2.de" "http://search.nixos.c3d2.de" "http://tracker.nixos.c3d2.de" "http://owncast.c3d2.de" "http://pretalx.c3d2.de" "http://talks.datenspuren.de" "http://sshlog.flpk.zentralwerk.org" "http://ticker.c3d2.de" "http://vaultwarden.c3d2.de" ];
      };

      c3d2_http_v6 = {
        ip = "ip6";
        inherit (c3d2_http) statusCode;
        urls = /* http_v6 */ [ "http://broker.serv.zentralwerk.org" "http://grafana.hq.c3d2.de" "http://home-assistant.hq.c3d2.de" "http://hydra.hq.c3d2.de" "http://matemat.hq.c3d2.de" "http://nixtaler.serv.zentralwerk.org" "http://drkkr.hq.c3d2.de" "http://drucker.hq.c3d2.de" "http://ledfx.hq.c3d2.de" "http://mopidy.hq.c3d2.de" "http://pipebert.hq.c3d2.de" "http://prometheus.serv.zentralwerk.org" "http://scrape.hq.c3d2.de" "http://adsb.hq.c3d2.de" "http://sdr.hq.c3d2.de" "http://stream.hq.c3d2.de" "http://torrents.hq.c3d2.de" ];
      };

      c3d2_https = {
        # some services immeadately redirect to a login page or similar. Also accept those
        statusCode = [ 200 204 301 302 303 307 308 401 403 ];
        urls = /* https */ [ "https://auth.c3d2.de" "https://blogs.c3d2.de" "https://relay.fedi.buzz" "https://autotopia.c3d2.de" "https://c3d2.de" "https://datenspuren.de" "https://openpgpkey.c3d2.de" "https://taler.datenspuren.de" "https://www.c3d2.de" "https://www.datenspuren.de" "https://zentralwerk.org" "https://fedi.buzz" "https://drone.c3d2.de" "https://engel.datenspuren.de" "https://ftp.c3d2.de" "https://gitea.c3d2.de" "https://grafana.c3d2.de" "https://codimd.c3d2.de" "https://hackmd.c3d2.de" "https://hedgedoc.c3d2.de" "https://c3d2.social" "https://element.c3d2.de" "https://matrix.c3d2.de" "https://media.c3d2.de" "https://wiki.c3d2.de" "https://code.nixos.c3d2.de" "https://nixos.c3d2.de" "https://search.nixos.c3d2.de" "https://tracker.nixos.c3d2.de" "https://owncast.c3d2.de" "https://pretalx.c3d2.de" "https://talks.datenspuren.de" "https://sshlog.flpk.zentralwerk.org" "https://ticker.c3d2.de" "https://vaultwarden.c3d2.de" ];
      };

      c3d2_https_v6 = {
        ip = "ip6";
        inherit (c3d2_https) statusCode;
        urls = /* https_v6 */ [ "https://broker.serv.zentralwerk.org" "https://grafana.hq.c3d2.de" "https://home-assistant.hq.c3d2.de" "https://hydra.hq.c3d2.de" "https://matemat.hq.c3d2.de" "https://nixtaler.serv.zentralwerk.org" "https://drkkr.hq.c3d2.de" "https://drucker.hq.c3d2.de" "https://ledfx.hq.c3d2.de" "https://mopidy.hq.c3d2.de" "https://pipebert.hq.c3d2.de" "https://prometheus.serv.zentralwerk.org" "https://scrape.hq.c3d2.de" "https://adsb.hq.c3d2.de" "https://sdr.hq.c3d2.de" "https://stream.hq.c3d2.de" "https://torrents.hq.c3d2.de" ];
      };
    };
  };
}
