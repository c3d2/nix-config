{ config, lib, ... }:

{
  services.prometheus = {
    exporters.blackbox = {
      config.modules = rec {
        # mail
        # inspired by https://github.com/prometheus/blackbox_exporter/blob/master/example.yml#L109-L133
        tcp_imap_starttls = {
          prober = "tcp";
          tcp.query_response = [
            { expect = "OK.*STARTTLS"; }
            { send = ". STARTTLS"; }
            { expect = "OK"; }
            { starttls = true; }
            { send = ". capability"; }
            { expect = "CAPABILITY IMAP4rev1"; }
          ];
          timeout = "10s";
        };
        tcp_smtp_starttls = {
          prober = "tcp";
          tcp.query_response = [
            { expect = "^220 ([^ ]+) ESMTP (.+)$"; }
            { send = "EHLO prober\r"; }
            { expect = "^250-STARTTLS"; }
            { send = "STARTTLS\r"; }
            { expect = "^220"; }
            { starttls = true; }
            { send = "EHLO prober\r"; }
            { expect = "^250-AUTH"; }
            { send = "QUIT\r"; }
          ];
          timeout = "10s";
        };

        # firewall blocks ldaps
        tcp_ldaps = {
          tcp = {
            ip_protocol_fallback = false;
            preferred_ip_protocol = "ip4";
          };
          prober = "tcp";
          timeout = "10s";
        };
        tcp_ldaps_ip6 = lib.recursiveUpdate tcp_ldaps {
          tcp.preferred_ip_protocol = "ip6";
        };
      };

      # those are not resolable on the public internet
      dnsProbe.c3d2_ffd_dn42 = {
        # TODO: add c3d2.dn42 when in use
        names = [ /*"c3d2.dn42"*/ "c3d2.ffdd" "zentralwerk.dn42" "zentralwerk.ffdd" ];
        targets = [ "ns.c3d2.de." "ns.spaceboyz.net." "ns1.supersandro.de." ];
        type = "SOA";
      };

      httpProbe = {
        # move to generated when adding more than this lonely ipv4 only host
        mail_http = {
          ip = "ip4";
          statusCode = [ 301 ];
          urls = [ "http://autoconfig.c3d2.de" "http://autodiscover.c3d2.de" "http://lists.c3d2.de" "http://mail.c3d2.de" "http://mail.flpk.zentralwerk.org" "http://rspamd.c3d2.de" ];
        };
        mail_https = {
          ip = "ip4";
          statusCode = [ 200 301 307 308 ];
          urls = [ "https://autoconfig.c3d2.de" "https://autodiscover.c3d2.de" "https://lists.c3d2.de" "https://mail.c3d2.de" "https://mail.flpk.zentralwerk.org" "https://rspamd.c3d2.de" ];
        };

        # check that matemat's basic auth is working
        matemat = {
          statusCode = [ 401 ];
          urls = [
            "https://mate.c3d2.de"
            "https://matemat.c3d2.de"
          ];
        };
        matemat_hq = {
          statusCode = [ 401 ];
          # ip4 is private ip
          ip = "ip6";
          urls = [ "https://matemat.hq.c3d2.de" ];
        };
      };
    };

    rulesConfig = [ {
      groups = [ {
        name = "c3d2-blackbox-exporter";
        rules = [ {
          alert = "c3d2_probe";
          expr = ''probe_success{job=~"blackbox_(dns|http)_c3d2.*"} == 0'';
          for = "5m";
          labels = {
            alertname = "probe failing";
            severity = "error";
            team = "c3d2";
          };
          annotations.description = "Probe for {{ $labels.instance }} is failing";
        } {
          alert = "c3d2_ldaps_not_dropped";
          expr = ''probe_success{instance="auth.serv.zentralwerk.org:636"} == 1'';
          for = "5m";
          labels = {
            alertname = "ldaps port not being dropped";
            severity = "error";
            team = "c3d2";
          };
          annotations.description = "Port 636 for auth.serv.zentralwerk.org via IP6 is no longer being dropped";
        } {
          alert = "c3d2_matemat_no_basic_auth";
          expr = ''probe_success{instance=~"https://mate.*.c3d2.de",job=~"blackbox_http_matemat.*"} == 0'';
          for = "5m";
          labels = {
            alertname = "Matemat basic auth on {{ $labels.instance }}";
            severity = "error";
            team = "c3d2";
          };
          annotations.description = "Matemat via {{ $labels.instance }} does not have basic auth";
        } ];
      } ];
    } ];

    scrapeConfigs = let
      common = {
        metrics_path = "/probe";
        relabel_configs = [ {
          source_labels = [ "__address__" ];
          target_label = "__param_target";
        } {
          source_labels = [ "__param_target" ];
          target_label = "instance";
        } {
          target_label = "__address__";
          replacement = config.services.prometheus.exporters.blackbox.blackboxExporterURL;
        } ];
      };
    in [ (common // {
      # ip4 is private ip
      job_name = "blackbox_tcp_c3d2_ldaps_drop_ip6";
      params.module = [ "tcp_ldaps_ip6" ];
      static_configs = [ {
        targets = [ "auth.serv.zentralwerk.org:636" ];
      } ];
    }) (common // {
      job_name = "blackbox_tcp_c3d2_imap";
      params.module = [ "tcp_imap_starttls" ];
      static_configs = [ {
        targets = [ "mail.flpk.zentralwerk.org:993" ];
      } ];
    }) (common // {
      job_name = "blackbox_tcp_c3d2_smtp";
      params.module = [ "tcp_smtp_starttls" ];
      static_configs = [ {
        targets = [ "mail.flpk.zentralwerk.org:456" ];
      } ];
    }) ];
  };
}
