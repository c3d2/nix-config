#!/usr/bin/env bash
# shellcheck disable=SC2016

set -eou pipefail
cd "$(dirname "$0")" || exit 2

domains="$(nix eval .#self --apply 'self: let inherit (self.nixosConfigurations.auth.pkgs) lib; in lib.filter (x: x != "all" && x != "template" && !lib.hasSuffix ".ffdd" x && !lib.hasSuffix ".dn42" x) (lib.attrNames self.inputs.c3d2-dns.outputs.packages.x86_64-linux)')"
targets="$(nix eval .#self --apply 'self: self.inputs.c3d2-dns.outputs.packages.x86_64-linux.template.default.NS')"
urls_http="$(nix eval .#nixosConfigurations --apply 'configs: let inherit (configs.auth.pkgs) lib; in map (x: "http://" + x) (lib.filter (x: x != "_" && x != "localhost" && !lib.hasSuffix ".hq.c3d2.de" x && !lib.hasSuffix ".serv.zentralwerk.org" x && !lib.hasSuffix ".cluster.zentralwerk.org" x && !lib.hasSuffix ".ffdd" x) (lib.flatten (map (x: lib.attrNames configs.${x}.config.services.nginx.virtualHosts) (lib.filter (x: x != "mail") (lib.attrNames configs)))))')"
urls_http_v6="$(nix eval .#nixosConfigurations --apply 'configs: let inherit (configs.auth.pkgs) lib; in map (x: "http://" + x) (lib.filter (x: x != "_" && x != "localhost" && (lib.hasSuffix ".hq.c3d2.de" x || lib.hasSuffix ".serv.zentralwerk.org" x && !lib.hasSuffix ".cluster.zentralwerk.org" x)) (lib.flatten (map (x: lib.attrNames configs.${x}.config.services.nginx.virtualHosts) (lib.filter (x: x != "mail") (lib.attrNames configs)))))')"
urls_https="$(nix eval .#nixosConfigurations --apply 'configs: let inherit (configs.auth.pkgs) lib; in map (x: "https://" + x) (lib.filter (x: x != "_" && x != "localhost" && !lib.hasSuffix ".hq.c3d2.de" x && !lib.hasSuffix ".serv.zentralwerk.org" x && !lib.hasSuffix ".cluster.zentralwerk.org" x && !lib.hasSuffix ".ffdd" x) (lib.flatten (map (x: lib.attrNames configs.${x}.config.services.nginx.virtualHosts) (lib.filter (x: x != "mail") (lib.attrNames configs)))))')"
urls_https_v6="$(nix eval .#nixosConfigurations --apply 'configs: let inherit (configs.auth.pkgs) lib; in map (x: "https://" + x) (lib.filter (x: x != "_" && x != "localhost" && (lib.hasSuffix ".hq.c3d2.de" x || lib.hasSuffix ".serv.zentralwerk.org" x && !lib.hasSuffix ".cluster.zentralwerk.org" x)) (lib.flatten (map (x: lib.attrNames configs.${x}.config.services.nginx.virtualHosts) (lib.filter (x: x != "mail") (lib.attrNames configs)))))')"

sed -i ./blackbox-generated.nix \
  -e "s|names = .*|names = $domains;|" \
  -e "s|targets \*/ .*|targets */ $targets;|" \
  -e "s|urls = /\* http \*/ .*|urls = /\* http \*/ $urls_http;|" \
  -e "s|urls = /\* http_v6 \*/ .*|urls = /\* http_v6 \*/ $urls_http_v6;|" \
  -e "s|urls = /\* https \*/ .*|urls = /\* https \*/ $urls_https;|" \
  -e "s|urls = /\* https_v6 \*/ .*|urls = /\* https_v6 \*/ $urls_https_v6;|"
