{ tftproots, pkgs, ... }:

{
  # raspberrypi boot
  services.atftpd = {
    enable = true;
    root =
      let
        netbootxyzVersion = "2.0.83";
        netbootxyz_efi = pkgs.fetchurl {
          url = "https://github.com/netbootxyz/netboot.xyz/releases/download/${netbootxyzVersion}/netboot.xyz.efi";
          hash = "sha256-VyMha+mtlDBuPAs6HPunpEQswANApB6fFgfOnfXE2H0=";
        };
        netbootxyz_kpxe = pkgs.fetchurl {
          url = "https://github.com/netbootxyz/netboot.xyz/releases/download/${netbootxyzVersion}/netboot.xyz.kpxe";
          hash = "sha256-1JLRn4mmLOLBpPqZZIiqY6SK9AUMNFyS/+pbXqMAv7k=";
        };
      in
      pkgs.runCommand "tftproot" { } ''
        mkdir $out

        # PXE for PC
        ln -s ${netbootxyz_efi} $out/netboot.xyz.efi
        ln -s ${netbootxyz_kpxe} $out/netboot.xyz.kpxe

        # generic boot files for pis
        cp -sr ${tftproots.rpi-netboot-tftproot}/* $out/

        # dacbert
        ln -s /var/lib/nfsroot/dacbert/boot $out/3c271952
      '';
  };
}
