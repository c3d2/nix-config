{ lib, libC, ... }:
{
  # share /nix/store via NFS read-only
  services.nfs.server = {
    enable = true;
    exports =
      let
        allowed = [
          "172.22.99.0/24"
          "172.20.72.0/21"
          "2a00:8180:2c00:200::/56"
          "2a0f:5382:acab:1400::/56"
          "fd23:42:c3d2:500::/56"
        ];
        opts = o: fsid:
          lib.concatStringsSep "," [
            o "async"
            "no_subtree_check" "no_root_squash"
            "fsid=${toString fsid}"
          ];
      in ''
        # ro-store for netbooting Pi4
        /nix/store ${
          lib.concatMapStringsSep " " (subnet:
            "${subnet}(${opts "ro" 1})"
          ) allowed
        }
        # rootfs for 100% nfsroot
        /var/lib/nfsroot/dacbert ${
          lib.concatMapStringsSep " " (subnet:
            "${subnet}(${opts "rw" 2})"
          ) [ "${libC.site.net.c3d2.hosts4.dacbert}/32" ]
        }
        /var/lib/nfsroot/riscbert ${
          lib.concatMapStringsSep " " (subnet:
            "${subnet}(${opts "rw" 3})"
          ) allowed
        }
        # shared space for dump-dvb project
        /var/lib/dump-dvb/whoopsie ${
          lib.concatMapStringsSep " " (subnet:
            "${subnet}(${opts "rw" 4})"
          ) allowed
        }
    '';
  };

  systemd.services.nfs-mountd.requires = [
    "var-lib-nfsroot-riscbert.mount"
    "var-lib-nfsroot-dacbert.mount"
    ''var-lib-dump\x2ddvb-whoopsie.mount''
  ];
}
