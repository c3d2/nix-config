{ config, lib, ... }:

{
  boot.initrd.availableKernelModules = [ "ehci_pci" "ahci" "usb_storage" "usbhid" "sd_mod" ];
  boot.kernelModules = [ "kvm-intel" ];

  boot.initrd.luks.devices."server10-root".device = "/dev/disk/by-uuid/b4904afe-22b3-4abe-bd53-049817c1332f";

  fileSystems."/" =
    { device = "server10-root/root";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/EB9B-15AA";
      fsType = "vfat";
    };

  fileSystems."/etc" =
    { device = "server10-root/data/etc";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/home" =
    { device = "server10-root/home";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/nix" =
    { device = "server10-root/nix";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/nix/store" =
    { device = "server10-root/nix/store";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/nix/var" =
    { device = "server10-root/nix/var";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/var/backup" =
    { device = "server10-root/data/backup";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/var/lib" =
    { device = "server10-root/data/lib";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  networking.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
