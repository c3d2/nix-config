{ lib, libC, pkgs, ... }:

{
  c3d2 = {
    audioServer.enable = true;
    autoUpdate = false;
    interface = "eth0";
    journalToMqtt = false;
    k-ot.enable = true;
    statistics.enable = false;
  };

  boot = {
    # repeat https://github.com/NixOS/nixos-hardware/blob/master/raspberry-pi/4/default.nix#L20
    # to overwrite audio module
    kernelPackages = pkgs.linuxKernel.packages.linux_rpi4;
    kernelParams = [
      "verbose" "console=tty0"
      "zfs_force=1"
    ];
  };

  console.keyMap = "de";

  environment.systemPackages = with pkgs; [
    libraspberrypi
    raspberrypi-eeprom
    iw
    libva-utils
    mpv
    vlc
    yt-dlp
    ncpamixer
    pulseaudio # required for pactl
    chromium
    firefox
    mesa-demos
    pavucontrol
    projectm
    # tracer-game
    bevy_julia
    bevy_mandelbrot
    allcolors
    ffmpeg
    empty-epsilon
  ];

  fileSystems = {
    "/" = {
      fsType = "tmpfs";
      options = [ "mode=0755" ];
    };
    "/etc" = {
      fsType = "tmpfs";
      options = [ "mode=0755" ];
    };
     # mount the server's /nix/store
    "/nix/store" = {
      device = "${libC.hostRegistry.nfsroot.ip4}:/nix/store";
      fsType = "nfs";
      options = [ "nfsvers=3" "proto=tcp" "nolock" "hard" "async" "ro" ];
      neededForBoot = true;
    };
    "/var" = {
      fsType = "tmpfs";
      options = [ "mode=0755" ];
    };
  };

  hardware.bluetooth.enable = true;

  networking = {
    hostName = "rpi-netboot";
    useDHCP = false;
    interfaces.eth0.useDHCP = true;
  };

  nix.gc.automatic = lib.mkForce false;

  powerManagement.cpuFreqGovernor = lib.mkDefault "performance";

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  services = {
    fstrim.enable = true;
    displayManager = {
      defaultSession = "gnome-xorg";
      autoLogin = {
        enable = true;
        user = "k-ot";
      };
    };
    # Do not log to flash
    journald.extraConfig = ''
      Storage=volatile
    '';
    openssh.enable = true;
    xserver = {
      enable = true;
      xkb = {
        layout = "de";
        options = "eurosign:e";
      };
      displayManager.gdm = {
        enable = true;
        wayland = true;
      };
      desktopManager.gnome.enable = true;
    };
  };

  systemd = {
    # r/o /nix/store
    services = {
      nix-daemon.enable = false;
      nix-gc.enable = false;
    };
    sockets.nix-daemon.enable = false;

    user.services.x11vnc = {
      description = "X11 VNC server";
      wantedBy = [ "graphical-session.target" ];
      partOf = [ "graphical-session.target" ];
      serviceConfig = {
        ExecStart = ''
          ${pkgs.x11vnc}/bin/x11vnc -shared -forever -passwd k-ot
        '';
        RestartSec = 3;
        Restart = "always";
      };
    };
  };

  system.stateVersion = "21.11"; # Did you read the comment?
}
