{ lib, ... }:

{
  boot.initrd.availableKernelModules = [ "usbhid" ];
  # mkForce to get rid of "console=ttyAMA0" from sd-image-aarch64.nix
  boot.kernelParams = lib.mkForce [
    "snd_bcm2835.enable_headphones=1"
  ];

  fileSystems."/" = {
    device = "/dev/disk/by-label/NIXOS_SD";
    fsType = "ext4";
  };

  fileSystems."/boot/firmware" = {
    device = "/dev/disk/by-label/FIRMWARE";
    fsType = "vfat";
  };

  hardware.enableRedistributableFirmware = true;
  boot.loader.generic-extlinux-compatible.firmwareConfig = ''
    gpu_mem=192
    dtparam=audio=on
  '';

  powerManagement.cpuFreqGovernor = lib.mkDefault "performance";
}
