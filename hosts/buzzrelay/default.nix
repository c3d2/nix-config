{ config, lib, libC, pkgs, ... }:
let
  buzzrelay = pkgs.buzzrelay.overrideAttrs {
    postPatch = ''
      substituteInPlace static/index.html \
        --replace '</body>' '<script defer data-domain="relay.fedi.buzz" src="https://p.spaceboyz.net/js/script.js"></script></body>'
    '';
  };
in
{
  c3d2 = {
    deployment = {
      server = "server9";
      nixStoreBackend = "blk";
    };
    statistics.enable = true;
  };

  microvm = {
    mem = 8 * 1024;
    vcpu = 8;
  };

  networking.hostName = "buzzrelay";
  # Don't let journald spam the disk
  services.journald.extraConfig = ''
    Storage=volatile
  '';

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "buzzrelay/privKey".owner = config.services.buzzrelay.user;
      "buzzrelay/pubKey".owner = config.services.buzzrelay.user;
      "buzzrelay/redis/password".owner = config.services.buzzrelay.user;
    };
  };

  services = {
    buzzrelay = {
      enable = true;
      package = buzzrelay;
      hostName = "relay.fedi.buzz";
      privKeyFile = config.sops.secrets."buzzrelay/privKey".path;
      pubKeyFile = config.sops.secrets."buzzrelay/pubKey".path;
      redis = {
        connection = "redis://fedi.buzz:6379/";
        passwordFile = config.sops.secrets."buzzrelay/redis/password".path;
      };
    };

    nginx = {
      enable = true;
      recommendedOptimisation = lib.mkForce false;
      virtualHosts."relay.fedi.buzz" = {
        forceSSL = true;
        enableACME = true;
        locations = {
          "/" = {
            proxyPass = "http://127.0.0.1:${toString config.services.buzzrelay.listenPort}/";
            extraConfig = /* nginx */ ''
              access_log off;
              client_max_body_size 64K;
              keepalive_timeout 10;
              proxy_buffering off;
            '';
          };
          "/metrics" = {
            proxyPass = "http://127.0.0.1:${toString config.services.buzzrelay.listenPort}/";
            extraConfig = libC.hqNetworkOnly;
          };
        };
      };
    };

    postgresql = {
      package = pkgs.postgresql_17;
      settings.log_min_duration_statement = 50;
      upgrade.stopServices = [ "buzzrelay" ];
      ensureUsers = [ {
        name = "collectd";
      } ];
    };

    collectd.plugins.postgresql = ''
      <Query unique_followers>
        Statement "select count(distinct id) from follows;"
        <Result>
          Type gauge
          InstancePrefix "unique"
          ValuesFrom "count"
        </Result>
      </Query>
      <Query total_follows>
        Statement "select count(id) from follows;"
        <Result>
          Type gauge
          InstancePrefix "total"
          ValuesFrom "count"
        </Result>
      </Query>

      <Database ${config.networking.hostName}>
        Param database "${config.services.buzzrelay.database}"
        Query unique_followers
        Query total_follows
      </Database>
    '';

    # Magic sauce
    bpftune.enable = true;
  };

  system.stateVersion = "22.11";
}
