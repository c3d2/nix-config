{ config, pkgs, libC, ... }:

{
  c3d2 = {
    deployment.server = "server10";
    sendmail = {
      enable = true;
      name = "Gitea";
    };
  };

  microvm.mem = 4 * 1024;

  networking = {
    hostName = "gitea";
    firewall.allowedTCPPorts = [ 2222 ];
  };

  services = {
    backup = {
      enable = true;
      exclude = [
        "/var/lib/gitea/data/indexers/"
        "/var/lib/gitea/data/repo-archive"
        "/var/lib/gitea/data/queues"
        "/var/lib/gitea/data/tmp/"
      ];
      paths = [ "/var/lib/gitea/" ];
    };

    gitea = {
      enable = true;
      appName = "Gitea: with a cup of Mate";
      database.type = "postgres";
      lfs.enable = true;
      repositoryRoot = "/var/lib/gitea/repositories";

      ldap = {
        enable = true;
        searchUserPasswordFile = config.sops.secrets."gitea/ldapSearchUserPassword".path;
      };

      oidc = {
        enable = true;
        clientSecretFile = config.sops.secrets."gitea/oidcClientSecret".path;
      };

      settings = {
        # we use drone for internal tasks and don't want people to execute code on our infrastructure
        actions.ENABLED = true;
        database.LOG_SQL = false;
        # enable if it is actually useful
        # federation.ENABLED = true;
        indexer.REPO_INDEXER_ENABLED = true;
        mailer = {
          ENABLED = true;
          FROM = "Gitea <noreply@c3d2.de>";
          PROTOCOL = "sendmail";
          SENDMAIL_PATH = "/run/wrappers/bin/sendmail";
          SENDMAIL_ARGS = "--";
        };
        # disabled to prevent us becoming critical infrastructure, might revisit later
        packages.ENABLED = false;
        picture = {
          ENABLE_FEDERATED_AVATAR = true;
          GRAVATAR_SOURCE = "libravatar";
          REPOSITORY_AVATAR_FALLBACK = "random";
        };
        repository.DEFAULT_REPO_UNITS = "repo.code,repo.releases,repo.issues,repo.pulls";
        server = rec {
          DOMAIN = "gitea.c3d2.de";
          PROTOCOL = "http+unix";
          SSH_AUTHORIZED_KEYS_BACKUP = false;
          SSH_DOMAIN = DOMAIN;
        };
        service = {
          DISABLE_REGISTRATION = true;
          ENABLE_NOTIFY_MAIL = true;
          REGISTER_EMAIL_CONFIRM = true;
          USER_LOCATION_MAP_URL = "https://www.openstreetmap.org/search?query=";
        };
        ui = {
          EXPLORE_PAGING_NUM = 25;
          FEED_PAGING_NUM = 50;
          ISSUE_PAGING_NUM = 25;
        };
      };
    };

    gitea-actions.enableRegistrar = true;

    nginx = {
      enable = true;
      commonHttpConfig = /* nginx */ ''
        proxy_headers_hash_bucket_size 64;
      '';
      upstreams.gitea.servers."unix:${config.services.gitea.settings.server.HTTP_ADDR}" = { };
      virtualHosts."gitea.c3d2.de" = {
        forceSSL = true;
        enableACME = true;
        listen = libC.defaultListen;
        locations = {
          "/".proxyPass = "http://gitea";
          "=/robots.txt".alias = let
            giteaComRobotsTxt = pkgs.fetchurl {
              url = "https://gitea.com/robots.txt";
              hash = "sha256-Ys7WPY4+3eWS6RjCuYbwhQ/dF+yWPU8TK0gRRYcujJA=";
            };
          in pkgs.runCommand "robots.txt" { } ''
            cp ${giteaComRobotsTxt} $out
            chmod +w $out
            substituteInPlace $out \
              --replace-fail "Crawl-delay: 2" "Crawl-delay: 10"
            echo "User-agent: AhrefsBot
            Disallow: /
            " >> $out
          '';
        };
      };
    };

    openssh = {
      enable = true;
      extraConfig = /* ssh_config */ ''
        Match User gitea
          AllowAgentForwarding no
          AllowTcpForwarding no
          PermitTTY no
          X11Forwarding no
      '';
    };

    postgresql.package = pkgs.postgresql_17;
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "gitea/ldapSearchUserPassword".owner = "gitea";
      "gitea/oidcClientSecret".owner = "gitea";
    };
  };

  system.stateVersion = "21.11";
}
