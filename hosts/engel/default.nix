{ config, ... }:

{
  networking.hostName = "engel";
  microvm.mem = 1024;
  c3d2 = {
    deployment.server = "server10";
    sendmail = {
      enable = true;
      name = "Engelsystem";
    };
  };

  services = {
    backup = {
      exclude = [
        "/var/lib/engelsystem/storage/cache/"
      ];
      paths = [
        "/var/lib/engelsystem/"
      ];
    };

    engelsystem = {
      enable = true;
      settings = {
        autoarrive = true;
        database = {
          database = "engelsystem";
          host = "localhost";
          username = "engelsystem";
        };
        default_locale = "de_DE";
        email = {
          driver = "sendmail";
          from = {
            address = "noreply@c3d2.de";
            name = "Engelsystem";
          };
          sendmail = "/run/wrappers/bin/sendmail -t";
        };
        enable_dect = true;
        enable_mobile_show = true;
        enable_planned_arrival = false;
        enable_tshirt_size = false;
        footer_items = {
          Contact = "mailto:mail@datenspuren.de";
          FAQ = "https://www.c3d2.de/kontakt.html";
        };
        last_unsubscribe = 24;
        max_freeloadable_shifts = 20;
        night_shifts.enabled = true;
        signup_advance_hours = 0;
        signup_requires_arrival = true;
        timezone = config.time.timeZone;
      };
      createDatabase = true;
      domain = "engel.datenspuren.de";
    };

    mysqlBackup.databases = [ "engelsystem" ];

    nginx = {
      enable = true;
      virtualHosts."${config.services.engelsystem.domain}" = {
        default = true;
        forceSSL = true;
        enableACME = true;
      };
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
  };

  system.stateVersion = "24.05";
}
