{ config, lib, libC, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  c3d2 = {
    audioServer = {
      enable = true;
      ledfx = true;
    };
    baremetal = true;
    interface = "eth0";
    k-ot.enable = true;
    statistics.enable = true;
  };

  boot.loader = {
    efi.canTouchEfiVariables = true;
    systemd-boot.enable = true;
  };

  environment = {
    etc."wireplumber/main.lua.d/50-rename.lua".text = /* lua */ ''
      rule = {
        matches = {
          {
            -- get node name via: pw-cli ls Node
            { "node.name", "equals", "alsa_output.usb-Roland_UA-22-00.analog-stereo" },
          },
        },
        apply_properties = {
          -- TODO: better name?
          ["node.description"] = "Pipebert Audio Streaming",
        },
      }

      table.insert(alsa_monitor.rules,rule)
    '';
    systemPackages = with pkgs; [
      usbutils
    ];
  };

  networking = {
    domain = "hq.c3d2.de";
    firewall.allowedTCPPorts = [ 6600 ];
    firewall.extraInputRules = ''
      ip saddr { ${lib.concatStringsSep ", " libC.subnets.v4} } tcp dport 6600 accept comment "ldaps from internal"
      ip6 saddr { ${lib.concatStringsSep ", " libC.subnets.v6} } tcp dport 6600 accept comment "ldaps from internal"
    '';
    nftables.enable = true;
    hostId = "5c4ed15a";
    hostName = "pipebert";
    interfaces.eth0.useDHCP = true;
    useDHCP = false;
  };

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  services = {
    mopidy = {
      enable = true;
      configuration = /* toml */ ''
        [audio]
        output = pulsesink server=127.0.0.1

        [core]
        restore_state = true

        [file]
        enabled = true
        media_dirs = /home/k-ot/Music

        [http]
        allowed_origins =
          cristianpb.github.io
          mopster.urizen.pl

        [mpd]
        enabled = true
        hostname = ::

        [youtube]
        allow_cache = true
        youtube_dl_package = yt_dlp
      '';
      extensionPackages = with pkgs; [
        mopidy-iris
        mopidy-mpd
        mopidy-youtube
        python3Packages.yt-dlp
      ];
    };

    nginx = {
      enable = true;
      virtualHosts = {
        "drkkr.hq.c3d2.de" = {
          default = true;
          enableACME = true;
          forceSSL = true;
          listen = libC.defaultListen;
          locations."/" = {
            proxyPass = "http://127.0.0.1:${toString config.services.octoprint.port}";
            proxyWebsockets = true;
            extraConfig = ''
              proxy_set_header X-Scheme $scheme;
              proxy_set_header Accept-Encoding identity;
              client_max_body_size 200M;
            '' + libC.hqNetworkOnly;
          };
          # locations."/cam/stream" = {
          #   proxyPass = "http://localhost:3020/?action=stream";
          #   extraConfig = "proxy_pass_request_headers off;";
          # };
          # locations."/cam/capture" = {
          #   proxyPass = "http://localhost:3020/?action=snapshot";
          #   extraConfig = "proxy_pass_request_headers off;";
          # };
        };
        "drucker.hq.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
          locations."/".return = "307 https://drkkr.hq.c3d2.de/";
        };
        "ledfx.hq.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
          locations."/" = {
            proxyPass = "http://127.0.0.1:8888/";
            proxyWebsockets = true;
            extraConfig = libC.hqNetworkOnly;
          };
        };
        "mopidy.hq.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
          locations."/" = {
            proxyPass = "http://127.0.0.1:6680";
            proxyWebsockets = true;
            extraConfig = libC.hqNetworkOnly;
          };
        };
        "pipebert.hq.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
          locations."/" = {
            extraConfig = ''
              default_type text/html;
            '';
            return = ''200 '<!DOCTYPE html>
              <html>
                <body>
                  <ul>
                    <li><a href="https://drkkr.hq.c3d2.de/">Drucker</li></a>
                    <li><a href="https://ledfx.hq.c3d2.de/">LEDfx</li></a>
                    <li><a href="https://mopidy.hq.c3d2.de/iris/">Mopidy</li></a>
                    <li><a href="https://mopidy.hq.c3d2.de/youtube/">Mopidy - Add YouTube link</li></a>
                  </ul>
                </body>
              </html>' '';
          };
        };
      };
    };

    octoprint = {
      enable = true;
      plugins = plugins: with plugins; [ firmwareupdater ];
      # extraConfig.webcam = {
      #   snapshot = "http://localhost:3020?action=snapshot";
      #   stream = "https://drkkr.hq.c3d2.de/cam/stream";
      # };
    };

    udev.extraRules = ''
      SUBSYSTEM=="usb", DRIVER=="usb", MODE="0664", GROUP="users"
      SUBSYSTEM=="usb", DRIVER=="usb", \
        RUN+="/bin/sh -c \"chown -f root:users $sys$devpath/*-port*/disable || true\"" \
        RUN+="/bin/sh -c \"chmod -f 660 $sys$devpath/*-port*/disable || true\""
    '';
  };

  system.stateVersion = "22.11";

  systemd.user.services.pipewire.serviceConfig = {
    ExecStartPost = pkgs.writeShellScript "reload-usb-hubs" /* bash */ ''
      ${lib.getExe pkgs.uhubctl} --force --location 1-1 --action cycle
      ${lib.getExe pkgs.uhubctl} --force --location 2-1 --action cycle
    '';
  };

  users.users = lib.optionalAttrs config.services.octoprint.enable {
    # Allow access to printer serial port and GPIO
    "${config.services.octoprint.user}".extraGroups = [ "dialout" ];
  };
}
