{ lib, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  c3d2 = {
    baremetal = true;
    interface = "enp0s25";
    k-ot.enable = true;
  };

  nix.settings = {
    cores = 4;
    max-jobs = 4;
  };

  boot.loader = {
    efi.canTouchEfiVariables = true;
    systemd-boot.enable = true;
  };

  disko.disks = [ {
    device = "/dev/disk/by-id/ata-SSD0240S00_20201124BC41037";
    name = "glotzbert";
    withCeph = false;
    withLuks = false;
  } ];

  networking = {
    domain = "hq.c3d2.de";
    firewall = {
      allowedTCPPorts = [
        5900 # vnc
      ];
    };
    hostId = "42424242";
    hostName = "glotzbert";
    interfaces.enp0s25.useDHCP = true;
  };

  console.keyMap = "de";

  environment.systemPackages = with pkgs; [
    chromium
    firefox
    mpv
    kodi
    # tracer-game
    bevy_julia
    bevy_mandelbrot
    allcolors
  ];

  systemd.user.services.x11vnc = {
    description = "X11 VNC server";
    wantedBy = [ "graphical-session.target" ];
    partOf = [ "graphical-session.target" ];
    serviceConfig = {
      ExecStart = "${lib.getExe pkgs.x11vnc} -forever -shared -passwd k-ot";
      RestartSec = 5;
      Restart = "always";
    };
  };

  hardware = {
    graphics.extraPackages = with pkgs; [
      intel-media-driver
    ];
    # TODO: migrate to pipewire!
    pulseaudio = {
      enable = true;
      systemWide = true;
      zeroconf = {
        discovery.enable = true;
        publish.enable = true;
      };
      tcp = {
        enable = true;
        anonymousClients.allowAll = true;
      };
      extraConfig = ''
        load-module module-tunnel-sink server=pipebert.hq.c3d2.de
      '';
      extraClientConf = ''
        default-server = pipebert.hq.c3d2.de
      '';
    };
  };

  services = {
    displayManager = {
      autoLogin = {
        enable = true;
        user = "k-ot";
      };
      defaultSession = "gnome-xorg";
    };

    pipewire.enable = lib.mkForce false;

    xserver = {
      enable = true;
      xkb = {
        layout = "de";
        options = "eurosign:e";
      };

      displayManager.lightdm.enable = true;
      desktopManager = {
        gnome.enable = true;
        kodi.enable = true;
      };
    };
  };

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ./secrets.yaml;
  };

  users = {
    groups."k-ot".gid = 1000;
    users."k-ot" = {
      group = "k-ot";
      extraGroups = [ "networkmanager" ];
    };
  };

  system.stateVersion = "22.11";
}
