# Public Access Proxy

We are using Haproxy to do SNI-based forwarding without opening the TLS connection.

## How to add a domain?

To proxy a new domain, it should be added to [`services.proxy.proxyHosts`](./default.nix).

| Option | Usage |
|--------|-------|
|`hostNames`|A list of domain(s) that should be proxied|
|`proxyTo.host`|The IP it should be proxied to|
|`proxyTo.httpPort`|The http port to proxy to, defaults to 80|
|`proxyTo.httpsPort`|The http port to proxy to, defaults to 443|
|`proxyTo.proxyHttpPort`|The http proxy protocol port to proxy to, defaults to 8080|
|`proxyTo.proxyHttpsPort`|The http proxy protocol port to proxy to, defaults to 8443|
|`proxyProtocol`|If the web server we are proxying to, talks proxy protocol. Please make sure, to do IP filtering. See []`services.nginx.commonServerConfig`](../../config/default.nix).|
|`matchArg`|Optional arguments to HAProxy `req.ssl_sni -i`|

## How to forward a port?

This is done in the [network repo](https://gitea.c3d2.de/zentralwerk/network/), to be exact in the [`site.hosts.upstream4.forwardPorts` option](https://gitea.c3d2.de/zentralwerk/network/src/branch/master/config/net/upstream.nix#L32-L217).
