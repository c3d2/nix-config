{ config, lib, libC, pkgs, ... }:

{
  imports = [
    ./proxy.nix
  ];

  boot.kernel.sysctl = {
    # table overflow causing packets from nginx to the service to drop
    # nf_conntrack: nf_conntrack: table full, dropping packet
    "net.netfilter.nf_conntrack_max" = toString (4096*32);
  };

  c3d2 = {
    deployment.server = "server10";
    statistics.enable = true;
  };

  networking = {
    firewall.extraInputRules = let
      # source https://networksdb.io/ip-addresses-of/alibaba-cloud-llc
      ipv4Range = [
        "47.74.0.0/15"
        "47.76.0.0/15"
        "47.78.0.0/15"
        "47.80.0.0/15"
        "47.82.0.0/15"
        "47.84.0.0/15"
        "47.86.0.0/15"
      ];
      allowedTCPPorts = [
        # ssh
        22
        # haproxy
        80 443
        # gemini
        1965
      ];
    in
    # allowing tcp ports must be done manually because otherwise traffic gets accepted before we can drop it.
    ''
      ip saddr { ${lib.concatStringsSep ", " ipv4Range} } drop comment "Alibaba Cloud"
      tcp dport { ${lib.concatStringsSep ", " (map toString allowedTCPPorts)} } accept
    '';
    hostName = "public-access-proxy";
  };

  services = {
    collectd.plugins.exec = ''
      Exec "collectd" "${lib.getExe pkgs.ruby}" "${./haproxy-stats.rb}"
    '';

    # add a socket that is world-accessible for collectd
    haproxy.config = ''
      global
        stats socket /run/haproxy/haproxy-stats.sock mode 666
    '';

    proxy = {
      enable = true;
      proxyHosts = [ {
        hostNames = [ "auth.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.auth.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "jabber.c3d2.de" ];
        matchArg = "-m end";
        proxyTo.host = libC.hostRegistry.jabber.ip4;
        # TODO: enable
        # proxyProtocol = true;
      } {
        hostNames = [ "zw.poelzi.org" ];
        proxyTo.host = "172.20.73.162";
        matchArg = "-m end";
      } {
        hostNames = [ "borken.dvb.solutions" "borken.tlm.solutions" ];
        proxyTo.host = libC.hostRegistry.borken-data-hoarder.ip4;
        matchArg = "-m end";
      } {
        hostNames = [ "staging.dvb.solutions" "staging.tlm.solutions" ];
        proxyTo.host = libC.hostRegistry.staging-data-hoarder.ip4;
        matchArg = "-m end";
      } {
        hostNames = [ "dvb.solutions" "tlm.solutions" ];
        proxyTo.host = "172.20.73.69";
        matchArg = "-m end";
      } {
        hostNames = [ "blogs.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.blogs.ip4;
        # TODO: enable
        # proxyProtocol = true;
      } {
        hostNames = [
          "datenspuren.de" "www.datenspuren.de" "ds.c3d2.de" "datenspuren.c3d2.de" "taler.datenspuren.de"
          "c3d2.de" "www.c3d2.de" "c3dd.de" "www.c3dd.de" "cccdd.de" "www.cccdd.de" "dresden.ccc.de" "www.dresden.ccc.de"
          "autotopia.c3d2.de" "openpgpkey.c3d2.de"
          "chaosmachtschule.de" "www.chaosmachtschule.de"
          "netzbiotop.org" "www.netzbiotop.org"
          "zentralwerk.org" "www.zentralwerk.org"
        ];
        proxyTo.host = libC.site.net.flpk.hosts4.c3d2-web;
        # TODO: enable
        # proxyProtocol = true;
      } {
        hostNames = [
          "codimd.c3d2.de"
          "hackmd.c3d2.de"
          "hedgedoc.c3d2.de"
        ];
        proxyTo.host = libC.hostRegistry.hedgedoc.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "ftp.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.ftp.ip4;
        # TODO: enable
        # proxyProtocol = true;
      } {
        hostNames = [ "gitea.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.gitea.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "grafana.c3d2.de" "grafana.hq.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.grafana.ip4;
        proxyProtocol = true;
      } {
        hostNames = [
          "hydra.hq.c3d2.de"
        ];
        proxyTo.host = libC.hostRegistry.hydra.ip4;
        # TODO: enable in hydra
        # proxyProtocol = true;
      } {
        hostNames = [ "mate.c3d2.de" "matemat.c3d2.de" "matemat.hq.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.matemat.ip4;
        proxyProtocol = true;
      } {
        hostNames = [
          "element.c3d2.de"
          "matrix.c3d2.de"
        ];
        proxyTo.host = libC.hostRegistry.matrix.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "media.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.mediagoblin.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "mobilizon.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.mobilizon.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "nixos.c3d2.de" "code.nixos.c3d2.de" "search.nixos.c3d2.de" "tracker.nixos.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.nixos-misc.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "drkkr.hq.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.pulsebert.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "scrape.hq.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.scrape.ip4;
        proxyProtocol = true;
      } {
        hostNames = [
          "adsb.hq.c3d2.de"
          "sdr.hq.c3d2.de"
        ];
        proxyTo.host = libC.hostRegistry.sdrweb.ip4;
        # TODO: enable
        # proxyProtocol = true;
      } {
        hostNames = [
          "stream.hq.c3d2.de" "torrents.hq.c3d2.de"
        ];
        proxyTo.host = libC.hostRegistry.stream.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "ticker.c3d2.de" "dresden.wtf" "www.dresden.wtf" ];
        proxyTo.host = libC.hostRegistry.ticker.ip4;
        # TODO: enable
        # proxyProtocol = true;
      } {
        hostNames = [ "wiki.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.mediawiki.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "owncast.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.owncast.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "relay.fedi.buzz" ];
        proxyTo.host = libC.site.net.serv.hosts4.buzzrelay;
        # TODO: enable
        # proxyProtocol = true;
      } {
        hostNames = [ "drone.c3d2.de" "drone.hq.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.drone.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "home-assistant.hq.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.home-assistant.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "pretalx.c3d2.de" "talks.datenspuren.de" ];
        proxyTo.host = libC.hostRegistry.pretalx.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "vaultwarden.c3d2.de" ];
        proxyTo.host = libC.hostRegistry.vaultwarden.ip4;
        proxyProtocol = true;
      } {
        hostNames = [ "engel.datenspuren.de" ];
        proxyTo.host = libC.hostRegistry.engel.ip4;
      } {
        hostNames = [ "backend.taler.datenspuren.de" "bank.taler.datenspuren.de" "exchange.taler.datenspuren.de" ];
        proxyTo.host = libC.hostRegistry.taler.ip4;
      } ];
    };
  };

  security.dhparams = {
    enable = true;
    params.haproxy = { };
  };

  # DNS records IN AAAA {www.,}c3d2.de point to this host but
  # gemini:// is served on c3d2-web only
  systemd.services = {
    gemini-forward = {
      wantedBy = [ "multi-user.target" ];
      script = ''
        ${lib.getExe pkgs.socat} tcp6-listen:1965,fork "tcp6:[${libC.site.net.flpk.hosts6.flpk.c3d2-web}]:1965"
      '';
      serviceConfig = {
        ProtectSystem = "strict";
        DynamicUser = true;
      };
    };

    # restart instead of starting & stopping
    haproxy = {
      reloadIfChanged = false;
      restartIfChanged = false;
      stopIfChanged = false;
    };
  };

  system.stateVersion = "18.09";
}
