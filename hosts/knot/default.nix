{ config, dns, lib, libC, pkgs, ... }:

{
  c3d2 = {
    deployment.server = "server10";
    statistics.enable = true;
  };

  # required for CI
  microvm = let
    writableStoreOverlayImage = "/var/tmp/microvm-knot-nix-store-overlay.img";
  in {
    mem = 2 * 1024;
    preStart = ''
      # Discard old writable store overlay
      rm -f "${writableStoreOverlayImage}"
    '';
    volumes = [ {
      image = writableStoreOverlayImage;
      mountPoint = config.microvm.writableStoreOverlay;
      size = 3 * 1024;
    } ];
    writableStoreOverlay = "/nix/.rw-store";
  };

  environment = {
    etc.gitconfig.text = /* gitconfig */ ''
      [url "gitea@gitea.c3d2.de:"]
      insteadOf = https://gitea.c3d2.de/
    '';
    systemPackages = with pkgs; [
      rsync # used in drone CI
    ];
  };

  # changes in knot config cause a rebuild because tools like keymgr are wrapped with the config file *and* contain the man pages
  documentation.man.generateCaches = false;

  networking = {
    hostName = "knot";
    firewall = {
      allowedTCPPorts = [
        # DNS
        53
      ];
      allowedUDPPorts = [
        # DNS
        53
      ];
    };
  };

  nix.enable = true;

  services.knot = {
    enable = true;
    keyFiles = [ config.sops.secrets."knot/keyFile".path ];
    settings = {
      acl = [
        {
          id = "jabber";
          key = "jabber.c3d2.de";
          action = "update";
          update-owner = "name";
          update-owner-match = "sub-or-equal";
          update-owner-name = [ "chat.c3d2.de." "jabber.c3d2.de." ];
        }
        {
          id = "axfr";
          address = [
            # Inbert
            "2001:67c:1400:2240::1/128"
            # dns.serv.zentralwerk.org
            "172.20.73.2/32"
            "2a00:8180:2c00:282:2::2"
          ];
          action = [ "transfer" "notify" ];
        }
        {
          # https://www.knot-dns.cz/docs/3.3/singlehtml/index.html#catalog-zones-configuration-examples
          id = "zone_xfr";
          address  = [
            # ns.spaceboyz.net old
            "95.217.229.209" "2a01:4f9:4b:39ec::4"
            # ns.spaceboyz.net
            "37.27.116.148" "2a01:4f9:3070:2728::4"
            # ns1.supersandro.de
            "188.34.196.104" "2a01:4f8:1c1c:1d38::1"
          ];
          action = "transfer";
        }
        {
          id = "dns-zentralwerk_notify";
          address = [ /*"172.20.73.2"*/ "2a00:8180:2c00:282:2::2" ];
          action = "notify";
        }
      ];

      log = [ {
        any = "notice";
        target = "syslog";
      } ];

      mod-stats = [ {
        id = "default";
        query-type = "on";
      } ];

      remote = let
        via = with libC.site.net.serv; [ hosts4.knot hosts6.up4.knot ];
      in [
        {
          id = "ns-spaceboyz";
          address = [
            "37.27.116.148" "2a01:4f9:3070:2728::4"
          ];
          inherit via;
        } {
          id = "ns1-supersandro";
          # IPv4 doesn't work because of nat
          address = [ /*"188.34.196.104"*/ "2a01:4f8:1c1c:1d38::1" ];
          inherit via;
        } {
          id = "dns-zentralwerk";
          address = [ "172.20.73.2" "2a00:8180:2c00:282:2::2" ];
          inherit via;
        }
      ];

      remotes = [ {
        id = "all";
        remote = [ "ns-spaceboyz" "ns1-supersandro" ];
      } ];

      server = {
        answer-rotation = true;
        automatic-acl = true;
        identity = "ns.c3d2.de";
        listen = with libC.site.net.serv; [
          "127.0.0.1"
          "::1"
          hosts4.knot hosts6.up4.knot hosts6.dn42.knot
          "2a00:8180:2c00:282:2041:cbff:fe0c:8516"
          "fd23:42:c3d2:582:2041:cbff:fe0c:8516"
        ];
        tcp-fastopen = true;
        version = null;
      };

      template = [
        {
          # default is a magic name and is always loaded.
          # Because we want to use catalog-role/catalog-zone settings for all zones *except* the catalog zone itself, we must split the templates
          id = "default";
          global-module = [ "mod-stats" ];
        }
        {
          id = "c3d2";
          catalog-role = "member";
          catalog-zone = "c3d2.";
          dnssec-signing = true;
          file = "%s.zone";
          journal-content = "all"; # required for zonefile-load=difference-no-serial and makes cold starts like zone reloads
          module = "mod-stats/default";
          semantic-checks = true;
          serial-policy = "dateserial";
          storage = "/var/lib/knot/zones";
          zonefile-load = "difference-no-serial";
        }
        {
          id = "zentralwerk_template";
          acl = "dns-zentralwerk_notify";
          master = "dns-zentralwerk";
          storage = "/var/lib/knot/catalog/zentralwerk";
        }
      ];

      zone = [
        {
          acl = "zone_xfr";
          catalog-role = "generate";
          domain = "c3d2.";
          notify = [ "ns1-supersandro" "ns-spaceboyz" ];
          storage = "/var/lib/knot/catalog";
        }
        {
          acl = "dns-zentralwerk_notify";
          catalog-role = "interpret";
          catalog-template = "zentralwerk_template";
          domain = "zentralwerk.";
          master = "dns-zentralwerk";
          storage = "/var/lib/knot/catalog";
        }
      ] ++ map (domain: {
        inherit domain;
        template = "c3d2";
        notify = [ "all" ];
        acl = [ "axfr" "zone_xfr" ]
          ++ lib.optional (domain == "c3d2.de") "jabber";
      }) (lib.attrNames (lib.filterAttrs (n: v: (!v.skip or false)) dns.outputs.packages.${pkgs.stdenv.system}));
    };
  };

  security.sudo.extraRules = [ {
    users = [ "knot" ];
    commands = [ {
      command = "/etc/profiles/per-user/knot/bin/reload-knot";
      options = [ "NOPASSWD" ];
    } ];
  } ];

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
     "knot/keyFile".owner = "knot";
     "ssh-keys/knot/private" = {
        owner = "knot";
        path = "${config.users.users.knot.home}/.ssh/id_ed25519";
      };
      "ssh-keys/knot/public" = {
        owner = "knot";
        path = "${config.users.users.knot.home}/.ssh/id_ed25519.pub";
      };
    };
  };

  system.stateVersion = "23.11";

  users.users.knot = {
    home = "/var/lib/knot/zones/";
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHIkIN1gi5cX2wV2WuNph/QzVK7vvYkvqnR/P69s36mZ drone@c3d2"
    ];
    packages = [
      (pkgs.writeScriptBin "reload-knot" ''
        knotc reload
      '')
    ];
    useDefaultShell = true;
  };
}
