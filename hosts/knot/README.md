# DNS & Domains

## DNS

Currently there are two primary DNS servers, ns.c3d2.de deployed from this directory with its zone config located in the [dns repository](https://gitea.c3d2.de/c3d2/dns) and dns.serv.zentralwerk.org with its mostly auto generated config managed in the [network repository](https://gitea.c3d2.de/zentralwerk/network/src/branch/master/nix/nixos-module/container/dns.nix).
There are two secondary DNS servers: ns1.supersandro.de managed by Sandro and ns.spaceboyz.net managed by Astro.
The managed zones are synced through [catalog zones](https://kb.isc.org/docs/aa-01401), so zone edits, additions or removals can be done without requiring any interaction from the secondary servers and any changes are usually propagated within a few seconds.

### DNSSEC

To get the DNSKEY and DS record ssh into the VM and run the following commands:

```console
$ keymgr c3d2.de dnskey
$ keymgr c3d2.de ds
```

Note for INWX: Their website requires to add an `IN` between the domain name and record type.

## Domains

Please see [this wiki page](https://wiki.c3d2.de/wiki/Domains) for a list of domains and their owner.
