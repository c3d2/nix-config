{ config, libC, pkgs, ... }:

{
  imports = [
    ./owncast-archiver.nix
  ];

  c3d2 = {
    deployment = {
      mounts = [ "archive" ];
      server = "server10";
    };
    statistics.enable = true;
  };

  microvm.mem = 768;

  networking.hostName = "owncast";

  services = {
    backup = {
      enable = true;
      paths = [ "/var/lib/owncast/" ];
    };

    nginx = {
      enable = true;
      additionalModules = [ pkgs.nginxModules.fancyindex ];
      virtualHosts."owncast.c3d2.de" = {
        default = true;
        forceSSL = true;
        enableACME = true;
        listen = libC.defaultListen;
        locations = {
          "/" = {
            proxyPass = "http://127.0.0.1:${toString config.services.owncast.port}";
            proxyWebsockets = true;
          };
          "/archive".return = "307 /archive/";
          "/archive/" = {
            alias = "${config.services.owncast-archiver.targetDir}/";
            extraConfig = ''
              fancyindex on;
              fancyindex_exact_size off;
            '';
          };
        };
      };
    };

    owncast = {
      enable = true;
      openFirewall = true;
      port = 8800;
    };
  };

  sops.defaultSopsFile = ./secrets.yaml;

  systemd.services.nginx.serviceConfig.ReadWritePaths = [
    config.services.owncast-archiver.targetDir
  ];

  services.owncast-archiver.enable = true;

  system.stateVersion = "23.05";
}
