# Mail

## Client configuration

### Server
- Server: `mail.flpk.zentralwerk.org`
- Username: Username (without Domain)
- Password: LDAP Password

### IMAP
- Port: 993
- Security: TLS
- Auth type: Password

### SMTP
- Port: 465
- Security: TLS
- Auth type: Password
