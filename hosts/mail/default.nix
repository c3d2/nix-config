{ config, lib, libC, pkgs, ... }:

let
  generatedAliases = pkgs.writeText "generated-aliases" (lib.concatStringsSep "\n"
    (lib.mapCartesianProduct ({ aliases, domain }: "${aliases}@${domain}  root@c3d2.de") {
      aliases = [
        "abuse"
        "hostmaster"
        "noreply"
        "postmaster"
        "webmaster"
      ];
      domain = config.mailserver.domains;
    }));

  # NOTE: only works with port 25 and STARTTLS
  relayIps = with libC.hostRegistry; [
    "127.0.0.1"
    "::1"
    engel.ip4
    engel.ip6
    gitea.ip4
    gitea.ip6
    mastodon.ip4
    mastodon.ip6
    matemat.ip4
    matemat.ip6
    matrix.ip4
    matrix.ip6
    mediagoblin.ip4
    mediagoblin.ip6
    mediawiki.ip4
    mediawiki.ip6
    pretalx.ip4
    pretalx.ip6
    vaultwarden.ip4
    vaultwarden.ip6
  ];
in
{
  # restic backup got rather big and OOMs with less
  microvm.mem = 6 * 1024;

  networking = {
    firewall = {
      extraInputRules = ''
        ip saddr { ${lib.concatStringsSep ", " libC.subnets.v4} } tcp dport ${toString config.services.prometheus.exporters.postfix.port} accept comment "node_exporter from internal"
        ip6 saddr { ${lib.concatStringsSep ", " libC.subnets.v6} } tcp dport ${toString config.services.prometheus.exporters.postfix.port} accept comment "node_exporter from internal"
      '';
    };
    hostName = "mail";
    nftables.enable = true;
  };

  c3d2.deployment = {
    server = "server10";
    nixStoreBackend = "blk";
  };

  mailserver = let
    inherit (config.security) ldap;
  in {
    enable = true;
    certificateScheme = "acme-nginx";
    dmarcReporting = {
      enable = true;
      domain = "c3d2.de";
      fromName = "C3D2";
      organizationName = "Netzbiotop Dresden e.V.";
    };
    domains = [
      "c3d2.de"
      "c3d2.social"
      "c3dd.de"
      "cccdd.de"
      "chaosmachtschule.de"
      "datenspuren.de"
      "dresden.ccc.de"
      "netzbiotop.org"
      "zentralwerk.org"
    ];
    relayDomains = [
      "lists.c3d2.de"
    ];
    dkimKeyBits = 2048;
    dkimSelector = "default";
    dkimSigning = true;
    enableImap = true;
    enableImapSsl = true;
    enableManageSieve = true;
    enablePop3 = true;
    enablePop3Ssl = true;
    enableSubmission = true;
    enableSubmissionSsl = true;
    extraVirtualAliases = {};
    fqdn = "mail.flpk.zentralwerk.org";
    ldap = {
      enable = true;
      bind = {
        dn = ldap.bindDN;
        passwordFile = config.sops.secrets."dovecot/ldapSearchUserPassword".path;
      };
      domains = lib.filter (domain: domain != "zentralwerk.org") config.mailserver.domains;
      dovecot = let
        ldapFilter = ldap.searchFilterWithGroupFilter "mail-users" "(uid=%n)";
      in {
        passFilter = ldapFilter;
        userAttrs = ""; # ignore posixAccount from ldap
        userFilter = ldapFilter;
      };
      postfix = {
        filter = ldap.searchFilterWithGroupFilter "mail-users" "(uid=%u)";
        mailAttribute = "uid";
        uidAttribute = "uid";
      };
      searchBase = ldap.userBaseDN;
      uris = [ ldap.serverURI ];
    };
    lmtpSaveToDetailMailbox = "no"; # DOS potential
    mailboxes = {
      Drafts = {
        auto = "subscribe";
        specialUse = "Drafts";
      };
      Sent = {
        auto = "subscribe";
        specialUse = "Sent";
      };
      Spam = {
        auto = "subscribe";
        specialUse = "Junk";
      };
      Trash = {
        auto = "subscribe";
        specialUse = "Trash";
      };
    };
    maxConnectionsPerUser = 10;
    messageSizeLimit = 10 * 1000 * 1024; # 10 MiB
    # TODO: upstream relay IPs
    # depends on https://gitlab.com/simple-nixos-mailserver/nixos-mailserver/-/merge_requests/337
    policydSPFExtraConfig = ''
      skip_addresses = ${lib.concatStringsSep "," relayIps}
    '';
    rejectRecipients = [ config.mailserver.dmarcReporting.localpart ];
    rspamdWebUi = {
      enable = true;
      domain = "rspamd.c3d2.de";
    };
    localDnsResolver = false;
    virusScanning = false;
    vmailGroupName = "vmail";
    vmailUserName = "vmail";
  };

  networking = {
    enableIPv6 = false; # mail is stuck in the 80s
    hosts = {
      # https://gitea.c3d2.de/zentralwerk/network/issues/19
      "172.20.73.53" = [ "gitea.c3d2.de" ];
      "172.20.73.72" = [ "auth.c3d2.de" ];
    };
    nameservers = with libC.hostRegistry.dnscache; lib.mkForce [
      ip4
      ip6
    ];
  };

  # TODO: upstream
  security.dhparams = {
    enable = true;
    params.postfix = { };
  };

  services = {
    automx2 = {
      enable = true;
      domain = "c3d2.de";
      settings = {
        provider = "C3D2";
        domains = config.mailserver.domains;
        servers = map (x: {
            name = config.mailserver.fqdn;
            type = "imap";
            user_name = "%EMAILLOCALPART%";
          } // x) [
          { type = "imap"; }
          { type = "pop"; }
          { type = "smtp"; }
        ];
      };
    };

    backup = {
      enable = true;
      paths = [
        "/var/lib/dovecot/"
        "/var/lib/postfix/"
        "/var/dkim/"
        "/var/sieve/"
        "/var/vmail/"
      ];
    };

    dovecot2.mailLocation = lib.mkForce "maildir:/var/vmail/%n";

    fail2ban = {
      enable = true;
      jails = {
        dovecot = {
          enabled = true;
          settings = {
            filter = "dovecot";
            findtime = 43200; # 12h
            maxretry = 3;
          };
        };
        postfix = {
          enabled = true;
          settings = {
            filter = "postfix";
            findtime = 43200; # 12h
            mode = "aggressive";
            maxretry = 3;
          };
        };
      };
    };

    mailman = {
      enable = true;
      enablePostfix = true;
      enablePostgres = true;
      serve.enable = true;
      hyperkitty.enable = true;
      siteOwner = "listmaster@c3d2.de";
      openidConnect.clientSecretFile = config.sops.secrets."mailman/clientSecret".path;
      settings = {
        ARC = {
          enabled = "yes";
          authserv_id = config.services.postfix.hostname;
          privkey = config.sops.secrets."mailman/arcPrivKey".path;
          selector = "mailman";
          domain = "lists.c3d2.de";
        };
      };
      webSettings = {
        DEFAULT_FROM_EMAIL = "noreply@lists.c3d2.de";
        SERVER_EMAIL = "noreply@lists.c3d2.de";
      };
    };

    nginx = {
      enable = true;
      commonHttpConfig = /* nginx */ ''
        proxy_headers_hash_bucket_size 96;
      '';
      virtualHosts = {
        "autoconfig.${config.services.automx2.domain}" = {
          serverAliases = lib.mkForce (lib.concatMap (domain:
          # don't duplicate server name
          lib.optional (domain != "c3d2.de") ("autoconfig." + domain)
          ++ [ ("autodiscover." + domain) ])
          (lib.filter (domain: domain != "zentralwerk.org") config.mailserver.domains));
        };

        # used for roundcube
        "mail.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
        };

        "mail.flpk.zentralwerk.org" = {
          enableACME = true;
          forceSSL = true;
          locations."/".return = "308 https://mail.c3d2.de/";
        };

        "lists.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
        };
      };
    };

    oauth2-proxy = {
      cookie.domain = config.mailserver.rspamdWebUi.domain;
      email.domains = [ "*" ];
      keyFile = config.sops.secrets."oauth2-proxy/environment".path;
      nginx = {
        domain = config.mailserver.rspamdWebUi.domain; # This only works as long as we only have one protected virtualHost
        virtualHosts."${config.mailserver.rspamdWebUi.domain}".allowed_groups = [ "mail-admins" ];
      };
    };

    opendkim.settings = let
      trustedHosts = "refile:${pkgs.writeText "TrustedHosts" (lib.concatStringsSep "\n" relayIps)}";
    in {
      # TODO upstream
      # depends on https://gitlab.com/SuperSandro2000/nixos-mailserver/-/commit/5d99cbcc59d1b1b680b8d2081aaab78e18eddbfc
      # which depends on https://github.com/NixOS/nixpkgs/pull/333758
      ExternalIgnoreList = trustedHosts;
      InternalHosts = trustedHosts;
    };

    portunus.oauth2-proxy = {
      configure = true;
      clientID = "oauth2_proxy_mail";
    };

    postgresql.package = pkgs.postgresql_17;

    postfix = let
      # NOTE: regex/mailserver.loginAccounts.<name>.aliasesRegexp are also overwritten here!
      submissionOptions.smtpd_sender_login_maps = lib.mkForce "hash:/etc/postfix/virtual,ldap:/run/postfix/ldap-sender-login-map.cf";
    in {
      config = {
        smtpd_recipient_restrictions = [
          "check_recipient_access regexp:${pkgs.writeText "discard_recipients" ''
            /^dmarc-noreply@c3d2\.de$/  DISCARD
          ''}"
        ];
        # TODO: upstream
        smtpd_tls_dh1024_param_file = config.security.dhparams.params.postfix.path;
        virtual_alias_maps = [
          "hash:/etc/postfix/valias"
          "texthash:${generatedAliases}"
        ];
        # mailman doc and nixos module assertions
        unknown_local_recipient_reject_code = 550;
        local_recipient_maps = [ "hash:/var/lib/mailman/data/postfix_lmtp" ];
        relay_domains = [
          "hash:/var/lib/mailman/data/postfix_domains"
        ];
        transport_maps = [ "hash:/var/lib/mailman/data/postfix_lmtp" ];
      };
      mapFiles = {
        "valias" = lib.mkForce "/var/lib/postfix/valias";
        "virtual" = lib.mkForce "/var/lib/postfix/virtual";
      };
      # TODO: upstream
      # depends on https://gitlab.com/simple-nixos-mailserver/nixos-mailserver/-/merge_requests/337
      networks = [
        "127.0.0.1/32"
        "[::1]/128"
      ] ++ map (ip: if lib.hasInfix ":" ip then "[${ip}]/128" else "${ip}/32") relayIps;
      submissionOptions = submissionOptions;
      submissionsOptions = submissionOptions;
    };

    prometheus.exporters.postfix.enable = true;

    # breaks rspamd as it returns NOERROR instead of NXDOMAIN and kresd is enabled
    resolved.enable = false;

    roundcube = {
      enable = true;
      hostName = "mail.c3d2.de";
      plugins = [ "archive" "managesieve" "markasjunk" ];
      extraConfig = /* php */ ''
        # starttls needed for authentication, so the fqdn required to match the certificate
        $config['smtp_server'] = "tls://${config.mailserver.fqdn}";
        $config['smtp_user'] = "%u";
        $config['smtp_pass'] = "%p";

        $config['support_url'] = 'https://c3d2.de/impressum.html';

        # managesieve plugin
        $config['managesieve_host'] = '${config.mailserver.fqdn}';
      '';
    };

    rspamd.locals = {
      # https://github.com/rspamd/rspamd/blob/master/conf/statistic.conf#L47-L53
      "classifier-bayes.conf".text = ''
        autolearn {
          spam_threshold = 6.0;
          junk_threshold = 4.0;
          ham_threshold = -0.5;
          check_balance = true;
          min_balance = 0.9;
        }
      '';
      "groups.conf".text = ''
        symbols {
          # default 5.1
          # https://github.com/rspamd/rspamd/blob/master/conf/scores.d/statistics_group.conf#L22
          "BAYES_SPAM" { weight = 7.7; };
          # default 1.0
          # https://github.com/rspamd/rspamd/blob/master/conf/scores.d/rbl_group.conf#L342
          "RBL_MAILSPIKE_BAD" { weight = 2.0; };
          # https://github.com/rspamd/rspamd/blob/master/conf/scores.d/rbl_group.conf#L337
          "RBL_MAILSPIKE_VERYBAD" { weight = 3.0; };
          # https://github.com/rspamd/rspamd/blob/master/conf/scores.d/rbl_group.conf#L332
          "RBL_MAILSPIKE_WORST" { weight = 4.0; };
          # default 2.5
          # https://github.com/rspamd/rspamd/blob/master/conf/scores.d/hfilter_group.conf#L118
          "HFILTER_HOSTNAME_UNKNOWN" { weight = 3.5; };
          # default 1.0
          # https://github.com/rspamd/rspamd/blob/master/conf/scores.d/hfilter_group.conf#L78
          "HFILTER_HELO_IP_A" { weight = 1.5; };
          # default 0.3
          # https://github.com/rspamd/rspamd/blob/master/conf/scores.d/hfilter_group.conf#L74
          "HFILTER_HELO_NORES_A_OR_MX" = { weight = 0.6; };
        }
      '';
      "history_redis.conf".text = ''
        nrows = 2000;
      '';
      "spf.conf".text = ''
        whitelist = ${pkgs.writeText "rspamd-spf-whitelist" (lib.concatStringsSep "\n" relayIps)}
      '';
    };
  };

  security.sudo.extraRules = [ {
    users = [ "drone" ];
    commands = [ {
      # must match command in .drone.yaml
      command = "/run/current-system/sw/bin/systemctl restart postfix-setup.service";
      options = [ "NOPASSWD" ];
    } ];
  } ];

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "dovecot/ldapSearchUserPassword".owner = config.users.users.dovecot2.name;
      "mailman/clientSecret" = {
        owner = config.systemd.services.mailman.serviceConfig.User;
        mode = "0440";
      };
      "mailman/arcPrivKey" = {
        owner = config.systemd.services.mailman.serviceConfig.User;
        mode = "0440";
      };
      "oauth2-proxy/environment".owner = "oauth2-proxy";
    };
  };

  system.stateVersion = "23.11";

  systemd = {
    services = {
      # saves a whopping 60MB RAM
      mailman.serviceConfig.Environment = "PYTHONOPTIMIZE=2";

      mailman-settings.script = lib.mkAfter /* bash */ ''
        echo "[antispam]
        header_checks=
          X-Spam: (yes|maybe)
          Authentication-Results: mail.flpk.zentralwerk.org; dmarc=(fail|quarantine)
        jump_chain=discard
        " >> /etc/mailman.cfg
      '';
    };
    tmpfiles.settings.postfix."/var/lib/postfix".d = {
      group = "postfix";
      mode = "770";
      user = "root";
    };
  };

  users = {
    users.drone = {
      group = "postfix";
      isSystemUser = true;
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHIkIN1gi5cX2wV2WuNph/QzVK7vvYkvqnR/P69s36mZ drone@c3d2"
      ];
      # otherwise the the drone ssh runner cannot log in
      useDefaultShell = true;
    };
  };
}
