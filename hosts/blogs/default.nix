{ config, pkgs, ... }:

{
  c3d2.deployment = {
    server = "server10";
    nixStoreBackend = "blk";
  };

  microvm = {
    mem = 2 * 1024;
    balloonMem = 1 * 1024;
  };

  networking.hostName = "blogs";

  services = {
    backup = {
      enable = true;
      paths = [ "/var/lib/plume/" ];
    };

    nginx = {
      enable = true;
      virtualHosts."blogs.c3d2.de" = {
        forceSSL = true;
        enableACME = true;
        locations."/".proxyPass = "http://[::1]:7878";
      };
    };

    postgresql = {
      ensureDatabases = [ "plume" ];
      package = pkgs.postgresql_17;
      upgrade.stopServices = [ "plume" ];
    };

    plume = {
      enable = true;
      # See secrets/hosts/blogs for the .env file with all settings
      envFile = config.sops.secrets."plume/env".path;
    };
  };

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "plume/env".owner = config.systemd.services.plume.serviceConfig.User;
    };
  };

  system.stateVersion = "22.05";
}
