{ config, lib, libC, nuschtos-search, pkgs, self, ... }:

{
  imports = [
    # check changes with https://git.qyliss.net/nixlib/log/modules/server/git/nixpkgs/default.nix !!!
    # (builtins.fetchurl {
    #   url = "https://git.qyliss.net/nixlib/plain/modules/server/git/nixpkgs/default.nix";
    #   sha256 = "sha256:1apyxpahd1220bfmwv6al4ickbg2x4bk7wy6d4njjrzjy9vvs8b3";
    # })
    ./qyliss-nixpkgs.nix
  ];

  c3d2 = {
    deployment = {
      server = "server10";
      nixStoreBackend = "blk";
    };
    statistics.enable = true;
  };

  microvm = {
    cpu = 2;
    mem = 2 * 1024;
  };

  networking.hostName = "nixos-misc";

  services = {
    nginx = {
      enable = true;
      virtualHosts = {
        "nixos.c3d2.de" = {
          default = true;
          forceSSL = true;
          enableACME = true;
          listen = libC.defaultListen;
          locations."/".root = pkgs.runCommand "nixos-web" {
            nativeBuildInputs = with pkgs; [ mdbook moreutils xorg.lndir ];
          } ''
            mkdir docs
            lndir ${self} docs
            cd docs
            find -iname '*.md' -exec bash -c 'echo "- [$(grep '"'"'# '"'"' {} | head -n 1 | awk '"'"'{ print $2 }'"'"')]({})"' \; | sort | sponge SUMMARY.md
            mdbook build -d $out .
          '';
        };
        "code.nixos.c3d2.de" = {
          forceSSL = true;
          enableACME = true;
          listen = libC.defaultListen;
          locations."/".proxyPass = "http://${config.services.hound.listen}";
        };
        "search.nixos.c3d2.de" = {
          forceSSL = true;
          enableACME = true;
          listen = libC.defaultListen;
          locations."/".root = nuschtos-search.packages.${pkgs.stdenv.system}.mkMultiSearch {
            scopes = [
              { modules = [ self.inputs.alert2muc.nixosModules.default ]; name = "alert2muc"; urlPrefix = "https://gitea.c3d2.de/c3d2/alert2muc/src/branch/main/"; }
              { modules = [
                  { _module.args = { inherit pkgs; }; }
                  self.inputs.buzzrelay.nixosModules.default
                ];
                name = "buzzrelay";
                urlPrefix = "https://github.com/astro/buzzrelay/blob/main/";
              }
              { modules = [
                { _module.args = { inherit pkgs; }; }
                self.inputs.caveman.nixosModule
                ];
                name = "caveman";
                urlPrefix = "https://gitea.c3d2.de/astro/caveman/src/branch/main/";
              }
              { modules = [ self.inputs.c3d2-user-module.nixosModule ]; name = "c3d2-user-module"; urlPrefix = "https://gitea.c3d2.de/c3d2/nix-user-module/src/branch/master/"; }
              { modules = [ self.inputs.deployment.nixosModules.deployment-options ]; name = "c3d2-deployment"; urlPrefix = "https://gitea.c3d2.de/c3d2/deployment/src/branch/main/"; }
              {
                modules = [ self.inputs.disko.nixosModules.default ];
                name = "disko";
                specialArgs.modulesPath = pkgs.path + "/nixos/modules";
                urlPrefix = "https://github.com/nix-community/disko/blob/master/";
              }
              { modules = [ self.inputs.heliwatch.nixosModules.heliwatch ]; name = "heliwatch"; urlPrefix = "https://gitea.c3d2.de/astro/heliwatch/src/branch/master/"; }
              {
                modules = with self.inputs.microvm.nixosModules; [
                  { _module.args = { inherit pkgs; }; }
                  microvm
                  host
                ];
                name = "microvm.nix";
                urlPrefix = "https://github.com/astro/microvm.nix/blob/main/";
              }
              {
                optionsJSON = (import "${self.inputs.nixpkgs}/nixos/release.nix" { }).options + /share/doc/nixos/options.json;
                name = "NixOS";
                urlPrefix = "https://github.com/NixOS/nixpkgs/tree/master/";
              }
              {
                modules = lib.filter (x: (builtins.tryEval x).success) (lib.attrValues self.inputs.nixos-hardware.nixosModules)
                ++ [ {
                  hardware.rockchip.platformFirmware = pkgs.hello; # fake that the package is missing on stable
                } ];
                name = "nixos-hardware";
                specialArgs = {
                  modulesPath = pkgs.path + "/nixos/modules";
                  inherit pkgs;
                };
                urlPrefix = "https://github.com/NixOS/nixos-hardware/blob/master/";
              }
              {
                modules = [
                  self.inputs.nixos-modules.nixosModule
                  {
                    _module.args = { inherit pkgs; };
                    imports = [ (pkgs.path + "/nixos/modules/misc/extra-arguments.nix") ];
                  }
                ];
                name = "nixos-modules";
                urlPrefix = "https://github.com/NuschtOS/nixos-modules/blob/main/";
              }
              { modules = [ self.inputs.skyflake.nixosModules.default ]; name = "skyflake"; urlPrefix = "https://github.com/astro/skyflake/blob/main/"; }
              {
                modules = [
                  self.inputs.simple-nixos-mailserver.nixosModules.default
                  # based on https://gitlab.com/simple-nixos-mailserver/nixos-mailserver/-/blob/290a995de5c3d3f08468fa548f0d55ab2efc7b6b/flake.nix#L61-73
                  {
                    mailserver = {
                      fqdn = "mx.example.com";
                      domains = [ "example.com" ];
                      dmarcReporting = {
                        organizationName = "Example Corp";
                        domain = "example.com";
                      };
                    };
                  }
                ];
                name = "simple-nixos-mailserver";
                urlPrefix = "https://gitlab.com/simple-nixos-mailserver/nixos-mailserver/-/blob/master/";
              }
              { modules = [ self.inputs.sops-nix.nixosModules.default ]; name = "sops-nix"; urlPrefix = "https://github.com/Mic92/sops-nix/blob/master/"; }
              { modules = [ self.inputs.ticker.nixosModules.default ]; name = "ticker"; urlPrefix = "https://gitea.c3d2.de/astro/ticker/src/branch/master/"; }
              # heavily depends on loaded hostname
              # {
              #   modules = [ self.inputs.zentralwerk.nixosModule ];
              #   name = "zentralwerk-network";
              #   # https://gitea.c3d2.de/zentralwerk/network/src/branch/master/flake.nix#L33-L42
              #   specialArgs = {
              #     inherit (self.inputs.zentralwerk) lib;
              #     hostName = "<host>";
              #   };
              #   urlPrefix = "https://gitea.c3d2.de/zentralwerk/network/src/branch/master/";
              # }
            ];
          };
        };
        # source https://git.qyliss.net/nixlib/tree/modules/server/nixpk.gs/pr-tracker/default.nix
        "tracker.nixos.c3d2.de" = {
          forceSSL = true;
          enableACME = true;
          listen = libC.defaultListen;
          locations."/" = {
            proxyPass = "http://unix:/run/pr-tracker/pr-tracker.sock:/pr-tracker.html";
            extraConfig = ''
              proxy_http_version 1.1;
            '';
          };
        };
      };
    };

    hound = {
      enable = true;
      package = pkgs.hound.override { mercurial = null; };
      repos = [
        "https://gitea.c3d2.de/c3d2/deployment.git"
        "https://gitea.c3d2.de/c3d2/nix-config.git"
        "https://gitea.c3d2.de/c3d2/nix-user-module.git"
        "https://gitea.c3d2.de/zentralwerk/network.git"
        "https://github.com/Gabriella439/nix-diff.git"
        "https://github.com/Mic92/envfs.git"
        "https://github.com/Mic92/nix-ld.git"
        "https://github.com/Mic92/nix-update.git"
        "https://github.com/Mic92/nixos-shell.git"
        "https://github.com/Mic92/nixpkgs-review.git"
        "https://github.com/Mic92/sops-nix.git"
        "https://github.com/NixOS/flake-registry.git"
        "https://github.com/NixOS/hydra.git"
        "https://github.com/NixOS/nix-pills.git"
        "https://github.com/NixOS/nix.git"
        "https://github.com/NixOS/nixos-hardware.git"
        "https://github.com/NixOS/nixpkgs.git"
        "https://github.com/NixOS/rfcs.git"
        "https://github.com/NuschtOS/nixos-modules.git"
        "https://github.com/astro/microvm.nix.git"
        "https://github.com/astro/nix-openwrt-imagebuilder.git"
        "https://github.com/astro/skyflake.git"
        "https://github.com/cachix/pre-commit-hooks.nix.git"
        "https://github.com/edolstra/flake-compat.git"
        "https://github.com/guibou/nixGL.git"
        "https://github.com/hercules-ci/gitignore.nix.git"
        "https://github.com/ipetkov/crane.git"
        "https://github.com/jtojnar/nixpkgs-hammering.git"
        "https://github.com/maralorn/nix-output-monitor.git"
        "https://github.com/msteen/nix-prefetch.git"
        "https://github.com/nix-community/NixOS-WSL.git"
        "https://github.com/nix-community/comma.git"
        "https://github.com/nix-community/disko.git"
        "https://github.com/nix-community/fenix.git"
        "https://github.com/nix-community/harmonia.git"
        "https://github.com/nix-community/home-manager.git"
        "https://github.com/nix-community/lanzaboote.git"
        "https://github.com/nix-community/naersk.git"
        "https://github.com/nix-community/nix-ld-rs.git"
        "https://github.com/nix-community/nixvim.git"
        "https://github.com/numtide/flake-utils.git"
        "https://github.com/oxalica/rust-overlay.git"
        "https://gitlab.com/simple-nixos-mailserver/nixos-mailserver.git"
      ];
      settings = {
        vcs-config.git.ms-between-poll = "${toString (1000 * 3600 * 8)}";
      };
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets."pr-tracker/github-token" = { };
  };

  system.stateVersion = "24.05";

  # source https://git.qyliss.net/nixlib/tree/modules/server/nixpk.gs/pr-tracker/default.nix
  systemd.services.pr-tracker = {
    path = with pkgs; [ gitMinimal ];
    requires = [ "pr-tracker.socket" ];
    serviceConfig = {
      ExecStart = "${pkgs.pr-tracker.overrideAttrs ({ patches ? [ ], ... }: {
        patches = patches ++ [
          # TODO: rebase when git.qyliss.net is back up
          # ../../overlays/0001-Allow-requesting-in-json-format-remove-leftover-dbg.patch
        ];
      })}/bin/pr-tracker --path /var/lib/git/nixpkgs.git --remote origin --user-agent 'pr-tracker run by c3d2' --source-url https://git.qyliss.net/pr-tracker --mount pr-tracker.html";
      StandardInput = "file:${config.sops.secrets."pr-tracker/github-token".path}";
      DynamicUser = true;
      SupplementaryGroups = "nixpkgs";
      UMask = "0002";
      ReadWritePaths = "/var/lib/git/nixpkgs.git";
    };
  };

  systemd.sockets.pr-tracker = {
    wantedBy = [ "sockets.target" ];
    before = [ "nginx.service" ];
    socketConfig.ListenStream = "/run/pr-tracker/pr-tracker.sock";
  };
}
