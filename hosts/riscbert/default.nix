{ config, lib, libC, pkgs, ... }:

{
  networking.hostName = "riscbert";

  boot = {
    kernelParams = [
      "verbose"
      # "console=ttyS0,115200n8"
      "elevator=deadline"
      "boot.shell_on_fail"
    ];
    initrd = {
      network = {
        enable = true;
        flushBeforeStage2 = false;
      };
      supportedFilesystems = lib.mkForce [
        "nfs"
      ];
      # includeDefaultModules = false;
      availableKernelModules = [
        "dwmac-generic"
        "nfs" "nfsv4"
        "usbhid"
      ];
    };

    tmp.useTmpfs = true;
  };

  fileSystems."/mnt/sd" = {
    device = "/dev/disk/by-label/NIXOS_SD";
    fsType = "ext4";
  };
  # install kernel+initrd onto sd card
  fileSystems."/boot" = {
    device = "/mnt/sd/boot";
    fsType = "none";
    options = [ "bind" ];
  };
  fileSystems."/" = {
    device = "${libC.hostRegistry.nfsroot.ip4}:/var/lib/nfsroot/riscbert";
    fsType = "nfs";
    options = [ "nfsvers=4" "proto=tcp" "nolock" "hard" "async" "rw" ];
  };

  system.build.bootFiles = pkgs.runCommand "boot-files" {} ''
    mkdir -p $out/boot
    ${config.boot.loader.generic-extlinux-compatible.populateCmd} -c ${config.system.build.toplevel} -d $out/boot
  '';

  services.journald.extraConfig = ''
    Storage=volatile
  '';

  nix = {
    daemonCPUSchedPolicy = "idle";
    daemonIOSchedClass = "idle";
    settings = {
      builders-use-substitutes = true;
      cores = 2;
      max-jobs = 1;
      # trusted-users = [ "client" ];
    };
  };

  system.stateVersion = "22.05";
}
