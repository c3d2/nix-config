{ config, lib, libS, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./network.nix
    ./updater.nix
  ];

  c3d2 = {
    baremetal = true;
    statistics.enable = true;
  };

  boot = {
    tmp = {
      useTmpfs = true;
      tmpfsSize = "80%";
    };
    kernelModules = [ "kvm-intel" ];
    loader = {
      efi.canTouchEfiVariables = true;
      systemd-boot.enable = true;
    };
    # For cross-building
    binfmt.emulatedSystems = [ "armv6l-linux" "armv7l-linux" "aarch64-linux" "riscv32-linux" "riscv64-linux" ];
  };

  nix = {
    buildMachines = [
      {
        hostName = "localhost";
        maxJobs = config.nix.settings.max-jobs;
        protocol = null;
        speedFactor = 10;
        supportedFeatures = config.nix.settings.system-features;
        systems = [ "x86_64-linux" "i686-linux" "aarch64-linux" ];
      }
      {
        hostName = "client@dacbert.hq.c3d2.de";
        system = lib.concatStringsSep "," [
          "aarch64-linux" # very slow compared to gallium
          "armv6l-linux" "armv7l-linux"
        ];
        speedFactor = 1;
        supportedFeatures = [ "kvm" "nixos-test" ];
        maxJobs = 1;
      }
    ];
    daemonCPUSchedPolicy = "idle";
    daemonIOSchedClass = "idle";
    daemonIOSchedPriority = 7;
    optimise = {
      automatic = true;
      dates = [ "05:30" ];
    };
    # we require https://github.com/NixOS/nix/pull/9547 for hydra
    package = pkgs.nixVersions.nix_2_20;
    remoteBuilder = {
      enable = true;
      sshPublicKeys = config.users.users.root.openssh.authorizedKeys.keys;
    };
    settings = {
      allowed-uris = [
        "git+https://git.joinplu.me/"
        "git+https://gitea.c3d2.de/"
        "git+https://gitea.nek0.eu/nek0/"
        "git+https://github.com/"
        "git+https://gitlab.com/SuperSandro2000/"
        "git+https://gitlab.com/simple-nixos-mailserver/"
        "github:"
        "gitlab:SuperSandro2000/"
        "gitlab:simple-nixos-mailserver/"
      ];
      builders-use-substitutes = true;
      cores = 20;
      keep-outputs = true;
      max-jobs = 8;
      trusted-users = [ "hydra" "root" "@wheel" ];
      system-features = [ "benchmark" "big-parallel" "ca-derivations" "kvm" "nixos-test" ];
    };
    extraOptions = ''
      !include ${config.sops.secrets."nix/access-tokens".path}
    '';
  };

  networking = {
    hostId = "3f0c4ec4";
    hostName = "hydra";
  };

  services = {
    fail2ban = {
      enable = true;
      ignoreIP = [
        "2a00:8180:2c00:200::/56"
        "2a0f:5382:acab:1400::/56"
        "fd23:42:c3d2:500::/56"
        "::1/128"
        "172.22.99.0/24"
        "172.20.72.0/21"
        "127.0.0.0/8"
      ];
    };

    gitea-actions = {
      enableRunner = false; # currently unused
      kvm = true;
      zfsDataset = "hydra/data/podman";
      giteaUrl = "https://gitea.c3d2.de";
    };

    hydra = {
      enable = true;
      buildMachinesFiles = [
        "/etc/nix/machines"
        "/var/lib/hydra/machines"
      ];
      hydraURL = "https://hydra.hq.c3d2.de";
      ldap.enable = true;
      logo = ./c3d2.svg;
      minimumDiskFree = 50;
      minimumDiskFreeEvaluator = 50;
      notificationSender = "noreply@c3d2.de";
      useSubstitutes = true;
      extraConfig =
        let
          key = config.sops.secrets."nix/signing-key/secretKey".path;
        in
        ''
          binary_cache_secret_key_file = ${key}
          compress_num_threads = 4
          evaluator_workers = 4
          evaluator_max_memory_size = 2048
          max_output_size = ${toString (5*1024*1024*1024)} # sd card and raw images
          store_uri = auto?secret-key=${key}&write-nar-listing=1&ls-compression=zstd&log-compression=zstd
          upload_logs_to_binary_cache = true
        '';
    };

    harmonia = {
      enable = true;
      port = 5000;
      settings.workers = 20;
      signKeyPath = config.sops.secrets."nix/signing-key/secretKey".path;
    };

    nginx = {
      enable = true;
      virtualHosts."hydra.hq.c3d2.de" = {
        default = true;
        enableACME = true;
        forceSSL = true;
        locations = let
          harmonia = {
            proxyPass = "http://127.0.0.1:${toString config.services.harmonia.port}";
            # harmonia serves already compressed content and we want to preserve Content-Length
            extraConfig = /* nginx */ ''
              proxy_buffering off;
              brotli off;
              gzip off;
              zstd off;
            '';
          };
        in {
          "/".proxyPass = "http://127.0.0.1:${toString config.services.hydra.port}";
          "/static/".alias = "${config.services.hydra.package}/libexec/hydra/root/static/";
          "~ /.*\\.ls$" = harmonia;
          "~ /.*\\.narinfo$" = harmonia;
          "~ /nar/.*\\.nar$" = harmonia;
          "= /version" = harmonia;
          "= /nix-cache-info" = harmonia;
        };
      };
    };

    postgresql = {
      ensureDatabases = [ "hydra" ];
      package = pkgs.postgresql_17;
    };

    renovate = {
      enable = true;
      runtimePackages = with pkgs; [
        cargo
        config.nix.package
        ruby # scrapers
      ];
      schedule = "*:20,50";
      settings = {
        allowedPostUpgradeCommands = [ "bundix" ];
        autodiscover = true;
        endpoint = "https://gitea.c3d2.de";
        # autodiscoverFilter = [
        #   "!/NuschtOS/nuschtpkgs/"
        # ];
        forkCreation = false;
        gitAuthor = "Astro Bot <astro@spaceboyz.net>";
        # onboarding = false; # TODO: uncomment
        platform = "gitea";
      };
    };

    resolved.enable = false;

    zfs.trim.enable = true;
  };

  simd.arch = "ivybridge";

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "ldap/search-user-pw" = {
        mode = "440";
        owner = config.users.users.hydra-queue-runner.name;
        path = "/var/lib/hydra/ldap-password.conf";
      };
      "machine-id" = {
        mode = "444";
        path = "/etc/machine-id";
      };
      "nix/access-tokens" = {
        mode = "444";
      };
      "nix/signing-key/secretKey" = {
        mode = "440";
        owner = config.users.users.hydra-queue-runner.name;
      };
      "renovate/environment" = { };
      "ssh-keys/hydra/private" = {
        owner = "hydra";
        # used for cloning flake inputs
        path = "/var/lib/hydra/.ssh/id_ed25519";
      };
      "ssh-keys/hydra/public" = {
        owner = "hydra";
        mode = "440";
        path = "/var/lib/hydra/.ssh/id_ed25519.pub";
      };
      "ssh-keys/root/private" = {
        owner = "hydra-queue-runner";
        # used to build the actual derivations
        path = "/var/lib/hydra/queue-runner/.ssh/id_ed25519";
      };
      "ssh-keys/root/public" = {
        owner = "hydra-queue-runner";
        mode = "440";
        path = "/var/lib/hydra/queue-runner/.ssh/id_ed25519.pub";
      };
      "ssh-keys/updater/private" = {
        owner = "updater";
        path = "/var/lib/updater/.ssh/id_ed25519";
      };
      "ssh-keys/updater/public" = {
        owner = "updater";
        mode = "440";
        path = "/var/lib/updater/.ssh/id_ed25519.pub";
      };
    };
  };

  system.stateVersion = "20.09";

  systemd.services = {
    hydra-evaluator.serviceConfig = {
      CPUWeight = 2;
      MemoryHigh = "64G";
      MemoryMax = "64G";
      MemorySwapMax = "64G";
    };

    # uses memory for no function
    hydra-notify.enable = false;

    nix-daemon.serviceConfig = {
      CPUWeight = 5;
      MemoryHigh = "64G";
      MemoryMax = "64G";
      MemorySwapMax = "64G";
    };

    renovate = lib.mkIf config.services.renovate.enable {
      serviceConfig.EnvironmentFile = config.sops.secrets."renovate/environment".path;
    };
  };
}
