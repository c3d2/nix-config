{ config, lib, libC, ... }:
{
  networking = {
    useDHCP = false;
    useNetworkd = true;
  };

  boot.kernelParams = [
    # Prevents automatic creation of interface bond0 by the kernel
    "bonding.max_bonds=0"
  ];

  systemd.network = {
    enable = true;

    netdevs = {
      serv.netdevConfig = {
        Kind = "bond";
        Name = "serv";
      };
      # LACP
      serv.bondConfig.Mode = "802.3ad";
    };

    networks = {
      en = {
        # physical ethernet ports
        matchConfig.Name = "en*";
        networkConfig = {
          Bond = "serv";
          LLDP = true;
          EmitLLDP = true;
        };
      };
      "serv" =
        let
          inherit (config.networking) hostName;
          netConfig = libC.site.net.serv;
          address = netConfig.hosts4.${hostName};
          prefixLen = netConfig.subnet4Len;
        in
          {
            matchConfig.Name = "serv";
            networkConfig = {
              LLDP = true;
              EmitLLDP = true;
              DHCP = "no";
              IPv6AcceptRA = "no";
            };
            addresses =
              lib.optional (netConfig.hosts4 ? ${hostName}) {
                Address = "${address}/${toString prefixLen}";
              } ++
              builtins.concatMap (hosts6:
                lib.optional (hosts6 ? ${hostName}) {
                  Address = "${hosts6.${hostName}}/64";
                }
              ) (builtins.attrValues netConfig.hosts6);

            gateway = [
              netConfig.hosts4.serv-gw
              netConfig.hosts6.dn42.serv-gw
            ];
          };
    };
  };
}
