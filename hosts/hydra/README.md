# Hydra

We use [Hydra](https://github.com/nixos/hydra) to build and cache all our NixOS configurations.
This is necessary in addition to the official [cache](https://cache.nixos.org), as we have some custom projects which are not in [nixpkgs](https://github.con/NixOS/nixpkgs/) and some packages use custom patches/overrides.

The jobset for this repo can be found at <https://hydra.hq.c3d2.de/jobset/c3d2/nix-config#tabs-jobs>.
