{ config, libC, pkgs, ... }:

let
  c3d2MacAddress = "00:0b:ad:00:1d:ea";
in
{
  c3d2.deployment.server = "server10";
  microvm = {
    mem = 1 * 1024;
    vcpu = 2;
    # add a network interface in c3d2 for mDNS
    interfaces = [{
      type = "tap";
      id = builtins.substring 0 15 "c3d2-${config.networking.hostName}";
      mac = c3d2MacAddress;
    }];
  };

  networking.hostName = "home-assistant";

  systemd.network = {
    links."40-c3d2" = {
      matchConfig.MACAddress = c3d2MacAddress;
      # rename interface to net name
      linkConfig.Name = "c3d2";
    };
    networks."40-c3d2" = {
      dhcpV4Config.UseRoutes = "no";
      matchConfig.MACAddress = c3d2MacAddress;
      networkConfig = {
        DHCP = "no";
        IPv6AcceptRA = "no";
        LinkLocalAddressing = "yes";
      };
    };
  };

  services = {
    avahi.enable = true;

    backup = {
      enable = true;
      paths = [ "/var/lib/hass/" ];
    };

    home-assistant = {
      enable = true;
      config = {
        binary_sensor = [
          {
            platform = "rest";
            name = "Turmlabor";
            unique_id = "status_turmlabor_dresden";
            resource = "https://turmlabor.de/spaces.api";
            method = "GET";
            scan_interval = 60;
            verify_ssl = true;
            value_template = "{{ value_json['state']['open'] }}";
            device_class = "door";
          }
          {
            platform = "rest";
            name = "c3d2";
            unique_id = "status_c3d2";
            resource = "http://schalter.hq.c3d2.de/schalter.json";
            method = "GET";
            scan_interval = 60;
            verify_ssl = true;
            value_template = "{{ value_json['status'] }}";
            device_class = "door";
          }
        ];
        homeassistant = {
          external_url = "https://home-assistant.hq.c3d2.de";
          latitude = "51.08105";
          longitude = "13.72867";
          name = "C3D2";
        };
        http = rec {
          # TODO: turn on when the public-access-proxy is using PROXY PROTOCOL
          # ip_ban_enabled = true;
          # login_attempts_threshold = 5;
          server_host = [
            "127.0.0.1"
            "::1"
          ];
          trusted_proxies = server_host;
          use_x_forwarded_for = true;
        };
      };
      extraComponents = [
        "esphome"
        # https://github.com/NixOS/nixpkgs/issues/330377
        # TODO: move to nixos-modules?
        "isal"
        "met" # Meteorologisk institutt aka the weather widget
        "mqtt"
        "wled"
        "zha" # Zigbee
      ];
      ldap.enable = true;
      package = pkgs.home-assistant.override {
        # those tests take a long(er) time and can't be sped up with pytest-xdist
        packageOverrides = _: prev: let
          noTests.doCheck = false;
        in {
          aws-sam-translator = prev.aws-sam-translator.overridePythonAttrs (_: noTests);
          moto = prev.moto.overridePythonAttrs (_: noTests);
        };
      };
    };

    nginx = {
      enable = true;
      virtualHosts."home-assistant.hq.c3d2.de" = {
        forceSSL = true;
        enableACME = true;
        listen = libC.defaultListen;
        locations."/" = {
          proxyPass = "http://127.0.0.1:${toString config.services.home-assistant.config.http.server_port}";
          proxyWebsockets = true;
        };
      };
    };
  };

  sops.defaultSopsFile = ./secrets.yaml;

  system.stateVersion = "22.11";
}
