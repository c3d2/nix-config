# MediaGoblin

## Reprocess failed media

MediaGoblin is not a very robust software and some media might fail processing because of issues in the module we wrote or other things.
To reprocess them run the following command: ``mediagoblin-gmg reprocess initial``

## Delete media

If it is for whatever reason necessary to delete media, this can be done with ``mediagoblin-gmg deletemedia 774``.
