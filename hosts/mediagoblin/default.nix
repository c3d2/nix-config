{ config, lib, libC, pkgs, ... }:

{
  c3d2.deployment.server = "server9";

  microvm.mem = (2 * 1024)+1;

  networking.hostName = "mediagoblin";

  nixpkgs.overlays = [ (final: prev: {
    mediagoblin = prev.mediagoblin.overridePythonAttrs ({ patches ? [ ], ... }: {
      patches = patches ++ [
        ./emulate-double-bind.diff
      ];
    });
  }) ];

  services = {
    backup = {
      enable = true;
      paths = [ "/var/lib/mediagoblin/" ];
    };

    mediagoblin = {
      enable = true;
      domain = "media.c3d2.de";
      pluginPackages = [
        (with pkgs; python3.pkgs.buildPythonPackage {
          pname = "mediagoblin_svg";
          version = "0-unstable-2023-09-29";

          src = fetchFromGitHub {
            owner = "commonsmachinery";
            repo = "mediagoblin_svg";
            rev = "2ae4cdc18c7bc92d8531ef9b4e9dc7ef70443a3b";
            hash = "sha256-weJEyQo7o+XDlPsDs/VNtXdCNzGxHuRSuvkuqxEY56I=";
          };

          postPatch = ''
            substituteInPlace mediagoblin_svg/processing.py \
              --replace-fail "'rsvg'" "'${lib.getExe librsvg}'" \
              --replace-fail "png_filename]" '"-o", png_filename]'
          '';

          nativeCheckInputs = with python3.pkgs; [ mediagoblin ];
        })

        (with pkgs; python3.pkgs.buildPythonPackage {
          pname = "mediagoblin-basicsearch";
          version = "0-unstable-2024-10-23";

          src = fetchFromGitHub {
            owner = "SuperSandro2000";
            repo = "mediagoblin-basicsearch";
            rev = "f9428702f91992c0696fef1cee16a6993989d302";
            hash = "sha256-ZpVk8dBx/MJBKCmNDWq7RPWuTkl032z3FzkU0jtHaeg=";
          };

          nativeCheckInputs = with python3.pkgs; [ mediagoblin ];
        })
      ];
      settings.mediagoblin = {
        allow_registration = true; # allows creating new ldap accounts
        email_debug_mode = false;
        email_sender_address = "noreply@c3d2.de";
        email_smtp_host = "mail.flpk.zentralwerk.org";
        email_smtp_port = 25;
        email_smtp_force_starttls = true;
        # TODO: move to nixos-modules
        plugins = lib.mkForce {
          "mediagoblin.media_types.ascii" = { };
          "mediagoblin.media_types.image" = { };
          "mediagoblin.media_types.pdf" = { };
          "mediagoblin.media_types.video" = { };
          "mediagoblin.plugins.geolocation" = { };
          "mediagoblin.plugins.ldap"."c3d2" = let
            inherit (config.security) ldap;
          in {
            EMAIL_SEARCH_FIELD = ldap.mailField;
            LDAP_SEARCH_BASE = ldap.userBaseDN;
            LDAP_SERVER_URI = ldap.serverURI;
            LDAP_START_TLS = false;
            LDAP_USER_DN_TEMPLATE = "uid={username},${ldap.userBaseDN}";
          };
          "mediagoblin.plugins.processing_info" = { };
          "mediagoblin.plugins.trim_whitespace" = { };
          "mediagoblin.plugins.basicsearch" = { };
          "mediagoblin_svg" = {
            svg_previews = true;
            svg_thumbnails = false;
          };
        };
      };
    };

    nginx = {
      commonHttpConfig = /* nginx */ ''
        proxy_headers_hash_bucket_size 64;
      '';
      virtualHosts."${config.services.mediagoblin.domain}" = {
        enableACME = true;
        listen = libC.defaultListen;
      };
    };

    postgresql = {
      package = pkgs.postgresql_17;
      upgrade.stopServices = [ "mediagoblin-celeryd" "mediagoblin-paster" ];
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets."mediagoblin/environment" = { };
  };

  system.stateVersion = "24.05";

  systemd.services.mediagoblin-paster.serviceConfig.EnvironmentFile = config.sops.secrets."mediagoblin/environment".path;
}
