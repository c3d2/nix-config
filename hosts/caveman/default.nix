{ config, lib, libC, zentralwerk, pkgs, ... }:

{
  system.stateVersion = "22.05";

  c3d2 = {
    deployment = {
      server = "server10";
      mounts = lib.mkForce [ "etc" "home" ];
      nixStoreBackend = "blk";
    };
    statistics.enable = true;
  };

  microvm = {
    vcpu = 6;
    mem = 10 * 1024;
    balloonMem = 4 * 1024;
    volumes = [ {
      image = "/dev/zvol/server10-root/vm/caveman/var.img";
      mountPoint = "/var";
      autoCreate = false;
    } ];
  };

  networking = {
    firewall = {
      allowedTCPPorts = [ /* telnet */ 23 ];
      extraInputRules = let
        inherit (zentralwerk.lib.config.site.net) serv;
        inherit (config.services) caveman;
      in ''
        ip saddr { ${lib.concatStringsSep ", " libC.subnets.v4} } tcp dport ${toString caveman.hunter.settings.prometheus_port} accept comment "node_exporter from internal"
        ip saddr { ${lib.concatStringsSep ", " libC.subnets.v4} } tcp dport ${toString caveman.sieve.settings.prometheus_port} accept comment "node_exporter from internal"
        ip6 saddr { ${lib.concatStringsSep ", " libC.subnets.v6} } tcp dport ${toString caveman.hunter.settings.prometheus_port} accept comment "node_exporter from internal"
        ip6 saddr { ${lib.concatStringsSep ", " libC.subnets.v6} } tcp dport ${toString caveman.sieve.settings.prometheus_port} accept comment "node_exporter from internal"

        # Allow redis for buzzrelay
        ip saddr { ${serv.hosts4.buzzrelay} } tcp dport ${toString caveman.redis.port} accept comment "redis from buzzrelay"
        ip6 saddr { ${lib.concatStringsSep ", " (map ({ buzzrelay, ... }: buzzrelay) (lib.attrValues serv.hosts6))} } tcp dport ${toString caveman.redis.port} accept comment "redis from buzzlrelay"
      '';
    };
    hostName = "caveman";
    nftables.enable = true;
  };

  services = {
    journald.extraConfig = ''
      Storage=volatile
    '';

    postgresql = {
      ensureDatabases = [ "caveman" ];
      package = pkgs.postgresql_17;
      upgrade.stopServices = [ "caveman-gatherer" "collectd" ];
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "redis/caveman/requirePass".mode = "0444";
      "redis/cache/requirePass".mode = "0444";
      # Must be readable for DynamicUser caveman-sieve
      "caveman/sieve/privKey".mode = "0444";
    };
  };

  services = {
    redis.servers.caveman = {
      # Listen on the public network
      bind = null;
      # Override default backup schedule to reduce I/O
      save = [
        # Every 2h if at least 1 entry changed
        [ 7200 1 ]
        # # Every 30min if at least 10000 entries changed
        # [ 1800 10000 ]
      ];
    };

    caveman = {
      enable = true;
      redis = {
        maxmemory = "4gb";
        passwordFile = config.sops.secrets."redis/caveman/requirePass".path;
      };
      cacheRedis = {
        passwordFile = config.sops.secrets."redis/cache/requirePass".path;
      };

      hunter = {
        enable = true;
        settings = {
          prometheus_port = 9103;
          max_workers = 16;
          hosts = with builtins;
            filter (line: isString line && line != "") (
              split "\n" (
                readFile ./mastodon-instances.txt
              )
            );
        };
      };
      sieve = {
        enable = true;
        settings.priv_key_file = config.sops.secrets."caveman/sieve/privKey".path;
      };
      butcher.enable = true;
      gatherer.enable = true;
      smokestack.enable = true;
      cacher.enable = true;
    };

    nginx = {
      enable = true;
      virtualHosts."fedi.buzz" = {
        default = true;
        forceSSL = true;
        enableACME = true;
        serverAliases = [
          "www.fedi.buzz"
          "caveman.flpk.zentralwerk.org"
        ];
        locations = {
          "/".proxyPass = "http://127.0.0.1:${toString config.services.caveman.gatherer.settings.listen_port}/";
          "/metrics".extraConfig = libC.hqNetworkOnly;
        };
      };
    };
  };

  systemd.services.caveman-hunter.serviceConfig = {
    MemoryHigh = lib.mkForce "8G";
    MemoryMax = lib.mkForce "16G";
    LimitRSS = lib.mkForce "8G";
  };
}
