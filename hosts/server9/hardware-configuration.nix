{ config, lib, ... }:

{
  boot.initrd.availableKernelModules = [ "uhci_hcd" "ehci_pci" "ahci" "mpt3sas" "usb_storage" "usbhid" "sd_mod" "sr_mod" ];
  boot.kernelModules = [ "kvm-intel" ];

  boot.initrd.luks.devices = {
    "server9-root".device = "/dev/disk/by-uuid/351982ff-20a5-4aa0-aa54-896784d6e899";
    "server9-storage1".device = "/dev/disk/by-uuid/1340eea4-f79a-4b41-a2e1-dac83b37ba2b";
    "server9-storage2".device = "/dev/disk/by-uuid/1ce58e2c-7bb6-4142-9b4d-ef3bc3661963";
    "server9-storage3".device = "/dev/disk/by-uuid/c5d16c1f-290d-47e0-ad4f-b058273c89a3";
  };

  boot.zfs.extraPools = [ "server9-storage" ];

  fileSystems."/" =
    { device = "server9-root/root";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/nix" =
    { device = "server9-root/nix";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/home" =
    { device = "server9-root/home";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/var/backup" =
    { device = "server9-root/data/backup";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/etc" =
    { device = "server9-root/data/etc";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/nix/var" =
    { device = "server9-root/nix/var";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/nix/store" =
    { device = "server9-root/nix/store";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/var/lib" =
    { device = "server9-root/data/lib";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/var/lib/restic" =
    { device = "server9-root/data/restic";
      fsType = "zfs";
      options = [ "zfsutil" ];
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/12CE-A600";
      fsType = "vfat";
      options = [ "fmask=0022" "dmask=0022" ];
    };

  swapDevices = [ ];

  networking.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
