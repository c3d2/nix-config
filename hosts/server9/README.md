# Server9

## libvirtd

The connection string for virsh is: `qemu+ssh://root@server9.cluster.zentralwerk.org/system`.

If you want to use a GUI instead, virt-manager is a good option. You can connect under File > Add Connection.
Then choose QEMU/KVM as the Hypervisor, connect via ssh, `root` as username and `server9.cluster.zentralwerk.org` as host and click Connect.
