{ config, lib, options, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  c3d2 = {
    baremetal = true;
    deployment.microvmBaseZfsDataset = "server9-storage/microvms";
    statistics.enable = true;
  };

  boot = {
    extraModprobeConfig = ''
      options zfs zfs_arc_max=${toString (1024*1024*1024*48)}
    '';
    initrd = {
      availableKernelModules = [ "igb" ];
      network = {
        enable = true;
        ssh.enable = true;
      };
    };
    loader.grub = lib.mkIf (!options?isoImage) {
      enable = true;
      device = "/dev/disk/by-id/ata-Samsung_SSD_860_EVO_1TB_S3Z9NB0M203731L";
    };
    tmp = {
      useTmpfs = true;
      tmpfsSize = "80%";
    };
  };

  disko.disks = [ {
    device = "/dev/disk/by-id/ata-Samsung_SSD_860_EVO_1TB_S3Z9NB0M203731L";
    name = "root";
    partitionTableFormat = "msdos";
    withBoot = true;
    withLuks = true;
  } ];

  networking = {
    hostName = "server9";
    hostId = "d4c0fb69";
  };

  # required by libvirtd
  security.polkit.enable = true;

  services = {
    nginx = {
      enable = true;
      additionalModules = [ pkgs.nginxModules.fancyindex ];
      virtualHosts."server9.cluster.zentralwerk.org" = {
        default = true;
        forceSSL = true;
        enableACME = true;
        locations = {
          "/archive".return = "307 /archive/";
          "/archive/" = {
            alias = "/tank/owncast-archive/";
            extraConfig = ''
              fancyindex on;
              fancyindex_exact_size off;
            '';
          };
          "/restic/" = {
            proxyPass = "http://${config.services.restic.server.listenAddress}/";
            extraConfig = ''
              client_max_body_size 40M;
              proxy_buffering off;
            '';
          };
        };
      };
    };

    restic.server = {
      enable = true;
      listenAddress = "127.0.0.1:8080";
      privateRepos = true;
    };
  };

  simd.arch = "westmere";

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "machine-id" = {
        mode = "444";
        path = "/etc/machine-id";
      };
      # "ceph/osd.3/keyfile" = { };
      # "ceph/osd.7/keyfile" = { };
      "restic/htpasswd" = {
        owner = config.systemd.services.restic-rest-server.serviceConfig.User;
        path = "/var/lib/restic/.htpasswd";
      };
    };
  };

  system.stateVersion = "24.05";

  users = {
    motd = ''

        ______    ______
       / / / /   / /\ \ \
      / / / /   / /  \ \ \
      \ \ \ \  / /   / / /
       \_\_\_\/_/   /_/_/

      #############################################################
      #                                                           #
      #  Create a dataset for each user and function in storage!  #
      #                                                           #
      #############################################################

    '';
    motdFile = null;
    users.root.openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCvbeMnZGlujN9IsLxUSTuHBUESoN6ZTXmEFi/GkBKp9RQ7xZAS84JDcV0GFddiCfTHGQL04mkeYXkof5d9NkMWjtTvQ9QFtUgGOqI3PbGCGW83vSwJHsqVbfbv5uqFCODjpTx4/WiJNkEpN/HgE2eIYFcm+Dcpo1vTgC26LjdMfqUaAGv/OTmKWmaMqyOzc4MXuUiBQT83oTfn2zSWDEOJG67yULqg5jMzaXqzPbhsaokBIPehks0bJR/q7OWNwDQJyM1oTKeRIh0ZmC0qYwGu/vlc3rYW4cp//ErAXG5iPfCXXUySUc0DTFHazzkNzUStuUwugznxuUCgbvg2tu5WEnrh/PpzIMuvNaUk2ZSDq00hHDrpVJyKKCih2d1cGV+qs9XFjDoD6Ch2NiSdPnG7fSsTxVw1SB7/APuMqfXWrQDfT5ee5xOKGzKOxQckGihQi39kmqf4WjNESOw4tmhNFZOee9dpC7ESYSpG/3zK8WG6FDln1nU6rHGjVgaSK6HwZUOsneS2fntiQZR2olzEY9+ybL1vh1fk6pkhS78X/PiwS3cSiu4q7JWJ4tUiB0T/cHAFBaODDKHXmEaGefLrPBWLLFRdjbnepWo1d0k1z/NLVovXDCGlF4IOBKqcImIffods/jTpH0hjU/vBjIQHH3Ii0J6vtGJw9whcNAGQfw== t3sserakt@posteo.de"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOUTfQYeOJ7wvBydRqT2bD3XK4jTzrxfszq/PZmFSIt6 mikolai.guetschow@tu-dresden.de"
    ];
  };

  # TODO: enable for zw-ev and poelzi-ha until we find a better solution
  virtualisation.libvirtd = {
    enable = true;
    onShutdown = "shutdown";
  };
}
