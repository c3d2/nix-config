{ config, libC, pkgs, ... }:

{
  c3d2 = {
    deployment.server = "server10";
    sendmail = {
      enable = true;
      name = "Matemat";
    };
  };

  microvm.mem = 2 * 1024;

  networking.hostName = "matemat";

  services = {
    backup.enable = true;

    nginx = {
      enable = true;
      virtualHosts."matemat.hq.c3d2.de" = {
        default = true;
        forceSSL = true;
        enableACME = true;
        listen = libC.defaultListen;
        locations."/" = {
          proxyPass = "http://127.0.0.1:3000";
          extraConfig = libC.hqNetworkOnly + ''
            add_header X-Robots-Tag "noindex" always;

            auth_basic secured;
            auth_basic_user_file ${config.sops.secrets."nginx/basic-auth".path};
          '';
        };
        serverAliases = [ "mate.c3d2.de" "matemat.c3d2.de" ];
      };
    };

    postgresql = {
      package = pkgs.postgresql_17;
      upgrade.stopServices = [ "yammat" ];
    };

    yammat = {
      enable = true;
      config = /* yaml */ ''
        copyright_link: "https://gitea.c3d2.de/c3d2/yammat"
        email:
        - sandro@c3d2.de
        - vater@c3d2.de
        - vv01lf@c3d2.de
      '';
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "nginx/basic-auth".owner = "nginx";
    };
  };

  system.stateVersion = "22.05";
}
