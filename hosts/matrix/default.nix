{ config, libC, pkgs, ... }:

{
  c3d2.deployment = {
    server = "server10";
    nixStoreBackend = "blk";
  };
  microvm = {
    mem = 2 * 1024;
    vcpu = 2;
  };

  networking.hostName = "matrix";

  nixpkgs.overlays = [
    (final: prev: {
      matrix-synapse = prev.matrix-synapse.overrideAttrs (_: {
        # fail and take a good amount of time
        doCheck = false;
      });
    })
  ];

  services = {
    backup = {
      enable = true;
      paths = [ "/var/lib/matrix-synapse/" ];
    };

    matrix-synapse = {
      enable = true;
      element-web = {
        enable = true;
        domain = "element.c3d2.de";
      };
      domain = "matrix.c3d2.de";
      extraConfigFiles = [
        config.sops.secrets."matrix-synapse/config".path
      ];
      listenOnSocket = true;
      ldap = {
        enable = true;
        searchUserPasswordFile = config.sops.secrets."matrix-synapse/ldapSearchUserPassword".path;
      };
      settings = {
        admin_contact = "mailto:root@c3d2.de";
        email = {
          enable_notifs = true;
          notif_for_new_users = false;
          notif_from = "Your Friendly %(app)s homeserver <noreply@c3d2.de>";
          require_transport_security = true;
          smtp_host = "mail.flpk.zentralwerk.org";
        };
        enable_registration = false;
        registration_requires_token = true;
        report_stats = false;
        retention = {
          enabled = true;
          default_policy = {
            min_lifetime = "1d";
            max_lifetime = "1y";
          };
        };
        server_name = "c3d2.de";
        serve_server_wellknown = true;
        url_preview_enabled = true;
        user_ips_max_age = "7d";
      };
    };

    matterbridge = {
      enable = true;
      configPath = config.sops.secrets."matterbridge/config".path;
    };

    nginx = {
      enable = true;
      virtualHosts = {
        "element.c3d2.de" = {
          enableACME = true;
          listen = libC.defaultListen;
        };
        "matrix.c3d2.de" = {
          forceSSL = true;
          enableACME = true;
          listen = libC.defaultListen;
          locations."^~ /_synapse/admin/".return = "403";
        };
      };
    };

    postgresql = {
      enable = true;
      ensureUsers = [{
        name = "matrix-synapse";
      }];
      # TODO: move into nixos-modules?
      initialScript = pkgs.writeText "synapse-init.sql" ''
        CREATE ROLE "matrix-synapse" WITH LOGIN;
        CREATE DATABASE "matrix-synapse" WITH OWNER "matrix-synapse"
          TEMPLATE template0
          LC_COLLATE = "C"
          LC_CTYPE = "C";
      '';
      package = pkgs.postgresql_17;
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "matterbridge/config".owner = "matterbridge";
      "matrix-synapse/config".owner = "matrix-synapse";
      "matrix-synapse/ldapSearchUserPassword".owner = "matrix-synapse";
    };
  };

  system.stateVersion = "22.11";
}
