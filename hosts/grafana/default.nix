{ config, lib, libC, pkgs, ... }:

{
  c3d2.deployment = {
    server = "server10";
    nixStoreBackend = "blk";
  };

  microvm = {
    mem = 2 * 1024;
  };

  environment.systemPackages = with pkgs; [ influxdb ];

  networking = {
    firewall = {
      # influxdb
      allowedTCPPorts = [ 8086 ];
      # collectd
      allowedUDPPorts = [ 25826 ];
    };
    hostName = "grafana";
  };

  services = {
    backup = {
      enable = true;
      paths = [ "/var/lib/grafana/" ];
    };

    grafana = {
      enable = true;
      configureNginx = true;
      oauth = {
        enable = true;
        adminGroup = "grafana-admins";
        enableViewerRole = true;
        userGroup = "grafana-users";
      };

      provision = {
        enable = true;
        datasources.settings.datasources = map
          (datasource: {
            inherit (datasource) name type access orgId url user database isDefault jsonData;
          })
          (builtins.fromJSON (lib.readFile ./datasources.json));
        dashboards.settings.providers = [{
          settings = {
            apiVersion = 1;
            providers = [{
              name = "c3d2";
            }];
          };
          options.path = ./dashboards;
        }];
      };

      settings = {
        "auth.anonymous" = {
          enabled = true;
          org_name = "Chaos";
        };
        "auth.generic_oauth".client_secret = "$__file{${config.sops.secrets."grafana/client-secret".path}}";
        security = {
          admin_password = "$__file{${config.sops.secrets."grafana/admin-password".path}}";
          secret_key = "$__file{${config.sops.secrets."grafana/secret-key".path}}";
        };
        server.domain = "grafana.c3d2.de";
        users.allow_sign_up = false;
      };
    };

    influxdb =
      let
        collectdTypes = pkgs.runCommand "collectd-types" { } ''
          mkdir -p $out/share/collectd
          cat ${pkgs.collectd-data}/share/collectd/types.db >> $out/share/collectd/types.db
          echo "stations  value:GAUGE:0:U" >> $out/share/collectd/types.db
        '';
      in
      {
        enable = true;
        extraConfig = {
          logging.level = "debug";
          collectd = [{
            enabled = true;
            database = "collectd";
            typesdb = "${collectdTypes}/share/collectd/types.db";
            # create retention policy "30d" on collectd duration 30d replication 1 default
            retention-policy = "30d";
          }];
        };
      };

    nginx = {
      enable = true;
      virtualHosts = {
        "${config.services.grafana.settings.server.domain}" = {
          default = true;
          enableACME = true;
          forceSSL = true;
          listen = libC.defaultListen;
        };

        "grafana.hq.c3d2.de" = {
          enableACME = true;
          forceSSL = true;
          listen = libC.defaultListen;
          locations."/".return = "307 https://grafana.c3d2.de$request_uri";
        };
      };
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = let
      grafana = config.systemd.services.grafana.serviceConfig.User;
    in {
      "grafana/admin-password".owner = grafana;
      "grafana/client-secret".owner = grafana;
      "grafana/secret-key".owner = grafana;
    };
  };

  systemd.services = {
    # work around our slow storage that can't keep up
    influxdb.serviceConfig.LimitNOFILE = "1048576:1048576";
    influxdb.serviceConfig.TimeoutStartSec = "infinity";
  };

  system.stateVersion = "22.05";

  users.users.nginx.extraGroups = [ "grafana" ];
}
