#!/usr/bin/env bash
cd "$(dirname "$0")" || exit

curl -s "https://root:$SECRET@grafana.c3d2.de/api/datasources" | jq > datasources.json

for uid in $(curl -s "https://root:$SECRET@grafana.c3d2.de/api/search" | jq -j 'map(.uid) | join(" ")'); do
  response="$(curl -s "https://root:$SECRET@grafana.c3d2.de/api/dashboards/uid/$uid")"
  title="$(echo "$response" | jq -r ".dashboard.title")"
  echo "$response" | jq .dashboard > "dashboards/$title.json"
done
