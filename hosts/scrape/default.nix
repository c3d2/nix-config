{ config, lib, libC, pkgs, scrapers, ... }:

let
  freifunkNodes = {
    "1884" = "10.200.7.100";
    "1891" = "10.200.7.107";
    "1099" = "10.200.4.80";
    "1864" = "10.200.7.80";
  };
in {
  c3d2.deployment.server = "server10";

  microvm.mem = 1024;

  networking.hostName = "scrape";

  users.groups.scrape = {};
  users.users.scrape = {
    isNormalUser = true;
    group = "scrape";
    # don't make /home/scrape inaccessible by nginx
    createHome = false;
  };

  services.nginx = {
    enable = true;
    virtualHosts."scrape.hq.c3d2.de" = {
      default = true;
      forceSSL = true;
      enableACME = true;
      locations."/".root = config.users.users.scrape.home;
      listen = libC.defaultListen;
      extraConfig = ''
        autoindex on;
      '';
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "scrape/matemat/user".owner = config.users.users.scrape.name;
      "scrape/matemat/password".owner = config.users.users.scrape.name;
      # "scrape/xeri/user".owner = config.users.users.scrape.name;
      # "scrape/xeri/password".owner = config.users.users.scrape.name;
      "scrape/solar/user".owner = config.users.users.scrape.name;
      "scrape/solar/password".owner = config.users.users.scrape.name;
    };
  };

  systemd.services = let
    serviceConfig = {
      User = config.users.users.scrape.name;
      Group = config.users.users.scrape.group;
    };
    scraperPkgs = import scrapers { inherit pkgs; };
    makeService = {
      script,
      host ? "",
      userFile ? "",
      passwordFile ? ""
    }: {
      script = "${lib.getExe scraperPkgs."${script}"} ${host} ${lib.optionalString (userFile != "") ''"$(cat ${userFile})"''} ${lib.optionalString (passwordFile != "") ''"$(cat ${passwordFile})"''}";
      inherit serviceConfig;
    };
    makeNodeScraper = nodeId: {
      name = "scrape-node${nodeId}";
      value = makeService {
        script = "freifunk_node";
        host = freifunkNodes."${nodeId}";
      };
    };
  in {
    nginx.serviceConfig.ProtectHome = "read-only";

    # scrape-xeri = makeService {
    #   script = "xerox";
    #   host = "xeri.hq.c3d2.de";
    #   userFile = config.sops.secrets."scrape/xeri/user".path;
    #   passwordFile = config.sops.secrets."scrape/xeri/password".path;
    # };
    # scrape-roxi = makeService {
    #   script = "xerox";
    #   host = "roxi.hq.c3d2.de";
    # };
    scrape-matemat = makeService {
      script = "matemat";
      host = "matemat.hq.c3d2.de";
      userFile = config.sops.secrets."scrape/matemat/user".path;
      passwordFile = config.sops.secrets."scrape/matemat/password".path;
    };
    scrape-solar = makeService {
      script = "solar";
      host = "schuggi2.de";
      userFile = config.sops.secrets."scrape/solar/user".path;
      passwordFile = config.sops.secrets."scrape/solar/password".path;
    };
    scrape-riesa-efau-kalender = {
      script = ''
        ${scraperPkgs.riesa-efau-kalender}/bin/riesa-efau-kalender > /tmp/riesa-efau-kalender.ics
        mv /tmp/riesa-efau-kalender.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-kreuzchor-termine = {
      script = ''
        ${scraperPkgs.kreuzchor-termine}/bin/kreuzchor-termine > /tmp/kreuzchor-termine.ics
        mv /tmp/kreuzchor-termine.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-mkz-programm = {
      script = ''
        ${scraperPkgs.mkz-programm}/bin/mkz-programm > /tmp/mkz-programm.ics
        mv /tmp/mkz-programm.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-drk-impfaktionen = {
      script = ''
        ${scraperPkgs.drk-impfaktionen}/bin/drk-impfaktionen > /tmp/drk-impfaktionen.ics
        mv /tmp/drk-impfaktionen.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-zuendstoffe = {
      script = ''
        ${scraperPkgs.zuendstoffe}/bin/zuendstoffe > /tmp/zuendstoffe.xml
        mv /tmp/zuendstoffe.xml ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-dresden-versammlungen = {
      script = ''
        ${scraperPkgs.dresden-versammlungen}/bin/dresden-versammlungen > /tmp/dresden-versammlungen.ics
        mv /tmp/dresden-versammlungen.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-azconni = {
      script = ''
        ${scraperPkgs.azconni}/bin/azconni > /tmp/azconni.ics
        mv /tmp/azconni.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-kunsthaus = {
      script = ''
        ${scraperPkgs.kunsthaus}/bin/kunsthaus > /tmp/kunsthaus.ics
        mv /tmp/kunsthaus.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-staatsoperette = {
      script = ''
        ${scraperPkgs.staatsoperette}/bin/staatsoperette > /tmp/staatsoperette.ics
        mv /tmp/staatsoperette.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-dresden-kulturstadt = {
      script = ''
        ${scraperPkgs.dresden-kulturstadt}/bin/dresden-kulturstadt > /tmp/dresden-kulturstadt.ics
        mv /tmp/dresden-kulturstadt.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-nabu = {
      script = ''
        ${scraperPkgs.nabu}/bin/nabu > /tmp/nabu.ics
        mv /tmp/nabu.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-museen-dresden = {
      script = ''
        ${scraperPkgs.museen-dresden}/bin/museen-dresden > /tmp/museen-dresden.ics
        mv /tmp/museen-dresden.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-criticalmass = {
      script = ''
        ${scraperPkgs.criticalmass}/bin/criticalmass > /tmp/criticalmass.ics
        mv /tmp/criticalmass.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-rauze = {
      script = ''
        ${scraperPkgs.rauze}/bin/rauze > /tmp/rauze.ics
        mv /tmp/rauze.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-hfbk-dresden = {
      script = ''
        ${scraperPkgs.hfbk-dresden}/bin/hfbk-dresden > /tmp/hfbk-dresden.ics
        mv /tmp/hfbk-dresden.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-dresden-ikt = {
      script = ''
        ${scraperPkgs.dresden-ikt}/bin/dresden-ikt > /tmp/dresden-ikt.ics
        mv /tmp/dresden-ikt.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-dresdencontemporaryart = {
      script = ''
        ${scraperPkgs.dresdencontemporaryart}/bin/dresdencontemporaryart > /tmp/dresdencontemporaryart.ics
        mv /tmp/dresdencontemporaryart.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-htw-dresden = {
      script = ''
        ${scraperPkgs.htw-dresden}/bin/htw-dresden > /tmp/htw-dresden.ics
        mv /tmp/htw-dresden.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-bibo-dresden = {
      script = ''
        ${scraperPkgs.bibo-dresden}/bin/bibo-dresden > /tmp/bibo-dresden.ics
        mv /tmp/bibo-dresden.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-johannstadthalle = {
      script = ''
        ${scraperPkgs.johannstadthalle}/bin/johannstadthalle > /tmp/johannstadthalle.ics
        mv /tmp/johannstadthalle.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-johannstadt = {
      script = ''
        ${scraperPkgs.johannstadt}/bin/johannstadt > /tmp/johannstadt.ics
        mv /tmp/johannstadt.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-kosmotique = {
      script = ''
        ${scraperPkgs.kosmotique}/bin/kosmotique > /tmp/kosmotique.ics
        mv /tmp/kosmotique.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-zwickmuehle = {
      script = ''
        ${scraperPkgs.zwickmuehle}/bin/zwickmuehle > /tmp/zwickmuehle.ics
        mv /tmp/zwickmuehle.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
    scrape-sowieso = {
      script = ''
        ${scraperPkgs.sowieso}/bin/sowieso > /tmp/sowieso.ics
        mv /tmp/sowieso.ics ${config.users.users.scrape.home}/
      '';
      inherit serviceConfig;
    };
  } // builtins.listToAttrs
    (map makeNodeScraper (builtins.attrNames freifunkNodes)
    );

  systemd.timers = let
    makeTimer = service: interval: {
      partOf = [ "${service}.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = interval;
    };
    makeNodeScraperTimer = nodeId:
      let name = "scrape-node${nodeId}";
      in {
        inherit name;
        value = makeTimer name "minutely";
      };
  in {
    # scrape-xeri = makeTimer "scrape-xeri.service" "minutely";
    # scrape-roxi = makeTimer "scrape-roxi.service" "minutely";
    scrape-matemat = makeTimer "scrape-matemat.service" "minutely";
    scrape-solar = makeTimer "scrape-solar.service" "hourly";
    scrape-riesa-efau-kalender = {
      partOf = [ "scrape-riesa-efau-kalender.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-kreuzchor-termine = {
      partOf = [ "scrape-kreuzchor-termine.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "daily";
    };
    scrape-mkz-programm = {
      partOf = [ "scrape-mkz-programm.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-drk-impfaktionen = {
      partOf = [ "scrape-drk-impfaktionen.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-zuendstoffe = {
      partOf = [ "scrape-zuendstoffe.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-dresden-versammlungen = {
      partOf = [ "scrape-dresden-versammlungen.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-azconni = {
      partOf = [ "scrape-azconni.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-kunsthaus = {
      partOf = [ "scrape-kunsthaus.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-staatsoperette = {
      partOf = [ "scrape-staatsoperette.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-dresden-kulturstadt = {
      partOf = [ "scrape-dresden-kulturstadt.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-nabu = {
      partOf = [ "scrape-nabu.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-museen-dresden = {
      partOf = [ "scrape-museen-dresden.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-criticalmass = {
      partOf = [ "scrape-criticalmass.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-rauze = {
      partOf = [ "scrape-rauze.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-hfbk-dresden = {
      partOf = [ "scrape-hfbk-dresden.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-dresden-ikt = {
      partOf = [ "scrape-dresden-ikt.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-dresdencontemporaryart = {
      partOf = [ "scrape-dresdencontemporaryart.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-htw-dresden = {
      partOf = [ "scrape-htw-dresden.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-bibo-dresden = {
      partOf = [ "scrape-bibo-dresden.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-johannstadthalle = {
      partOf = [ "scrape-johannstadthalle.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-johannstadt = {
      partOf = [ "scrape-johannstadt.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-kosmotique = {
      partOf = [ "scrape-kosmotique.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-zwickmuehle = {
      partOf = [ "scrape-zwickmuehle.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
    scrape-sowieso = {
      partOf = [ "scrape-sowieso.service" ];
      wantedBy = [ "timers.target" ];
      timerConfig.OnCalendar = "hourly";
    };
  } // builtins.listToAttrs
    (map makeNodeScraperTimer (builtins.attrNames freifunkNodes)
    );

  system.stateVersion = "20.03";
}
