{ config, libC, pkgs, ... }:

{
  c3d2.deployment = {
    server = "server10";
    nixStoreBackend = "blk";
  };

  microvm = {
    balloon = true;
    mem = 2 * 1024;
  };

  networking.hostName = "drone";

  services = {
    nginx = {
      enable = true;
      virtualHosts."drone.c3d2.de" = {
        forceSSL = true;
        enableACME = true;
        listen = libC.defaultListen;
        locations."/".proxyPass = "http://localhost:5000";
      };
    };

    postgresql = {
      enable = true;
      ensureDatabases = [
        "drone"
      ];
      ensureUsers = [{
        name = "drone";
        ensureDBOwnership = true;
      }];
      package = pkgs.postgresql_17;
      upgrade.stopServices = [ "drone-server" ];
    };
  };

  systemd.services = {
    # TODO: hardening
    # https://github.com/Mic92/dotfiles/commit/ca50aa545934f12999cb58f7cd452876c8b486de#diff-c83b36ea5739cf058ef055b65b20fa5e7fad16135b2d49c0f8968903146b985aL29-L64
    drone-runner-ssh = {
      wantedBy = [ "multi-user.target" ];
      after = [ "drone-server.service" ];
      requires = [ "drone-server.service" ];
      serviceConfig = {
        Environment = [
          "DRONE_RPC_HOST=drone.c3d2.de"
          "DRONE_RPC_PROTO=https"
        ];
        EnvironmentFile = config.sops.secrets."drone/runner/environmentFile".path;
        ExecStart = "${pkgs.drone-runner-ssh}/bin/drone-runner-ssh";
        User = "drone";

        PrivateTmp = true;
        ProtectSystem = "full";
        # ReadWritePaths = [ "/tmp" ];
      };
    };

    drone-server = {
      wantedBy = [ "multi-user.target" ];
      after = [ "nginx.service" ];
      serviceConfig = {
        Environment = [
          "DRONE_DATABASE_DATASOURCE=postgres:///drone?host=/run/postgresql"
          "DRONE_DATABASE_DRIVER=postgres"
          "DRONE_DATADOG_ENABLED=false"
          "DRONE_DATADOG_ENDPOINT=null"
          "DRONE_GITEA_SERVER=https://gitea.c3d2.de"
          "DRONE_SERVER_HOST=drone.c3d2.de"
          "DRONE_SERVER_PORT=:5000"
          "DRONE_SERVER_PROTO=https"
          "DRONE_USER_CREATE=username:sandro,admin:true"
          "DRONE_USER_FILTER=sandro,c3d2"
        ];
        EnvironmentFile = config.sops.secrets."drone/server/environmentFile".path;
        ExecStart = "${pkgs.drone}/bin/drone-server";
        User = "drone";

        PrivateTmp = true;
        ProtectSystem = "full";
      };
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    secrets = {
      "drone/runner/environmentFile".owner = "drone";
      "drone/server/environmentFile".owner = "drone";
    };
  };

  system.stateVersion = "22.11";

  users = {
    groups.drone = { };
    users."drone" = {
      group = "drone";
      isSystemUser = true;
    };
  };

  # only using ssh right now
  # virtualisation.docker = {
  #   enable = true;
  #   autoPrune = {
  #     enable = true;
  #     flags = [
  #       "--all"
  #       "--force"
  #       "--volumes"
  #     ];
  #   };
  # };
}
