{ config, lib, libC, ... }:

let
  cfg = config.c3d2.statistics;

  isMetal = !config.boot.isContainer && !(config ? microvm);

  nginxStatusPort = 9101;
in
{
  options.c3d2.statistics = {
    enable = lib.mkEnableOption "statistics collection";
    # TODO: switch to default = false after migrating prometheus rules away
    withCollectd = lib.mkEnableOption "collectd statistic collection" // { default = true; };
  };

  config = lib.mkIf cfg.enable {
    networking = {
      firewall = {
        allowedTCPPorts = lib.mkIf config.services.nginx.enable [ nginxStatusPort ];
        extraInputRules = ''
          ip saddr { ${lib.concatStringsSep ", " libC.subnets.v4} } tcp dport ${toString config.services.prometheus.exporters.node.port} accept comment "node_exporter from internal"
          ip6 saddr { ${lib.concatStringsSep ", " libC.subnets.v6} } tcp dport ${toString config.services.prometheus.exporters.node.port} accept comment "node_exporter from internal"
        '';
      };
      nftables.enable = true;
    };

    services = {
      collectd = lib.mkIf cfg.withCollectd {
        enable = true;
        extraConfig = ''
          FQDNLookup false
          Interval 10
        '';
        buildMinimalPackage = true;
        plugins = {
          logfile = ''
            LogLevel info
            File STDOUT
          '';
          network = ''
            Server "grafana.serv.zentralwerk.org" "25826"
          '';
          memory = "";
          processes = "";
          disk = "";
          df = "";
          cpu = "";
          entropy = "";
          load = "";
          swap = "";
          vmem = "";
          interface = "";
        } // lib.optionalAttrs isMetal {
          sensors = "";
          cpufreq = "";
          irq = "";
          thermal = "";
        } // lib.optionalAttrs (isMetal && config.nixpkgs.system == "x86_64-linux") {
          ipmi = "";
        } // lib.optionalAttrs config.services.nginx.enable {
          nginx = ''
            URL "http://localhost:${toString nginxStatusPort}/nginx_status"
          '';
        };
      };

      nginx.virtualHosts.localhost = lib.mkIf config.services.nginx.enable {
        listen = [ { addr = "127.0.0.1"; port = nginxStatusPort; } ]
          ++ lib.optional config.networking.enableIPv6 { addr = "[::1]"; port = nginxStatusPort; };
        locations."/nginx_status".extraConfig = /* nginx */ ''
          stub_status;

          access_log off;
          allow 127.0.0.1;
          allow ::1;
          deny all;
        '';
      };

      prometheus.exporters.node = lib.mkIf (config.nixpkgs.system != "riscv64-linux") {
        enable = true;
        enabledCollectors = [ "ethtool" "systemd" ];
        openFirewall = true;
      };
    };
  };
}
