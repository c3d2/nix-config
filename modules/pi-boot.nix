{ config, lib, ... }:

{
  options = {
    boot.loader.generic-extlinux-compatible.firmwareConfig = lib.mkOption {
      type = lib.types.lines;
    };
  };

  config = {
   boot.loader.generic-extlinux-compatible.populateCmd = ''
     cat ${config.boot.loader.generic-extlinux-compatible.firmwareConfig} >> firmware/config.txt
   '';
  };
}
