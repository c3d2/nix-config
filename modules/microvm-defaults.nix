# No MicroVM settings but some defaults that enable evaluating NixOS
# configurations that are destined to be used on Skyflake

{ config, lib, pkgs, ... }:

{
  c3d2 = {
    # autoupdates do not make sense inside MicroVMs with read-only /nix/store
    autoUpdate = false;
    statistics.enable = true;
  };

  boot = {
    loader.grub.enable = false;
    initrd.kernelModules = [
      # required for net.netfilter.nf_conntrack_max appearing in sysfs early at boot
      "nf_conntrack"
    ];
    kernel.sysctl =
      let
        mem = if (config?microvm) then config.microvm.mem else config.deployment.mem;
      in
      lib.optionalAttrs (mem <= 2*1024) {
        # table overflow causing packets from nginx to the service to drop
        # nf_conntrack: nf_conntrack: table full, dropping packet
        "net.netfilter.nf_conntrack_max" = lib.mkDefault "65536";
      };
    kernelParams = [
      # mitigations which cost the most performance and are the least real world relevant
      # NOTE: keep in sync with baremetal.nix
      "retbleed=off"
      "gather_data_sampling=off" # Downfall
    ];
  };

  fileSystems."/" = lib.mkDefault {
    fsType = "tmpfs";
  };

  hardware.enableRedistributableFirmware = false;

  # nix store is mounted read only
  nix = {
    enable = lib.mkDefault false;
    gc.automatic = false;
    optimise.automatic = false;
  };

  system.build.installBootLoader = "${pkgs.coreutils}/bin/true";

  systemd.tmpfiles.rules = [
    "d /home/root 0700 root root -" # createHome does not create it
  ];

  users = {
    mutableUsers = false;
    # store root users files persistent, especially .bash_history
    users."root" = {
      createHome = true;
      home = lib.mkForce "/home/root";
    };
  };
}
