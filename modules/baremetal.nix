{ config, lib, pkgs, utils, ... }:

{
  options.c3d2.baremetal = lib.mkEnableOption "options for baremetal server";

  config = lib.mkIf config.c3d2.baremetal {
    assertions = [
      {
        assertion = config.boot.initrd.network.ssh.enable -> (config.boot.initrd.availableKernelModules != []);
        message = "boot.initrd.availableKernelModules must be set for initrd networking to reliably work!";
      }
    ];

    c3d2.statistics.enable = true;

    boot = {
      initrd = {
        # make sure to set availableKernelModules to the kernel module used by the connected interface(s) otherwise things will not work!
        # the module can be found in a booted system by running `dmesg | rg "Link"` and looking at the first word after the date
        availableKernelModules = [ "bridge" "bonding" "8021q" ];
        network = {
          ssh = {
            authorizedKeys = config.users.users.root.openssh.authorizedKeys.keys;
            configureHostKeys = true;
            generateHostKeys = true;
            port = 4748;
          };
          postCommands = lib.mkIf (!config.boot.initrd.systemd.enable) ''
            cat <<EOF > /root/.profile
            cryptsetup-askpass
            EOF
          '';
        };
        systemd = {
          enable = true;
          inherit (config.systemd) network;
          # TODO: move to nixos-modules
          contents."/etc/profile".text = ''
            systemd-tty-ask-password-agent
          '';
          # TODO: move to nixos-modules
          services = lib.mkIf config.boot.zfs.enabled {
            zfs-import-rpool = let
              devices = map (name: "dev-mapper-${utils.escapeSystemdPath name}.device") (lib.attrNames config.boot.initrd.luks.devices);
            in {
              wants = devices;
              after = devices;
            };
          };
        };
      };
      kernel.sysctl = {
        "vm.swappiness" = 1;
      };
      kernelParams = [
        # "boot.shell_on_fail"
        # mitigations which cost the most performance and are the least real world relevant
        # NOTE: keep in sync with microvm-defaults.nix
        "retbleed=off"
        "gather_data_sampling=off" # Downfall
        "zfs_force=1"
      ];
    };

    environment.systemPackages = with pkgs; [
      freeipmi
      lshw
      pciutils # lscpi
      smartmontools # smartctl
    ] ++ lib.optional config.boot.zfs.enabled sanoid; # mainly for syncoid

    nix.settings.system-features = [
      "kvm" "big-parallel" "nixos-test" "benchmark"
    ];

    powerManagement.cpuFreqGovernor = "schedutil";

    services = {
      # just assume there are ssd's everywhere
      fstrim.enable = true;
      resolved.extraConfig = /* systemd */ ''
        # don't cache NXDOMAIN which often happened for hydra.hq.c3d2.de after a restart
        Cache=no-negative
      '';
      smartd.enable = true;
    };

    users.mutableUsers = false;
  };
}
