{ config, lib, ... }:
let
  cfg = config.services.gitea-actions;
in {
  options.services.gitea-actions.enableRegistrar = lib.mkEnableOption "gitea";

  config.systemd.services = lib.genAttrs (builtins.genList (n: "gitea-runner-nix${builtins.toString n}-token") cfg.numInstances) (name: {
    wantedBy = [ "multi-user.target" ];
    after = lib.optional config.services.gitea.enable "gitea.service";
    unitConfig.ConditionPathExists = [ "!/var/lib/gitea-registration/${name}" ];
    script = ''
      set -euo pipefail
      token=$(${lib.getExe config.services.gitea.package} actions generate-runner-token)
      echo "TOKEN=$token" > /var/lib/gitea-registration/${name}
    '';

    environment = {
      GITEA_CUSTOM = "/var/lib/gitea/custom";
      GITEA_WORK_DIR = "/var/lib/gitea";
    };

    serviceConfig = {
      User = "gitea";
      Group = "gitea";
      StateDirectory = "gitea-registration";
      Type = "oneshot";
      RemainAfterExit = true;
    };
  });
}
