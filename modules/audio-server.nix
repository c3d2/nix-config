{ config, lib, pkgs, ... }:

let
  cfg = config.c3d2.audioServer;
in
{
  options.c3d2.audioServer = {
    enable = lib.mkEnableOption "PulseAudio and Bluetooth sinks";

    ledfx = lib.mkEnableOption "LedFX service";
  };

  config = lib.mkIf cfg.enable ({
    # TODO: enable again, currently broken on unstable
    # boot.kernelPackages = lib.mkOverride 900 pkgs.linuxPackages-rt_latest;

    environment.systemPackages = with pkgs; [
      mpd
      # draws in mesa
      # TODO: drop else on 24.11 upgrade
      (if mpv-unwrapped ? wrapper
       then mpv-unwrapped.wrapper {
         mpv = mpv-unwrapped.override { drmSupport = false; };
       }
       else mpv)
      ncmpcpp
      ncpamixer
      pulseaudio # required for pactl
      somafm-cli
      uhubctl # to toggle usb devices
    ];

    hardware = {
      bluetooth.settings = {
        Policy.AutoEnable = true;
        General.DiscoverableTimeout = 0;
      };

      pulseaudio = {
        enable = !config.services.pipewire.pulse.enable;
        systemWide = true;
        tcp = {
          enable = true;
          anonymousClients.allowedIpRanges = [
            "127.0.0.0/8"
            "::1/128"
            "fd23:42:c3d2:500::/56"
            "172.22.99.0/24"
            "172.20.72.0/21"
            "2a00:8180:2c00:200::/56"
            "2a0f:5382:acab:1400::/56"
          ];
        };
        zeroconf.publish.enable = true;
        package = (pkgs.pulseaudio.override {
          bluetoothSupport = true;
          advancedBluetoothCodecs = true;
          zeroconfSupport = true;
        }).overrideAttrs (_: {
          # one test times out
          doCheck = false;
        });
      };
    };

    networking.firewall = {
      allowedTCPPorts = lib.optional cfg.ledfx 80 ++ [
        4713 # pulseaudio/pipewire tcp sink
      ];
      allowedUDPPorts = [
        5353 # mdns
        9875 # pulseaudio/pipewire rtp sink
      ];
    };

    security = {
      polkit.extraConfig = /* javascript */ ''
        // https://www.reddit.com/r/voidlinux/comments/o74i76/comment/h2z9u11/?utm_source=reddit&utm_medium=web2x&context=3
        polkit.addRule(function(action, subject) {
          if (action.id == "org.freedesktop.RealtimeKit1.acquire-high-priority"
            || action.id == "org.freedesktop.RealtimeKit1.acquire-real-time") {
            return polkit.Result.YES;
          }
        });

        // broader alternative if the above ever breaks
        // polkit.addRule(function(action, subject) {
        //   if (subject.isInGroup("rtkit")) {
        //     if (action.id.indexOf("org.freedesktop.RealtimeKit1.") == 0) {
        //       return polkit.Result.YES;
        //     }
        //   }
        // });
      '';
      rtkit.enable = true;
    };

    services = {
      # tell Avahi to publish services like Pipewire/PulseAudio
      avahi = {
        enable = true;
        nssmdns4 = true;
        publish = {
          enable = true;
          addresses = true;
          userServices = true;
        };
      };

      pipewire = {
        enable = true;
        #    _____ _______ ____  _____
        #   / ____|__   __/ __ \|  __ \
        #  | (___    | | | |  | | |__) |
        #   \___ \   | | | |  | |  ___/
        #   ____) |  | | | |__| | |
        #  |_____/   |_|  \____/|_|
        #
        # errors such as:
        # mod.zeroconf-publish: error id:47 seq:349 res:-2 (No such file or directory): enum params id:16 (Spa:Enum:ParamId:ProcessLatency) failed
        # are harmless and can be ignored. You most likely want to restart your local avahi-daemon: sudo systemctl restart avahi-daemon
        extraConfig = {
          pipewire."audio-server" = {
            "context.modules" = [ {
              # https://wiki.archlinux.org/title/PulseAudio/Examples#PulseAudio_over_network
              name = "libpipewire-module-rtp-sap";
              args = {
                "sess.ignore-ssrc" = true;
                "source.ip" = "0.0.0.0";
              };
            } ];
          };
          pipewire-pulse."audio-server" = {
            "context.exec" = [ {
              path = "${pkgs.pulseaudio}/bin/pactl";
              args = "load-module module-zeroconf-publish";
            } ];
            "pulse.properties" = {
              "auth-ip-acl" = [
                "127.0.0.0/8"
                "::1/128"
                "fd23:42:c3d2:500::/56"
                "172.22.99.0/24"
                "172.20.72.0/21"
                "2a00:8180:2c00:200::/56"
                "2a0f:5382:acab:1400::/56"
              ];
              "pulse.default.tlength" = "96000/48000";
              "server.address" = [
                "unix:native"
                "tcp:4713"
              ];
            };
          };
        };
        alsa.enable = lib.mkIf cfg.ledfx true; # required for ledfx
        pulse.enable = true;
      };
    };

    system.userActivationScripts.symlinkPipewireSystemdUnit.text = lib.mkIf config.c3d2.k-ot.enable /* bash */ ''
      if [[ $USERNAME == k-ot ]]; then
        systemctl --user enable pipewire-pulse
      fi
    '';

    systemd = {
      services = {
        bluetooth-agent = lib.mkIf config.hardware.bluetooth.enable {
          description = "Allow anyone to pair via Bluetooth";
          wantedBy = [ "multi-user.target" ];
          requires = [ "bluetooth.target" ];
          after = [ "bluetooth.service" ];
          serviceConfig = {
            Type = "simple";
            ExecStart = "${pkgs.bluez-tools}/bin/bt-agent -c NoInputNoOutput";
            Restart = "on-failure";
            RestartSec = 60;
          };
        };

        mpv-pause = {
          script = ''
            echo '{ "command": ["set_property", "pause", true] }' | ${lib.getExe pkgs.socat} - /tmp/mpvsocket
          '';
          serviceConfig = {
            Type = "oneshot";
            User = "root";
          };
        };
      };

      timers.mpv-pause = {
        wantedBy = [ "timers.target" ];
        timerConfig = {
          Unit = "mpv-pause.service";
          OnCalendar = "*-*-* 05:30:00";
        };
      };

      user.services = {
        ledfx = lib.mkIf cfg.ledfx {
          after = [ "pipewire.target" ];
          wantedBy = [ "default.target" ];
          serviceConfig = {
            ExecStart = lib.getExe pkgs.ledfx;
            Restart = "on-failure";
          };
        };

        pipewire.serviceConfig = {
          # hope we have internet by now
          RestartSec = "3s";
          StartLimitBurst = "10";
        };

        pipewire-pulse.enable = true;
      };
    };

    users = {
      groups.pulse-access = { };
      users.k-ot = lib.mkIf config.c3d2.k-ot.enable {
        extraGroups = [
          "pipewire"
          "pulse-access" # required for system wide pulseaudio
          "rtkit"
        ];
        linger = true;
        packages = with pkgs; [
          (yt-dlp.override { withAlias = true; })
        ];
      };
    };
  } // lib.optionalAttrs (lib.versionOlder lib.version "24.11") {
    sound.enable = true;
  });
}
