{ lib, zentralwerk, ... }:

{
  options = {
    services.nginx.virtualHosts = lib.mkOption {
      type = with lib.types; attrsOf (submodule ({ config, ... }: let
        cfgv = config;
        proxyProtocol = lib.any (listen: (lib.any (x: x == "proxy_protocol") listen.extraParameters)) cfgv.listen;
      in {
        options = {
          locations = lib.mkOption {
            type = with lib.types; attrsOf (submodule {
              config.extraConfig = lib.mkIf proxyProtocol (with zentralwerk.lib.config.site.net.serv; /* nginx */ ''
                # https://docs.nginx.com/nginx/admin-guide/load-balancer/using-proxy-protocol/
                set_real_ip_from ${hosts4.public-access-proxy};
                set_real_ip_from ${hosts6.up4.public-access-proxy};

                real_ip_header proxy_protocol;

                proxy_set_header Host               $host;
                proxy_set_header X-Real-IP          $proxy_protocol_addr;
                proxy_set_header X-Forwarded-For    $proxy_protocol_addr;
                proxy_set_header X-Forwarded-Proto  $scheme;
                proxy_set_header X-Forwarded-Host   $host;
                proxy_set_header X-Forwarded-Server $host;
              '');
              config.recommendedProxySettings = lib.mkIf proxyProtocol false;
            });
          };
        };
      }));
    };
  };
}
