{ lib
, copyDesktopItems
, fetchFromGitHub
, rustPlatform
, libGL
, makeDesktopItem
, mesa
, xorg
}:

rustPlatform.buildRustPackage rec {
  pname = "allcolors";
  version = "0.1.0";

  src = fetchFromGitHub {
    owner = "polygon";
    repo = "allcolors-rs";
    rev = "023bd480245052357a7fd5f42181ff6e67d98b31";
    sha256 = "sha256-whaV+k5xh01OQNOehwkEBUDpMWn47mvVihVwchBvWoE=";
  };

  cargoPatches = [ ./allcolors-cargo-update.patch ];

  cargoHash = "sha256-RbfACA4hcyemGkw9bqjpIk393SBgBM939I95+grVI0c=";

  nativeBuildInputs = [ copyDesktopItems ];

  buildInputs = [
    xorg.libX11
    xorg.libXcursor
    xorg.libXrandr
    xorg.libXi
    libGL
    mesa
  ];

  postFixup = ''
    patchelf --set-rpath ${lib.makeLibraryPath buildInputs} $out/bin/allcolors-rs
  '';

  desktopItems = [
    (makeDesktopItem {
      name = "allcolors";
      desktopName = "Polygon's allcolors-rs";
      categories = [ "Game" ];
      exec = "allcolors-rs";
    })
  ];
}
