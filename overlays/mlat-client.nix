{ buildPythonApplication, fetchFromGitHub }:

buildPythonApplication rec {
  pname = "mlat-client";
  version = "unstable-2023-12-22";

  src = fetchFromGitHub {
    owner = "adsbxchange";
    repo = "mlat-client";
    rev = "fabefdc3543702c6ef950111b94981162c45147d";
    hash = "sha256-7FgyoZ08hGoMJWb1O/duIfiVb0udt8jwSoRSDTQ1Y+g=";
  };
}
