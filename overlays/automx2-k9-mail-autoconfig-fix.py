diff --git a/automx2/generators/mozilla.py b/automx2/generators/mozilla.py
index 790a1a5..615d408 100644
--- a/automx2/generators/mozilla.py
+++ b/automx2/generators/mozilla.py
@@ -62,7 +62,6 @@ def client_config(self, local_part, domain_part: str, display_name: str, ignored
         if not provider:  # pragma: no cover (db constraints prevent this during testing)
             raise NoProviderForDomain(f'No provider for domain "{domain_part}"')
         provider_element = SubElement(root_element, 'emailProvider', attrib={'id': branded_id(provider.id)})
-        SubElement(provider_element, 'identity')  # Deliberately left empty
         for provider_domain in provider.domains:
             SubElement(provider_element, 'domain').text = provider_domain.name
         SubElement(provider_element, 'displayName').text = provider.name
diff --git a/automx2/views/autoconfig.py b/automx2/views/autoconfig.py
index 4f457f4..9f7d4fe 100644
--- a/automx2/views/autoconfig.py
+++ b/automx2/views/autoconfig.py
@@ -35,9 +35,20 @@ def get(self):
         """GET request is expected to contain ?emailaddress=user@example.com"""
         address = request.args.get(EMAIL_MOZILLA, '')
         if not address:
-            message = f'Missing request argument "{EMAIL_MOZILLA}"'
-            log.error(message)
-            return message, 400
+            host = request.headers.get('host', '')
+            # TODO: exist early if LDAP is used
+            if not host:
+                message = f'Missing request argument "{EMAIL_MOZILLA}"'
+                log.error(message)
+                return message, 400
+            else:
+                split = host.split(".", maxsplit=1)
+                if len(split) > 1:
+                    domain = split[1]
+                else:
+                    domain = split[0]
+                address = "dummy-user-name@" + domain
+
         try:
             return self.config_from_address(address)
         except NotFoundException:
diff --git a/tests/test_mozilla.py b/tests/test_mozilla.py
index c10e8bc..4768417 100644
--- a/tests/test_mozilla.py
+++ b/tests/test_mozilla.py
@@ -53,7 +53,7 @@ def smtp_server_elements(element: Element) -> List[Element]:
     def test_mozilla_missing_arg(self):
         with self.app:
             r = self.get(MOZILLA_CONFIG_ROUTE)
-            self.assertEqual(400, r.status_code)
+            self.assertEqual(204, r.status_code)
 
     def test_mozilla_no_domain_match(self):
         with self.app:
