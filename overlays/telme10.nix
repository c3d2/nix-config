{ buildGoModule
, fetchFromGitea
}:

buildGoModule {
  pname = "telme10";
  version = "unstable-2024-10-27";

  src = fetchFromGitea {
    domain = "gitea.c3d2.de";
    owner = "c3d2";
    repo = "telme10";
    rev = "be2b6a1aeb7ca6963f4af29827d6c0d70cc66e22";
    hash = "sha256-D1KZZxe++WZQGS9K9FDgZdRK+a/TirIPdZrkMlqMAE4=";
  };

  postPatch = ''
    substituteInPlace telme10.service \
      --replace-fail "/usr/local/bin/telme10" "${placeholder "out"}/bin/telme10"
  '';

  vendorHash = "sha256-SzYAXvWE2qt7aPX99AhgTQe7tmGuaBuOUZNNg7+CvCQ=";

  postInstall = ''
    install -Dt $out/etc/systemd/system telme10.service
    install -Dt $out/etc/systemd/system telme10.socket
  '';
}
