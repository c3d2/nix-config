{ callPackage
, applyPatches
, autoPatchelfHook
, binaryen
, buildEnv
, fenix
, fenix-old-rust-manifest
, fetchFromGitHub
, gettext
, naersk
, openssl
, pkg-config
, postgresql
, stdenv
, wasm-bindgen-cli
, wasm-pack
}:

let
  wasm-bindgen-cli' = wasm-bindgen-cli.override {
    version = "0.2.92";
    hash = "sha256-1VwY8vQy7soKEgbki4LD+v259751kKxSxmo/gqE6yV0=";
    cargoHash = "sha256-aACJ+lYNEU8FFBs158G1/JG8sc6Rq080PeKCMnwdpH0=";
  };
  rust = (fenix.packages.${stdenv.system}.fromManifestFile fenix-old-rust-manifest).withComponents [
    "cargo"
    "rustc"
  ];
  naersk' = callPackage naersk {
    cargo = rust;
    rustc = rust;
  };
  rust-wasm = with fenix.packages.${stdenv.system};
    combine [
      minimal.rustc
      minimal.cargo
      targets.wasm32-unknown-unknown.latest.rust-std
    ];
  naersk-wasm = callPackage naersk {
    cargo = rust-wasm;
    rustc = rust-wasm;
  };

  version = "0.7.2-2";

  src = applyPatches {
    src = fetchFromGitHub {
      owner = "Plume-org";
      repo = "Plume";
      rev = "304fb740d8ba3aaae64b3eb67ecfe476841b87c4";
      sha256 = "sha256-nMiVMR0arhv4bwx8b5FGawjQyiOXDhRZV8ERdBtHCYM=";
    };

    patches = [
      ./0001-cargo-update.patch
      ./0002-plume-front-fixup.patch
    ];
  };

  plume = naersk'.buildPackage {
    pname = "plume";
    inherit src version;

    nativeBuildInputs = [
      autoPatchelfHook # somehow the required .so of plume are not linked correctly...
      gettext
      pkg-config
      wasm-bindgen-cli'
    ];

    buildInputs = [
      openssl
      postgresql
    ];

    overrideMain = oa: {
      installPhase = ''
        ${oa.installPhase}
        # somehow hook is not run...
        autoPatchelfPostFixup

        mkdir -p $out/share/plume
        cp -ar static $out/share/plume/
      '';
    };
  };

  plm = naersk'.buildPackage {
    pname = "plm";
    inherit version;
    root = src;

    nativeBuildInputs = [
      autoPatchelfHook # somehow the required .so of plume are not linked correctly...
      pkg-config
    ];

    buildInputs = [
      openssl
      postgresql
      stdenv.cc.cc.lib
    ];

    cargoBuildOptions = x: x ++ [ "--package=plume-cli" ];

    overrideMain = oa: {
      installPhase = ''
        ${oa.installPhase}
        # somehow hook is not run...
        autoPatchelfPostFixup
      '';
    };
  };

  plume-front = naersk-wasm.buildPackage {
    pname = "plume-front";
    inherit version;
    root = src;

    nativeBuildInputs = [
      gettext
      wasm-pack
      wasm-bindgen-cli'
      binaryen
    ];

    CARGO_BUILD_TARGET = "wasm32-unknown-unknown";
    cargoBuildOptions = x: x ++ [ "--package=plume-front" ];

    copyLibs = true;

    overrideMain = _: {
      buildPhase = ''
        WASM_PACK_CACHE=.wasm-pack-cache wasm-pack build --mode no-install --target web --release plume-front
      '';
      installPhase = ''
        mkdir -p $out/share/plume/static
        cp -a plume-front/pkg/*.{js,ts,wasm} $out/share/plume/static/
      '';
    };
  };
in
buildEnv {
  name = "plume-env";
  paths = [ plume plume-front plm ];
  passthru = { inherit plume plm; };
}
