{ stdenv, bundlerEnv }:

let
  gems = bundlerEnv {
    name = "dump1090-influxdb-gems";
    gemdir = ./.;
  };
in
stdenv.mkDerivation rec {
  name = "dump1090-influxdb";
  buildInputs = [ gems.wrappedRuby ];
  buildCommand = ''
    install -D -m755 ${./main.rb} $out/bin/${name}
    patchShebangs $out/bin/${name}
  '';
}
