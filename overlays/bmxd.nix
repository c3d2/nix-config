{ stdenv, fetchgit }:

stdenv.mkDerivation {
  pname = "bmxd";
  version = "1.1-freifunk-dresden-c3d2";

  src = fetchgit {
    url = "https://gitlab.freifunk-dresden.de/firmware-developer/firmware.git";
    rev = "T_FIRMWARE_8.1.5";
    sha256 = "sha256-DEz9KROKhOSdp0XPvkpV5DCTN06FxIMj2YaVKC0hkyo=";
  };

  preBuild = ''
    cd feeds/common/bmxd/sources
  '';

  installPhase = ''
    mkdir -p $out/sbin
    cp bmxd $out/sbin
  '';
}
