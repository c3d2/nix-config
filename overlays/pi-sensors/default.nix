{ rustPlatform }:

rustPlatform.buildRustPackage {
  name = "pi-sensors";
  version = "0.0.0";

  src = ./.;

  cargoHash = "0pihg88jx61a3bxm6n6h0fila34xgfnpgqrsdk4bw165bwmp5laq";
}
