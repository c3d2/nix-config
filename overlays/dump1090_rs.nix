{ fetchFromGitHub
, rustPlatform
, pkg-config
, llvmPackages
, soapysdr-with-plugins
}:

rustPlatform.buildRustPackage rec {
  name = "dump1090_rs";
  version = "0.5.1";

  src = fetchFromGitHub {
    owner = "rsadsb";
    repo = "dump1090_rs";
    rev = "v${version}";
    sha256 = "1jhcb5b3l1q8zz3hfwyxy69i1015jmbdw3zlnhvalgqhp9qli2li";
  };

  cargoHash = "00270yfbgz794m8mifnskvgqd6h17mm18cxr10371zlymnsnjf2c";

  nativeBuildInputs = [ pkg-config llvmPackages.clang ];

  LIBCLANG_PATH = "${llvmPackages.libclang.lib}/lib";

  buildInputs = [ soapysdr-with-plugins ];
}
