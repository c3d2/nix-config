{ bevy-julia
, bevy-mandelbrot
, tracer
, fenix
, fenix-old-rust-manifest
, naersk
}:

final: prev:

with final; {
  allcolors = callPackage ./allcolors.nix { };

  inherit (bevy-julia.packages.${stdenv.system}) bevy_julia;
  inherit (bevy-mandelbrot.packages.${stdenv.system}) bevy_mandelbrot;

  bmxd = callPackage ./bmxd.nix { };

  ceph_17_2 = assert (lib.assertMsg ((lib.versions.majorMinor ceph.version) == "17.2") "Do not upgrade ceph!!!"); prev.ceph;

  dump1090-influxdb = callPackage ./dump1090-influxdb { };

  dump1090_rs = callPackage ./dump1090_rs.nix { };

  chromium = prev.chromium.override {
    # enable hardware accerlation with vaapi, force dark mode, detect kwallet
    commandLineArgs = "--enable-features=VaapiVideoEncoder,VaapiVideoDecoder,CanvasOopRasterization --force-dark-mode --password-store=detect";
  };

  firefox = prev.firefox.override {
    extraPolicies = {
      DisablePocket = true;
      FirefoxHome.Pocket = false;
    };
  };

  hedgedoc = prev.hedgedoc.overrideAttrs ({ patches ? [ ], ... }: {
    patches = patches ++ [
      ./hedgedoc-allowAnonymousUploads.diff
      ./hedgedoc-improve-protected-text.diff
    ];
  });

  hydra_unstable = prev.hydra_unstable.overrideAttrs (oldAttrs: {
    patches = oldAttrs.patches or [ ] ++ [
      # gitea webhook support
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/pull/1227/commits/750978a19232583e17620a1bd80435e957e7213a.patch";
        sha256 = "sha256-86Li0YUSVUdnw6lt6kZ56ohDRKPD13SZzukqPU1np8U=";
      })
      # fix github webhook from orgs
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/commit/4d664ecb0faaf51b21673f979b543ea4694c3f1b.patch";
        sha256 = "sha256-lF5Rnz8r9ptyMLhcg/XnjiNhOK1KcLA7hi01ye4KgmI=";
      })
      # Fix doi resolution
      (fetchpatch {
        url = "https://github.com/NixOS/hydra/commit/1b8154e67fb20f84b7f84049de03204b0df0a366.diff";
        hash = "sha256-E1yE9TOIY2TPXEbG2GO5MHmzrEPdID5uKklAOCSa07A=";
      })
    ];
  });

  jackett = prev.jackett.overrideAttrs {
    # 2 failing tests
    doCheck = false;
  };

  mailmanPackages = prev.mailmanPackages.extend (mfinal: mprev: {
    mailman = mprev.mailman.overrideAttrs ({ patches ? [ ], ... }: {
      patches = patches ++ [
        ./mailman-dont-attach-message.diff
      ];
    });
  });

  mlat-client = python3Packages.callPackage ./mlat-client.nix { };

  openssh = prev.openssh.overrideAttrs (_: {
    # takes 30 minutes
    doCheck = false;
  });

  pi-sensors = callPackage ./pi-sensors { };

  plume = callPackage ./plume {
    inherit fenix fenix-old-rust-manifest naersk;
  };

  pythonPackagesExtensions = prev.pythonPackagesExtensions ++ [
    (python-final: python-prev: {
      automx2 = python-prev.automx2.overrideAttrs ({ patches ? [ ], ... }: {
        patches = patches ++ [
          ./automx2-k9-mail-autoconfig-fix.py
          ./automx2-seed-json-user-name.diff
        ];
      });
    })
  ];

  readsb = callPackage ./readsb.nix { };

  renovate = prev.renovate.overrideAttrs ({ patches ? [ ], ... }: {
     patches = patches ++ [
       (fetchpatch {
         url = "https://github.com/renovatebot/renovate/pull/31921.diff";
         hash = "sha256-DiYZVhc+EaDiTPo/z6hTKuMgC10+/Onv+6g5C3QmnDQ=";
       })
     ];
  });

  roundcube = prev.roundcube.overrideAttrs ({ patches ? [ ], ... }: {
    patches = patches ++ [
      ./roundcube-imprint.diff
    ];
  });

  telme10 = callPackage ./telme10.nix { };

  tracer-game =
    if true
    then throw "tracer-game: haddock runs on affection for 10 hours and more"
    else tracer.packages.${stdenv.system}.tracer-game;
}
