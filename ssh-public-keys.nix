{
  # Please use ed25519 keys!

  astro = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGJJTSJdpDh82486uPiMhhyhnci4tScp5uUe7156MBC8 astro"
  ];
  dennis = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJYHv/LMo8N6iM3zFvOKrF7ZLp3eAG/cOED0yDzrvgkd openpgp:0x74CCE9B8"
  ];
  emery = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID7WMwQXLq4cJvKpuRTqJ9xrn2gAbQdXFCQBRwwS8iM0 emery"
  ];
  eri = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAn5V4X6EWQJ1nUvYHSBS9UGJG3ljWiGRWWGWCYszdL+ eri"
  ];
  marenz = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK0g2eARHuuxFJj/0/syHz/8iSPnTflmXnvugL+wpIbL markus.schmidl@mailbox.tu-dresden.de"
  ];
  nek0 = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILpLQaRn6wzdyU5f1MZKYgL3A9t0H/ELyZHEMK0e2I+k nek0@madness"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG1/j/UDBWP3trFbz1znNlyXv4uruBAlaAApyAY1s2WP nek0@mania"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIJqw0egP2bhA3PFN9eEvExnFAAcgCEqqaF1OsYW1/xi nek0@insanity"
  ];
  oxa = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJl9iYG5oHBq/poBn7Jf1/FGWWbAnbx+NKjs7qtT3uAK 0xa@toaster 2024-12-31"
  ];
  polygon = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICGEKrCGXyHqD0jdTYVHnnScL9mhDU2PR9VyH7fu528J jan@nixbrett - polygon"
  ];
  poelzi = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID2ILx8LXED0CkVu4GguT8QckavMlB5A9CKS8BH+CmFc 2023@poelzi.org"
  ];
  revol-xut = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC6NLB8EHnUgl2GO2uaojdf3p3YpsHH6px6CZleif8klhLN+ro5KeFK2OXC2SO3Vo4qgF/NySdsoInV9JEsssELZ2ttVbeKxI6f76V5dZgGI7qoSf4E0TXIgpS9n9K2AEmRKr65uC2jgkSJuo/T1mF+4/Nzyo706FT/GGVoiBktgq9umbYX0vIQkTMFAcw921NwFCWFQcMYRruaH01tLu6HIAdJ9FVG8MAt84hCr4D4PobD6b029bHXTzcixsguRtl+q4fQAl3WK3HAxT+txN91CDoP2eENo3gbmdTBprD2RcB/hz5iI6IaY3p1+8fTX2ehvI3loRA8Qjr/xzkzMUlpA/8NLKbJD4YxNGgFbauEmEnlC8Evq2vMrxdDr2SjnBAUwzZ63Nq+pUoBNYG/c+h+eO/s7bjnJVe0m2/2ZqPj1jWQp4hGoNzzU1cQmy6TdEWJcg2c8ints5068HN3o0gQKkp1EseNrdB8SuG+me/c/uIOX8dPASgo3Yjv9IGLhhx8GOGQxHEQN9QFC4QyZt/rrAyGmlX342PBNYmmStgVWHiYCcMVUWGlsG0XvG6bvGgmMeHNVsDf6WdMQuLj9luvxJzrd4FlKX6O0X/sIaqMVSkhIbD2+vvKNqrii7JdUTntUPs89L5h9DoDqQWkL13Plg1iQt4/VYeKTbUhYYz1lw== revo-xut@plank"
  ];
  sandro = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH1mO1oDrcg401cOzfBpHwRnaudwBvvsxXgEXKnjv6VI sandro@carbon"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFidD6Snqgd8J7avxHvdDd81rdi0zNZWSilBe3eaTIlv sandro@magnesium"
    "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIAUDvmdH7DwqMXLg/fAXtwme44P5L6ye9dFcVIdL+wk5AAAABHNzaDo= sandro@geode"
    "sk-ssh-ed25519@openssh.com AAAAGnNrLXNzaC1lZDI1NTE5QG9wZW5zc2guY29tAAAAIDZVEPkbVT3+g5PEngQ4HSmXWBppmoAYuDIrZrPYMeXrAAAABHNzaDo= sandro@prism"
  ];
  tboston = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIINkmizml/XsSRzp3mNIumb3ZEPQoZhi/TtDU7rOUiKA tboston"
  ];
  wolf = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJa4Xl4izrsirkBPxRruPSyByWj31Tya1h+jDQ94ZuU3 vv01f@debitch"
  ];
}
