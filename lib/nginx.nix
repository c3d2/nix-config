{ subnets, ... }:

{
  defaultListen = let
    listen = [
      {
        addr = "[::]";
        port = 80;
      }
      {
        addr = "[::]";
        port = 443;
        ssl = true;
      }
      {
        addr = "[::]";
        port = 8080;
        extraParameters = [ "proxy_protocol" ];
      }
      {
        addr = "[::]";
        port = 8443;
        ssl = true;
        extraParameters = [ "proxy_protocol" ];
      }
    ];
  in
  map (x: (x // { addr = "0.0.0.0"; })) listen ++ listen;

  hqNetworkOnly = ''
    satisfy any;
    ${builtins.concatStringsSep "\n" (map (subnet: "allow ${subnet};") subnets.v4)}
    ${builtins.concatStringsSep "\n" (map (subnet: "allow ${subnet};") subnets.v6)}
    allow ::1/128;
    allow 127.0.0.0/8;
    deny all;
  '';
}
