{ lib, zentralwerk }:

rec {
  extractZwHosts = { hosts4, hosts6, ... }:
    lib.recursiveUpdate (
      builtins.foldl' (result: name:
        lib.recursiveUpdate result {
          "${name}".ip4 = hosts4."${name}";
        }
      ) {} (builtins.attrNames hosts4)
    ) (
      builtins.foldl' (result: ctx:
        builtins.foldl' (result: name:
          lib.recursiveUpdate result {
            "${name}".ip6 = hosts6."${ctx}"."${name}";
          }
        ) result (builtins.attrNames hosts6."${ctx}")
      ) {} (builtins.attrNames hosts6)
    );

  hostRegistry =
    builtins.foldl' (result: net:
      lib.recursiveUpdate result (extractZwHosts zentralwerk.lib.config.site.net."${net}")
    ) {} [ "core" "cluster" "c3d2" "serv" "flpk" "pub" ];

  # TODO: also allow neighbors?
  subnets = {
    v4 = [
      # dn42/c3d2
      "172.22.99.0/24"
      # dn42/zentralwerk
      "172.20.72.0/21"
      # flpk
      "45.158.40.160/27"
    ];

    v6 = [
      # DSI
      "2a00:8180:2c00:200::/56"
      # flpk
      "2a0f:5382:acab:1400::/56"
      # dn42
      "fd23:42:c3d2:500::/56"
    ];
  };
}
