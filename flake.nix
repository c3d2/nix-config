{
  description = "C3D2 NixOS configurations";

  nixConfig = {
    extra-substituters = [ "https://hydra.hq.c3d2.de" ];
    extra-trusted-public-keys = [ "hydra.hq.c3d2.de:KZRGGnwOYzys6pxgM8jlur36RmkJQ/y8y62e52fj1ps=" ];
  };

  inputs = {
    # use sandro's fork full with cherry-picked fixes
    nixpkgs.url = "git+https://github.com/NuschtOS/nuschtpkgs.git?shallow=1&ref=backports-24.11";
    nixos-hardware.url = "github:nixos/nixos-hardware";
    nixpkgs-taler.url = "github:astro/nixpkgs/init/gnu-taler";

    affection-src = {
      url = "git+https://gitea.nek0.eu/nek0/affection";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
    alert2muc = {
      url = "git+https://gitea.c3d2.de/c3d2/alert2muc";
      inputs = {
        naersk.follows = "naersk";
        nixpkgs.follows = "nixpkgs";
        utils.follows = "flake-utils";
      };
    };
    bevy-mandelbrot = {
      # url = "github:matelab/bevy_mandelbrot";
      url = "git+https://gitea.c3d2.de/astro/bevy-mandelbrot.git";
      inputs = {
        naersk.follows = "naersk";
        nixpkgs.follows = "nixpkgs";
        rust-overlay.follows = "rust-overlay";
      };
    };
    bevy-julia = {
      # url = "github:matelab/bevy_julia";
      url = "git+https://gitea.c3d2.de/astro/bevy-julia.git";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        naersk.follows = "naersk";
        rust-overlay.follows = "rust-overlay";
      };
    };
    buzzrelay = {
      url = "github:astro/buzzrelay";
      inputs = {
        naersk.follows = "naersk";
        nixpkgs.follows = "nixpkgs";
        utils.follows = "flake-utils";
      };
    };
    caveman = {
      url = "git+https://gitea.c3d2.de/astro/caveman.git";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        utils.follows = "flake-utils";
      };
    };
    c3d2-user-module = {
      url = "git+https://gitea.c3d2.de/c3d2/nix-user-module.git";
      inputs = {
        nixos-modules.follows = "nixos-modules";
        nixpkgs.follows = "nixpkgs";
      };
    };
    deployment = {
      url = "git+https://gitea.c3d2.de/c3d2/deployment.git";
      inputs.zentralwerk.follows = "zentralwerk";
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    c3d2-dns = {
      url = "git+https://gitea.c3d2.de/c3d2/dns.git";
      inputs = {
        dns-nix.follows = "dns-nix";
        nixpkgs.follows = "nixpkgs";
      };
    };
    dns-nix = {
      url = "github:nix-community/dns.nix";
      inputs = {
        flake-utils.follows = "flake-utils";
        nixpkgs.follows = "nixpkgs";
      };
    };
    fenix = {
      url = "github:nix-community/fenix/monthly";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    fenix-old-rust-manifest = {
      url = "https://static.rust-lang.org/dist/2024-06-30/channel-rust-nightly.toml";
      flake = false;
    };
    flake-utils.url = "github:numtide/flake-utils";
    heliwatch = {
      url = "git+https://gitea.c3d2.de/astro/heliwatch.git";
      inputs = {
        fenix.follows = "fenix";
        nixpkgs.follows = "nixpkgs";
        naersk.follows = "naersk";
        utils.follows = "flake-utils";
      };
    };
    microvm = {
      # url = "github:astro/microvm.nix";
      url = "github:SuperSandro2000/microvm.nix/combined";
      inputs = {
        flake-utils.follows = "flake-utils";
        nixpkgs.follows = "nixpkgs";
      };
    };
    naersk = {
      url = "github:nix-community/naersk";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    nix-cache-cut = {
      url = "github:astro/nix-cache-cut";
      inputs = {
        naersk.follows = "naersk";
        nixpkgs.follows = "nixpkgs";
        utils.follows = "flake-utils";
      };
    };
    nixos-modules = {
      # NOTE: mirrored to https://gitea.c3d2.de/c3d2/nixos-modules
      # If there are questions, things should be added or changed, contact sandro
      url = "github:NuschtOS/nixos-modules";
      inputs = {
        flake-utils.follows = "flake-utils";
        nixpkgs.follows = "nixpkgs";
      };
    };
    nuschtos-search = {
      url = "github:NuschtOS/search";
      inputs = {
        flake-utils.follows = "flake-utils";
        nixpkgs.follows = "nixpkgs";
      };
    };
    oparl-scraper = {
      url = "github:offenesdresden/ratsinfo-scraper/oparl";
      flake = false;
    };
    openwrt = {
      url = "git+https://git.openwrt.org/openwrt/openwrt.git?ref=openwrt-23.05";
      flake = false;
    };
    openwrt-imagebuilder = {
      url = "github:astro/nix-openwrt-imagebuilder";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        systems.follows = "flake-utils/systems";
      };
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay/stable";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    scrapers = {
      url = "git+https://gitea.c3d2.de/c3d2/scrapers.git";
      flake = false;
    };
    skyflake = {
      url = "github:astro/skyflake";
      inputs = {
        microvm.follows = "microvm";
        nixpkgs.follows = "nixpkgs";
        nix-cache-cut.follows = "nix-cache-cut";
      };
    };
    sshlogd = {
      url = "git+https://gitea.c3d2.de/astro/sshlogd.git";
      inputs = {
        utils.follows = "flake-utils";
        naersk.follows = "naersk";
        nixpkgs.follows = "nixpkgs";
        fenix.follows = "fenix";
      };
    };
    simple-nixos-mailserver = {
      # url = "git+https://gitlab.com/simple-nixos-mailserver/nixos-mailserver.git?ref=nixos-24.05";
      # TODO: switch backports branch from master to real nixos-24.11 branch
      url = "git+https://gitlab.com/SuperSandro2000/nixos-mailserver.git?ref=backports-24.11";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        nixpkgs-24_05.follows = "nixpkgs";
      };
    };
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    spacemsg = {
      url = "git+https://gitea.c3d2.de/c3d2/spacemsg";
      flake = false;
    };
    ticker = {
      url = "git+https://gitea.c3d2.de/astro/ticker.git";
      inputs = {
        fenix.follows = "fenix";
        naersk.follows = "naersk";
        nixpkgs.follows = "nixpkgs";
        utils.follows = "flake-utils";
      };
    };
    tigger = {
      url = "git+https://gitea.c3d2.de/c3d2/tigger";
      flake = false;
    };
    tracer = {
      # url = "git+https://gitea.nek0.eu/nek0/tracer";
      url = "git+https://gitea.c3d2.de/astro/tracer";
      inputs = {
        affection-src.follows = "affection-src";
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
    yammat = {
      url = "git+https://gitea.c3d2.de/c3d2/yammat.git";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    zentralwerk = {
      url = "git+https://gitea.c3d2.de/zentralwerk/network.git";
      inputs = {
        c3d2-dns.follows = "c3d2-dns";
        dns-nix.follows = "dns-nix";
        nixos-modules.follows = "nixos-modules";
        nixpkgs.follows = "nixpkgs";
        openwrt.follows = "openwrt";
        openwrt-imagebuilder.follows = "openwrt-imagebuilder";
      };
    };
  };

  outputs = inputs@{ self, alert2muc, c3d2-user-module, c3d2-dns, deployment, disko, fenix, fenix-old-rust-manifest, heliwatch, microvm, naersk, nuschtos-search, nixos-hardware, nixos-modules, nixpkgs, buzzrelay, caveman, oparl-scraper, simple-nixos-mailserver, scrapers, skyflake, sshlogd, sops-nix, spacemsg, ticker, tigger, yammat, zentralwerk, ... }:
    let
      inherit (nixpkgs) lib;

      libC =
        let
          network = import ./lib/network.nix { inherit lib zentralwerk; };
        in {
          inherit (import ./lib/nginx.nix { inherit (network) subnets; }) defaultListen hqNetworkOnly;
          inherit (network) hostRegistry subnets;
          inherit (zentralwerk.lib) dns;
          inherit (zentralwerk.lib.config) site;
          ssh-public-keys = import ./ssh-public-keys.nix;
        };

      overlayList = [
        self.overlays
      ];

      # Our custom NixOS builder
      nixosSystem' =
        { nixpkgs ? inputs.nixpkgs
        , modules
        , system ? "x86_64-linux"
      }@args:

      { inherit args; } // nixpkgs.lib.nixosSystem {
        inherit system;

        modules = [
          {
            _module.args = {
              inherit libC nixpkgs zentralwerk;
              dns = c3d2-dns;
            };

            nixpkgs.overlays = overlayList;
          }

          self.nixosModules.c3d2
        ] ++ modules;
      };
    in {
      inherit self;

      overlays = import ./overlays {
        inherit (inputs) fenix fenix-old-rust-manifest naersk bevy-julia bevy-mandelbrot tracer;
      };

      legacyPackages = lib.attrsets.mapAttrs (_: pkgs: pkgs.appendOverlays overlayList) nixpkgs.legacyPackages;

      packages = import ./packages.nix { inherit libC inputs lib microvm self; };

      nixosConfigurations = {
        auth = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/auth
          ];
        };

        blogs = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/blogs
            {
              nixpkgs.overlays = [
                fenix.overlays.default
                naersk.overlay
              ];
            }
          ];
        };

        broker = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/broker
          ];
        };

        buzzrelay = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            buzzrelay.nixosModules.default
            { nixpkgs.overlays = [ buzzrelay.overlays.default ]; }
            ./hosts/buzzrelay
          ];
        };

        c3d2-web = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/c3d2-web
          ];
        };

        caveman = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            caveman.nixosModule
            ./hosts/caveman
          ];
        };

        dacbert = nixosSystem' {
          modules = [
            nixos-hardware.nixosModules.raspberry-pi-4
            self.nixosModules.rpi-netboot
            ./hosts/dacbert
          ];
          system = "aarch64-linux";
        };

        dn42 = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/dn42
          ];
        };

        engel = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/engel
          ];
        };

        knot = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/knot
          ];
        };

        drone = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/drone
          ];
        };

        freifunk = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/freifunk
          ];
        };

        ftp = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/ftp
          ];
        };

        gitea = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            self.nixosModules.gitea-actions-registrar
            self.nixosModules.gitea-actions-runner
            ./hosts/gitea
          ];
        };

        glotzbert = nixosSystem' {
          modules = [
            nixos-hardware.nixosModules.common-cpu-intel # also includes iGPU
            ./hosts/glotzbert
          ];
        };

        gnunet = nixosSystem' {
          modules = [
            self.nixosModules.cluster-options
            self.nixosModules.microvm
            ./hosts/gnunet
          ];
        };

        grafana = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/grafana
          ];
        };

        hedgedoc = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/hedgedoc
          ];
        };

        home-assistant = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/home-assistant
          ];
        };

        hydra = nixosSystem' {
          modules = [
            self.nixosModules.cluster
            self.nixosModules.gitea-actions-runner
            # skyflake.nixosModules.default
            ./hosts/hydra
          ];
        };

        iso = nixosSystem' {
          modules = [
            ({ modulesPath, ... }: {
              imports = lib.singleton "${modulesPath}/installer/cd-dvd/installation-cd-graphical-calamares-plasma5.nix";
            })
          ];
        };

        iso-minimal = nixosSystem' {
          modules = [
            ({ modulesPath, ... }: {
              imports = lib.singleton "${modulesPath}/installer/cd-dvd/installation-cd-minimal.nix";
            })
          ];
        };

        jabber = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/jabber
          ];
        };

        mail = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            simple-nixos-mailserver.nixosModules.mailserver
            ./hosts/mail
          ];
        };

        matrix = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/matrix
          ];
        };

        mastodon = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/mastodon
          ];
        };

        matemat = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/matemat
            yammat.nixosModule
          ];
        };

        mediagoblin = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/mediagoblin
          ];
        };

        mediawiki = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/mediawiki
          ];
        };

        mobilizon = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/mobilizon
          ];
        };

        mucbot = nixosSystem' {
          modules = [
            "${tigger}/module.nix"
            ./hosts/mucbot
            self.nixosModules.cluster-options
            self.nixosModules.microvm
          ];
        };

        nfsroot = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/nfsroot
            {
              _module.args.tftproots = nixpkgs.lib.filterAttrs (name: _:
                builtins.match ".+-tftproot" name != null
              ) self.packages.x86_64-linux;
            }
          ];
        };

        nixos-misc = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/nixos-misc
            {
              _module.args = { inherit self nuschtos-search; };
            }
          ];
        };

        nixtaler = nixosSystem' {
          nixpkgs = inputs.nixpkgs-taler;
          modules = [
            self.nixosModules.microvm
            ./hosts/nixtaler
          ];
        };

        oparl = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/oparl
            {
              _module.args = { inherit oparl-scraper; };
            }
          ];
        };

        owncast = nixosSystem' {
          modules = [
            self.nixosModules.cluster-options
            self.nixosModules.microvm
            ./hosts/owncast
          ];
        };

        pipebert = nixosSystem' {
          modules = [
            ./hosts/pipebert
          ];
        };

        pretalx = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/pretalx
          ];
        };

        prometheus = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            alert2muc.nixosModules.default
            ./hosts/prometheus
          ];
        };

        pulsebert = nixosSystem' {
          modules = [
            ./hosts/pulsebert
            # build: outputs.nixosConfigurations.pulsebert.config.system.build.sdImage
            # run: unzstd -cd result/sd-image/nixos-sd-image-*-aarch64-linux.img.zst | pv -br | sudo dd bs=4M of=/dev/sdX
            "${inputs.nixpkgs}/nixos/modules/installer/sd-card/sd-image-aarch64-new-kernel.nix"
            {
              nixpkgs = {
                hostPlatform = "aarch64-linux";
                # buildPlatform = "x86_64-linux";
              };
            }
          ];
        };

        public-access-proxy = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/public-access-proxy
          ];
        };

        radiobert = nixosSystem' {
          modules = [
            ./hosts/radiobert
            {
              nixpkgs.overlays = [ heliwatch.overlay ];
            }
          ];
          system = "aarch64-linux";
        };

        riscbert = nixosSystem' {
          modules = [
            nixos-hardware.nixosModules.starfive-visionfive-v1
            ./hosts/riscbert
            {
              nixpkgs.crossSystem = {
                config = "riscv64-unknown-linux-gnu";
                system = "riscv64-linux";
              };
            }
          ];
          system = "x86_64-linux";
        };

        rpi-netboot = nixosSystem' {
          modules = [
            nixos-hardware.nixosModules.raspberry-pi-4
            self.nixosModules.rpi-netboot
            ./hosts/rpi-netboot
          ];
          system = "aarch64-linux";
        };

        scrape = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/scrape
            {
              _module.args = { inherit scrapers; };
            }
          ];
        };

        sdrweb = nixosSystem' {
          modules = [
            ./hosts/sdrweb
            heliwatch.nixosModules.heliwatch
            self.nixosModules.microvm
            self.nixosModules.cluster-options
          ];
        };

        server9 = nixosSystem' {
          modules = [
            ./hosts/server9
            self.nixosModules.microvm-host
            self.nixosModules.cluster-network
            self.nixosModules.cluster
            # skyflake.nixosModules.default
            { _module.args = { inherit self; }; }
          ];
        };

        server10 = nixosSystem' {
          modules = [
            ./hosts/server10
            self.nixosModules.microvm-host
            self.nixosModules.cluster-network
            self.nixosModules.cluster
            # skyflake.nixosModules.default
            { _module.args = { inherit self; }; }
          ];
        };

        spaceapi = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            "${spacemsg}/spaceapi/module.nix"
            ./hosts/spaceapi
          ];
        };

        sshlog = nixosSystem' {
          modules = [
            self.nixosModules.cluster-options
            self.nixosModules.microvm
            sshlogd.nixosModule
            ./hosts/sshlog
          ];
        };

        stream = nixosSystem' {
          modules = [
            self.nixosModules.cluster-options
            self.nixosModules.microvm
            ./hosts/stream
          ];
        };

        ticker = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ticker.nixosModules.ticker
            ./hosts/ticker
          ];
        };

        vaultwarden = nixosSystem' {
          modules = [
            self.nixosModules.microvm
            ./hosts/vaultwarden
          ];
        };
      };

      nixosModules = {
        c3d2.imports = [
          # adds config.system.build.isoImage which can be used to build an iso for any system
          # which is very useful to get its networking configuration
          # ({ config, modulesPath, ... }: {
          #   imports = lib.singleton "${modulesPath}/installer/cd-dvd/installation-cd-minimal.nix";
          #   isoImage.edition = lib.mkForce config.networking.hostName;
          # })

          c3d2-user-module.nixosModule
          disko.nixosModules.disko
          nixos-modules.nixosModule
          sops-nix.nixosModules.default
          ./config
          ./modules/audio-server.nix
          ./modules/autoupdate.nix
          ./modules/backup.nix
          ./modules/baremetal.nix
          ./modules/c3d2.nix
          ./modules/disko.nix
          ./modules/nginx.nix
          ./modules/pi-boot.nix
          ./modules/pi-sensors.nix
          ./modules/plume.nix
          ./modules/stats.nix
        ];
        cluster = ./modules/cluster;
        cluster-network = ./modules/cluster/network.nix;
        cluster-options.imports = [
          deployment.nixosModules.deployment-options
          ./modules/microvm-defaults.nix
        ];
        microvm.imports = [
          microvm.nixosModules.microvm
          ./modules/microvm-defaults.nix
          ./modules/microvm.nix
        ];
        microvm-host.imports = [
          microvm.nixosModules.host
          ./modules/microvm-host.nix
        ];
        rpi-netboot = ./modules/rpi-netboot.nix;
        gitea-actions-registrar = ./modules/gitea-actions-registrar.nix;
        gitea-actions-runner = ./modules/gitea-actions-runner.nix;
      };

      # `nix develop`
      devShell = lib.mapAttrs (system: sopsPkgs:
        with nixpkgs.legacyPackages.${system};
        mkShell {
          sopsPGPKeyDirs = [ "./keys" ];
          nativeBuildInputs = [
            apacheHttpd
            sopsPkgs.sops-import-keys-hook
          ];
        }
      ) sops-nix.packages;

      hydraJobs =
        lib.mapAttrs (_: nixpkgs.lib.hydraJob) (
          let
            getBuildEntryPoint = name: nixosSystem:
              let
                cfg = if (lib.hasPrefix "iso" name) then
                  nixosSystem.config.system.build.isoImage
                else
                  nixosSystem.config.microvm.declaredRunner or nixosSystem.config.system.build.toplevel;
              in
              if nixosSystem.config.nixpkgs.system == "aarch64-linux" then
                # increase timeout for chromium
                lib.recursiveUpdate cfg { meta.timeout = 24 * 60 * 60; }
              else
                cfg;
          in
          lib.mapAttrs getBuildEntryPoint self.nixosConfigurations
          # NOTE: left here to have the code as reference if we need something like in the future, eg. on a stable update
          # // lib.mapAttrs' (hostname: nixosSystem: let
          #   hostname' = hostname + "-24-11";
          # in lib.nameValuePair
          #   hostname' # job display name
          #   (getBuildEntryPoint hostname' (nixosSystem' (nixosSystem.args // (with nixosSystem.args; {
          #     modules = modules ++ [
          #     #   {
          #     #     simd.enable = lib.mkForce true;
          #     #   }
          #     ];
          #     nixpkgs = inputs.nixpkgs-24-11;
          #   }))))
          # ) self.nixosConfigurations
          // nixpkgs.lib.filterAttrs (name: attr:
            (builtins.match ".+-tftproot" name != null && lib.isDerivation attr)
          ) self.packages.aarch64-linux
        );
    };
}
